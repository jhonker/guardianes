function update_album(id){
    $("#id_album").val(id);
    $.ajax({
        url:'/administracion/get_albums/'+id,
        type:'GET',
        dataType:'json',
        success:function(res){
            $(res).each(function(i, v){
                $("#titulo").val(v.titulo);
                $("#m_update_album").modal("show");
                //$("#ip_categoria_a").focus();
            });
        }
    })
}

function up_album(idalbum,titulo, token)
{
    $.ajax({
        url:'/administracion/publicaciones/admin_album',
        type:'POST',
        dataType:'json',
        headers :{'X-CSRF-TOKEN': token},
        data: {titulo:titulo, idalbum: idalbum},
        success:function(res){
        if(res.RES){
            alert("Album Guardado");
                window.location = '/administracion/publicaciones/administrar/admin_album';
            }else{
                alert("Error al Guardar");
            }
        }
    })
}

function delete_album(id){
    $("#ide_album").val(id);
    $.ajax({
        url:'/administracion/get_albums/'+id,
        type:'GET',
        dataType:'json',
        success:function(res){
            $(res).each(function(i, v){
                $("#dtitulo").val(v.titulo);
                $("#d_update_album").modal("show");
                //$("#ip_categoria_a").focus();
            });
        }
    })
}

function dl_album()
{
    var idalbum= $('#ide_album').val();
    var titulo = $('#dtitulo').val();
    var token = $("#token").val();
    if(titulo=="")
    {
        alert('No hay titulo');
    }
    else
    {
        $.ajax({
            url:'/administracion/publicaciones/delete_album',
            type:'POST',
            dataType:'json',
            headers :{'X-CSRF-TOKEN': token},
            data: {idalbum:idalbum},
            success:function(res){
            if(res.RES){
                alert("Registro Eliminado");
                    window.location = '/administracion/publicaciones/administrar/admin_album';
                }else{
                    alert("Error al Guardar");
                }
            }
        })
    }
}

function change_album(id)
{
    window.location = '/administracion/publicaciones/administrar/edit_album/'+id;
}