document.getElementById('archivo').style.display= 'none';

function update_foto(id){
    $("#id_fotografia").val(id);
    $.ajax({
        url:'/administracion/get_fotografias/'+id,
        type:'GET',
        dataType:'json',
        success:function(res){
            $(res).each(function(i, v){
                $("#titulo").val(v.titulo);
                $("#contenido").val(v.contenido);
                $("#categoria").val(v.categoria);
                $("#imagen").val(v.foto);
                $("#id_publicacion").val(v.id_publicacion);
                $("#m_update_foto").modal("show");
                //$("#ip_categoria_a").focus();
            });
        }
    })
}

function up_foto(categoria, titulo, contenido, foto, idpublicacion, idfoto, token)
{
    $.ajax({
        url:'/administracion/publicaciones/admin_picture',
        type:'POST',
        dataType:'json',
        headers :{'X-CSRF-TOKEN': token},
        data: {categoria:categoria, titulo:titulo, contenido:contenido, foto:foto, idpublicacion: idpublicacion, idfoto:idfoto},
        success:function(res){
        if(res.RES){
            alert("Publicacion Guardada");
                window.location = '/administracion/publicaciones/administrar/admin_picture';
            }else{
                alert("Error al Guardar");
            }
        }
    })
}

function delete_foto(id){
    $("#ide_fotografia").val(id);
    $.ajax({
        url:'/administracion/get_fotografias/'+id,
        type:'GET',
        dataType:'json',
        success:function(res){
            $(res).each(function(i, v){
                $("#dtitulo").val(v.titulo);
                $("#dcontenido").val(v.contenido);
                $("#d_update_foto").modal("show");
                //$("#ip_categoria_a").focus();
            });
        }
    })
}

function dl_foto()
{
    var idfoto= $('#ide_fotografia').val();
    var titulo = $('#dtitulo').val();
    var contenido = $('#dcontenido').val();
    var token = $("#token").val();
    if(titulo=="")
    {
        alert('No hay titulo');
    }
    else
    {
        $.ajax({
            url:'/administracion/publicaciones/delete_picture',
            type:'POST',
            dataType:'json',
            headers :{'X-CSRF-TOKEN': token},
            data: {idfoto:idfoto},
            success:function(res){
            if(res.RES){
                alert("Registro Eliminado");
                    window.location = '/administracion/publicaciones/administrar/admin_picture';
                }else{
                    alert("Error al Guardar");
                }
            }
        })
    }
}

function change_foto(id)
{
    $("#idc_fotografia").val(id);
    $("#c_update_foto").modal("show");
}