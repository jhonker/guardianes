function update_noticia(id){
    $("#id_noticia").val(id);
    $.ajax({
        url:'/administracion/get_noticia/'+id,
        type:'GET',
        dataType:'json',
        success:function(res){
            $(res).each(function(i, v){
                $("#titulo").val(v.titulo);
                $("#contenido").val(v.contenido);
                $("#m_update_noticia").modal("show");
                //$("#ip_categoria_a").focus();
            });
        }
    })
}

function up_noticia(idnoticia,titulo, contenido, token)
{
    $.ajax({
        url:'/administracion/publicaciones/admin_noticia',
        type:'POST',
        dataType:'json',
        headers :{'X-CSRF-TOKEN': token},
        data: {titulo:titulo, idnoticia: idnoticia, contenido:contenido},
        success:function(res){
        if(res.RES){
            alert("Noticia Actualizada");
                window.location = '/administracion/publicaciones/administrar/admin_noticia';
            }else{
                alert("Error al Actualizar");
            }
        }
    })
}

function delete_noticia(id){
    $("#ide_noticia").val(id);
    $.ajax({
        url:'/administracion/get_noticia/'+id,
        type:'GET',
        dataType:'json',
        success:function(res){
            $(res).each(function(i, v){
                $("#dtitulo").val(v.titulo);
                $("#d_update_noticia").modal("show");
                //$("#ip_categoria_a").focus();
            });
        }
    })
}

function dl_noticia()
{
    var idnoticia= $('#ide_noticia').val();
    var titulo = $('#dtitulo').val();
    var token = $("#token").val();
    if(titulo=="")
    {
        alert('No hay titulo');
    }
    else
    {
        $.ajax({
            url:'/administracion/publicaciones/delete_noticia',
            type:'POST',
            dataType:'json',
            headers :{'X-CSRF-TOKEN': token},
            data: {idnoticia:idnoticia},
            success:function(res){
            if(res.RES){
                alert("Registro Eliminado");
                    window.location = '/administracion/publicaciones/administrar/admin_noticia';
                }else{
                    alert("Error al Guardar");
                }
            }
        })
    }
}

function change_album(id)
{
    window.location = '/administracion/publicaciones/administrar/edit_noticia/'+id;
}

function change_video(id)
{
    $("#idc_video").val(id);
    $("#idce_video").val(id);
    $("#c_update_video").modal("show");
    //alert(id);
}