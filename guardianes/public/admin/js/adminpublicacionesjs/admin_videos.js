function update_video(id){
    $("#id_video").val(id);
    $.ajax({
        url:'/administracion/get_videos/'+id,
        type:'GET',
        dataType:'json',
        success:function(res){
            $(res).each(function(i, v){
                $("#titulo").val(v.titulo);
                $("#contenido").val(v.contenido);
                $("#categoria").val(v.categoria);
                $("#id_publicacion").val(v.id_publicacion);
                $("#m_update_video").modal("show");
                //$("#ip_categoria_a").focus();
            });
        }
    })
}

function up_video(categoria, titulo, contenido, idpublicacion, idvideo, token)
{
    $.ajax({
        url:'/administracion/publicaciones/admin_video',
        type:'POST',
        dataType:'json',
        headers :{'X-CSRF-TOKEN': token},
        data: {categoria:categoria, titulo:titulo, contenido:contenido, idpublicacion: idpublicacion, idvideo:idvideo},
        success:function(res){
        if(res.RES){
            alert("Publicacion Guardada");
                window.location = '/administracion/publicaciones/administrar/admin_video';
            }else{
                alert("Error al Guardar");
            }
        }
    })
}

function delete_video(id){
    $("#ide_video").val(id);
    $.ajax({
        url:'/administracion/get_videos/'+id,
        type:'GET',
        dataType:'json',
        success:function(res){
            $(res).each(function(i, v){
                $("#dtitulo").val(v.titulo);
                $("#dcontenido").val(v.contenido);
                $("#d_update_video").modal("show");
                //$("#ip_categoria_a").focus();
            });
        }
    })
}

function dl_video()
{
    var idvideo= $('#ide_video').val();
    var titulo = $('#dtitulo').val();
    var contenido = $('#dcontenido').val();
    var token = $("#token").val();
    if(titulo=="")
    {
        alert('No hay titulo');
    }
    else
    {
        $.ajax({
            url:'/administracion/publicaciones/delete_video',
            type:'POST',
            dataType:'json',
            headers :{'X-CSRF-TOKEN': token},
            data: {idvideo:idvideo},
            success:function(res){
            if(res.RES){
                alert("Registro Eliminado");
                    window.location = '/administracion/publicaciones/administrar/admin_video';
                }else{
                    alert("Error al Guardar");
                }
            }
        })
    }
}
function change_video(id)
{
    $("#idc_video").val(id);
    $("#idce_video").val(id);
    $("#c_update_video").modal("show");
}