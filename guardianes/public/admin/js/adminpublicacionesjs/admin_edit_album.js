function delete_foto(id){
    $("#id_fotografia").val(id);
    $.ajax({
        url:'/administracion/get_fotografias_album/'+id,
        type:'GET',
        dataType:'json',
        success:function(res){
            $(res).each(function(i, v){
                $("#id_album").val(v.id_album);
                $("#m_update_album").modal("show");
                //$("#ip_categoria_a").focus();
            });
        }
    })
}

function eliminar_foto(id, token)
{
    var idfoto = id;
    $.ajax({
        url:'/administracion/publicaciones/eliminar_picture',
        type:'POST',
        dataType:'json',
        headers :{'X-CSRF-TOKEN': token},
        data: {idfoto:idfoto},
        success:function(res){
        if(res.RES){
            alert("Fotografia Eliminada");
                window.location = '/administracion/publicaciones/administrar/admin_album';
            }else{
                alert("Error al Guardar");
            }
        }
    })
}
