function save_categoria(){
    var categoria = $("#ip_categoria").val();
    var token = $("#token").val();
    if(categoria==""){
        alert("El campo categoria se encuentra vacia!");
    }else{
        $.ajax({
            url:'/administracion/categoria/store',
            type:'POST',
            dataType:'json',
		    headers :{'X-CSRF-TOKEN': token},
            data:{
                categoria:categoria
            },
            success:function(res){
                    if(res.RES){
                        alert("Categoria registrada");
                        window.location = '/administracion/categorias';
                    }else{

                    }
            }
        });
    }
}

function update_categoria(id){
    $("#ip_idcategoria").val(id);
    $.ajax({
        url:'/administracion/get_categorias/'+id,
        type:'GET',
        dataType:'json',
        success:function(res){
            $(res).each(function(i, v){
                $("#ip_categoria_a").val(v.categoria);
                $("#m_update_categorias").modal("show");
                //$("#ip_categoria_a").focus();
            });
        }
    })
}

function up_categoria(){
    var id = $("#ip_idcategoria").val();
    var categoria = $("#ip_categoria_a").val();
    var token = $("#token").val();
    if(categoria == ""){
        alert("Por vafor no deje vacio el campo a actualizar")
    }else{
        $.ajax({
            url:'/administracion/update_categoria',
            type:'POST',
            dataType:'json',
		    headers :{'X-CSRF-TOKEN': token},
            data:{
                id:id,
                categoria:categoria
            },
            success:function(res){
                if(res.RES){
                    alert("Categoria actualizada");
                    window.location = '/administracion/categorias';
                }else{
                    alert("Error al actualizar");
                }
            }
        })
    }
}

function delete_categoria(id){
    $("#ipd_idcategoria").val(id);
    $.ajax({
        url:'/administracion/get_categorias/'+id,
        type:'GET',
        dataType:'json',
        success:function(res){
            $(res).each(function(i, v){
                $("#ipd_categoria_a").val(v.categoria);
                $("#d_update_categorias").modal("show");
                //$("#ip_categoria_a").focus();
            });
        }
    })
}

function dl_categoria(){
    var id = $("#ipd_idcategoria").val();
    var estado=0;
    var categoria = $("#ipd_categoria_a").val();
    var token = $("#token").val();
    if(categoria == ""){
        alert("Por vafor no deje vacio el campo a eliminar")
    }else{
        $.ajax({
            url:'/administracion/delete_categoria',
            type:'POST',
            dataType:'json',
		    headers :{'X-CSRF-TOKEN': token},
            data:{
                id:id,
                estado:estado
            },
            success:function(res){
                if(res.RES){
                    alert("Categoria eliminada");
                    window.location = '/administracion/categorias';
                }else{
                    alert("Error al eliminar");
                }
            }
        })
    }
}