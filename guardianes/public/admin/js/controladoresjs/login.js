$("#btn_login").click(function(){
	var usuario = $("#tv_usuario").val();
	var clave   = $("#tv_clave").val();

	if(usuario=="" && clave==""){
		alert("Por favor ingrese un usuario y una contraseña!");
	}else if(usuario == ""){
		alert("Por favor ingrese un usuario!");
		$("#tv_usuario").focus();
	}else if(clave  == ""){
		alert("Por favor ingrese un contraseña!");
		$("#tv_clave").focus();
	}else{
		login(usuario, clave);
	}
});

$("#tv_clave").keypress(function(tecla) {
    if (tecla.which == 13) {
       var usuario = $("#tv_usuario").val();
		var clave   = $("#tv_clave").val();
	if(usuario=="" && clave ==""){
		alert("Por favor ingrese su usuario y contraseña");
	}else if(usuario==""){
		alert("Por favor ingrese su usuario ");
		$("#tv_usuario").focus();
	}else if(clave==""){
		alert("Por favor ingrese su contraseña");
		$("#tv_clave").focus();
	}else{
		login(usuario,clave);
	}
    }
});


function login(usuario, clave){
	var token=$("#token").val();
	$.ajax({
		url:'/administracion/login',
		type:'POST',
		dataType:'json',
		headers :{'X-CSRF-TOKEN': token},
		data:{
			cedula:usuario,clave:clave
		},
		success:function(res){
			if(res.usuario==false){
				alert("nombre de usuario no existe");
				$("#tv_usuario").focus();
			}else if(res.clave==false){
				alert("La clave no es correcta para este usuario");
				$("#tv_clave").focus();
			}else if(res.result){
				alert("Bienvenido al sistema");
                    window.location = 'administracion/home';
			}
		}
	})
}