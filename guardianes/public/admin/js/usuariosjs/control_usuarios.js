function save_usuario(){
    var cedula = $("#ip_cedula").val();
    var nombres = $("#ip_usuario").val();
    var clave = $("#ip_clave").val();
    var privilegio = $("#ip_privilegio").val();
    var estado=1;
    var token = $("#token").val();
    if(nombres==""){
        alert("El campo nombre se encuentra vacia!");
    }else{
        if(cedula=="")
        {
            alert("El campo cedula se encuentra vacia!");
        }
        else
        {
            if(clave=="")
            {
                alert("El campo clave se encuentra vacia!");    
            }
            else
            {
                if(privilegio=="")
                {
                    alert("El campo privilegio se encuentra vacia!");    
                }
                else
                {
                    $.ajax({
                        url:'/administracion/usuario/store',
                        type:'POST',
                        dataType:'json',
                        headers :{'X-CSRF-TOKEN': token},
                        data:{
                            cedula:cedula, nombres:nombres, clave:clave, privilegio:privilegio, estado:estado
                        },
                        success:function(res){
                                if(res.RES){
                                    alert("Usuario registrado");
                                    window.location = '/administracion/usuarios';
                                }else{
                                    alert("No se pudo Guardar");
                                }
                        }
                    });    
                }
            }
        }
    }
}
function update_usuario(id){
    $("#ip_idusuario").val(id);
    $.ajax({
        url:'/administracion/get_usuarios/'+id,
        type:'GET',
        dataType:'json',
        success:function(res){
            $(res).each(function(i, v){
                $("#ipa_cedula").val(v.cedula);
                $("#ipa_usuario").val(v.nombre);
                $("#ipa_privilegio").val(v.privilegio);
                $("#m_update_usuarios").modal("show");
                //$("#ip_categoria_a").focus();
            });
        }
    })
}
function up_usuario(){
    var id = $("#ip_idusuario").val();
    var cedula = $("#ipa_cedula").val();
    var nombres = $("#ipa_usuario").val();
    var privilegio = $("#ipa_privilegio").val();
    var token = $("#token").val();
    if(nombres==""){
        alert("El campo nombre se encuentra vacia!");
    }else{
        if(cedula=="")
        {
            alert("El campo cedula se encuentra vacia!");
        }
        else
        {
            if(privilegio=="")
                {
                    alert("El campo privilegio se encuentra vacia!");    
                }
                else
                {
                    $.ajax({
                        url:'/administracion/update_usuario',
                        type:'POST',
                        dataType:'json',
                        headers :{'X-CSRF-TOKEN': token},
                        data:{
                            id:id, nombres:nombres, cedula:cedula, privilegio:privilegio
                        },
                        success:function(res){
                                if(res.RES){
                                    alert("Usuario actualizado");
                                    window.location = '/administracion/usuarios';
                                }else{
                                    alert("Error al actualizar");
                                }
                        }
                    });    
                }
        }
    }
}

function delete_usuario(id){
    $("#ipd_idusuario").val(id);
    $.ajax({
        url:'/administracion/get_usuarios/'+id,
        type:'GET',
        dataType:'json',
        success:function(res){
            $(res).each(function(i, v){
                $("#ipd_usuario").val(v.nombre);
                $("#d_update_usuarios").modal("show");
                //$("#ip_categoria_a").focus();
            });
        }
    })
}

function dl_usuario(){
    var id = $("#ipd_idusuario").val();
    var estado=0;
    var nombres = $("#ipd_usuario").val();
    var token = $("#token").val();
    if(nombres == ""){
        alert("Por vafor no deje vacio el campo a eliminar")
    }else{
        $.ajax({
            url:'/administracion/delete_usuario',
            type:'POST',
            dataType:'json',
		    headers :{'X-CSRF-TOKEN': token},
            data:{
                id:id,
                estado:estado
            },
            success:function(res){
                if(res.RES){
                    alert("Usuario eliminado");
                    window.location = '/administracion/usuarios';
                }else{
                    alert("Error al eliminar");
                }
            }
        })
    }
}

function generar_clave(id){
    $("#ipg_idusuario").val(id);
    $.ajax({
        url:'/administracion/get_usuarios/'+id,
        type:'GET',
        dataType:'json',
        success:function(res){
            $(res).each(function(i, v){
                $("#g_update_usuarios").modal("show");
                //$("#ip_categoria_a").focus();
            });
        }
    })
}

function guardar_clave()
{
    var id = $("#ipg_idusuario").val();
    var clave = $("#clave_usuario").val();
    //var rclave = $("#rclave_usuario").val();
    var token = $("#token").val();
    if(clave!="")
    {
        $.ajax({
            url:'/administracion/clave_usuario',
            type:'POST',
            dataType:'json',
		    headers :{'X-CSRF-TOKEN': token},
            data:{
                id:id,
                clave:clave
            },
            success:function(res){
                if(res.RES){
                    alert("Clave Guardada");
                    window.location = '/administracion/usuarios';
                }else{
                    alert("Error al Guardar");
                }
            }
        })    
    }
}