function save_publicacion()
{
    var categoria = $('#categoria').val();
    var titulo = $('#titulo').val();
    var contenido = $('#contenido').val();
    var token = $("#token").val();
    var img = $('#archivo').val();
    var datos  = new FormData($("#form")[0]);
    if(categoria==0)
    {
        alert('Seleccione una Categoria');
    }
    else
    {
        if(titulo=="")
        {
            alert('Ingrese un titulo');
        }
        else
        {
            if(img=="")
            {
                alert('No ha subido alguna imagen');
            }
            else{
            $.ajax({
                url:'/administracion/publicaciones/store_picture',
                type:'POST',
                dataType:'json',
                headers :{'X-CSRF-TOKEN': token},
                data: datos,
                contentType: false,
                processData: false,
                success:function(res){
                    if(res.RES){
                        alert("Publicacion Guardada");
                        window.location = '/administracion/publicaciones';
                    }else{
                        alert("Error al Guardar");
                    }
                }
            })
            }
        }
    }
    
}

function save_concurso()
{
    var categoria = $('#categoriac').val();
    var contenido = $('#contenidocon').val();
    var token = $("#token").val();
    var img = $('#archivoc').val();
    var datos  = new FormData($("#formconcurso")[0]);
    if(categoria=='0')
    {
        alert('No ha seleccionado la categoria');
    }
    else{
        if(contenido=='')
        {
            alert('No ha ingresado un contenido')
        }
        else{
            if(img=='')
            {
                alert('No ha subido alguna imagen');
            }
            else{
                $.ajax({
                    url:'/administracion/publicaciones/store_concurso',
                    type:'POST',
                    dataType:'json',
                    headers :{'X-CSRF-TOKEN': token},
                    data: datos,
                    contentType: false,
                    processData: false,
                    success:function(res){
                        if(res.RES){
                            alert("Concurso Guardado");
                            window.location = '/administracion/publicaciones';
                        }else{
                            alert("Error al Guardar");
                        }
                    }
                })
            }
        }
    }
}