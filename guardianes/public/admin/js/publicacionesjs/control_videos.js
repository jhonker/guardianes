function save_video()
{
    var categoria = $('#categoria').val();
    var titulo = $('#titulo').val();
    var contenido = $('#contenid').val();
    var token = $("#token").val();
    var enlace = $('#enlace').val();
    var video = $('#archivo').val();
    var datos  = new FormData($("#form")[0]);
    if(enlace=="")
    {
        //alert('enlace vacio');
        if(categoria==0)
        {
            alert('Seleccione una Categoria');
        }
        else
        {
            if(titulo=="")
            {
                alert('Ingrese un titulo');
            }
            else
            {
                if(video=="")
                {
                    alert('No ha subido algun video');
                }
                else{
                    $("#load").show();
                   $.ajax({
                    url:'/administracion/publicaciones/store_video',
                    type:'POST',
                    dataType:'json',
                    headers :{'X-CSRF-TOKEN': token},
                    data: datos,
                    contentType: false,
                    processData: false,
                    success:function(res){
                        document.getElementById('load').style.display='none';
                        if(res.RES){
                            alert("Video Guardado");
                            window.location = '/administracion/publicaciones';
                        }else{
                            alert("Error al Guardar");
                        }
                    }
                })   
                }
            }
        }
    }else{
        //alert('no vacio');
        if(categoria==0)
        {
            alert('Seleccione una Categoria');
        }
        else
        {
            if(titulo=="")
            {
                alert('Ingrese un titulo');
            }
            else
            {
                if(contenido=="")
                {
                    alert('Ingrese un contenido');    
                }
                else
                {
                    $.ajax({
                        url:'/administracion/publicaciones/store_video_enlace',
                        type:'POST',
                        dataType:'json',
                        headers :{'X-CSRF-TOKEN': token},
                        data: datos,
                        contentType: false,
                        processData: false,
                        success:function(res){
                            if(res.RES){
                                alert("Video Guardado");
                                window.location = '/administracion/publicaciones';
                            }else{
                                alert("Error al Guardar");
                            }
                        }
                    })    
                }
            }
        }
    }
}
