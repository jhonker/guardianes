function save_album()
{
    var titulo = $('#titulo').val();
    var token = $("#token").val();
    var img = $('#archivo').val();
    //var img = 1;
    var datos  = new FormData($("#form")[0]);
    if(titulo=="")
    {
        alert('Ingrese un titulo');
    }
    else
    {
        if(img=="")
        {
            alert('Seleccione imagenes');
        }
        else
        {
            $.ajax({
            url:'/administracion/publicaciones/store_album',
            type:'POST',
            dataType:'json',
		    headers :{'X-CSRF-TOKEN': token},
            data: datos,
            contentType: false,
            processData: false,
            success:function(res){
                if(res.RES){
                    alert("Publicacion Guardada");
                    window.location = '/administracion/publicaciones';
                }else{
                    alert("Error al Guardar");
                }
            }
        })      
        }
    }
}