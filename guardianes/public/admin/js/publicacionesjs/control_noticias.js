function save_noticia()
{
    var titulo = $('#titulonoticia').val();
    var contenido= $('#contenidonoticia')
    var token = $("#token").val();
    var img = $('#archivo').val();
    //var img = 1;
    var datos  = new FormData($("#form")[0]);
    if(titulo=="")
    {
        alert('Ingrese un titulo');
    }
    else
    {
        if(img=="")
        {
            alert('Seleccione imagenes');
        }
        else
        {
            $.ajax({
            url:'/administracion/publicaciones/store_noticia',
            type:'POST',
            dataType:'json',
		    headers :{'X-CSRF-TOKEN': token},
            data: datos,
            contentType: false,
            processData: false,
            success:function(res){
                if(res.RES){
                    alert("Publicacion Guardada");
                    window.location = '/administracion/publicaciones';
                }else{
                    alert("Error al Guardar");
                }
            }
        })      
        }
    }
}

function save_noticia_enlace()
{
    var titulo = $('#titulonoticia').val();
    var contenido= $('#contenidonoticia').val();
    var token = $("#token").val();
    var enlace = $('#enlacen').val();
    //var img = 1;
    var datos  = new FormData($("#form")[0]);
    if(titulo=="")
    {
        alert('Ingrese un titulo');
    }
    else
    {
        if(enlace=="")
        {
            alert('Ingrese un enlace');
        }
        else
        {
            if(contenido=="")
            {
                alert('Ingrese un contenido');
            }
            else
            {
                //alert(titulo); alert(contenido); alert(enlace);
                $.ajax({
                    url:'/administracion/publicaciones/store_noticia_enlace',
                    type:'POST',
                    dataType:'json',
                    headers :{'X-CSRF-TOKEN': token},
                    data: datos,
                    contentType: false,
                    processData: false,
                    success:function(res){
                        if(res.RES){
                            alert("Publicacion Guardada");
                            window.location = '/administracion/publicaciones';
                        }else{
                            alert("Error al Guardar");
                        }
                    }
                }) 
            }
           /* */     
        }
    }
}

function save_noticia_video()
{
    var titulo = $('#titulonoticia').val();
    var contenido = $('#contenidonoticia').val();
    var token = $("#token").val();
    var video = $('#archivov').val();
    var datos  = new FormData($("#form")[0]);
    if(titulo=="")
    {
        alert('Ingrese un titulo');
    }
    else
    {
        if(contenido=="")
        {
            alert('Ingrese un contenido');
        }
        else
        {
            if(video=="")
            {
                alert('No ha subido algun video');
            }
            else{
                $.ajax({
                    url:'/administracion/publicaciones/store_noticia_video',
                    type:'POST',
                    dataType:'json',
                    headers :{'X-CSRF-TOKEN': token},
                    data: datos,
                    contentType: false,
                    processData: false,
                    success:function(res){
                        if(res.RES){
                            alert("Video Guardado");
                            window.location = '/administracion/publicaciones';
                        }else{
                            alert("Error al Guardar");
                        }
                    }
                })
            }
        }
    }
}