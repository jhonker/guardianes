<div>
        <nav class="navbar navbar-expand-lg navbar-light">
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarSupportedContent">
              <ul class="nav navbar-nav mr-auto">
                <li class="nav-item active">
                  <a class="nav-link" href="/">
                    <figure>    
                      <img src="{{asset('img/1.png')}}" alt="">
                      <div class="t_imagen"></div>
                    </figure>
                  </a>  
                </li>
                <li class="nav-item">
                  <a class="nav-link" href="/personajes">
                    <figure>    
                      <img src="{{asset('img/2.png')}}" alt="">
                      <!--<span>Personajes</span>-->
                      <div class="t_imagen_personajes"></div>
                    </figure>
                  </a>
                </li>
                <!--<li class="nav-item">
                  <a class="nav-link" href="/personajes">
                    <figure>    
                      <img src="{{asset('img/2.png')}}" alt="">
                      <div class="t_imagen_personajes"></div>
                    </figure>
                  </a>
                </li>-->
                <li class="nav-item">
                  <a class="nav-link" href="/proceso">
                    <figure>    
                      <img src="{{asset('img/3.png')}}" alt="">
                      <div class="t_imagen_proceso"></div>
                    </figure>
                  </a>
                </li>
                <li class="nav-item">
                  <a class="nav-link" href="/gotinoticia">
                    <figure>    
                      <img src="{{asset('img/4.png')}}" alt="">
                      <div class="t_imagen_noticias"></div>
                    </figure>
                  </a>
                </li>
                <li class="nav-item">
                  <a class="nav-link" href="/gotiahorro">
                    <figure>    
                      <img src="{{asset('img/5.png')}}" alt="">
                      <div class="t_imagen_ahorro"></div>

                    </figure>
                  </a>
                </li>
                <li class="nav-item">
                  <a class="nav-link" href="/gotivideo">
                    <figure>    
                      <img src="{{asset('img/6.png')}}" alt="">
                      <div class="t_imagen_videos"></div>
                    </figure>
                  </a>
                </li>
                <li class="nav-item">
                  <a class="nav-link" href="/gotigaleria">
                    <figure>    
                      <img src="{{asset('img/7.png')}}" alt="">
                      <div class="t_imagen_galeria"></div>

                    </figure>
                  </a>
                </li>
                <li class="nav-item">
                  <a class="nav-link" href="/descarga">
                    <figure>    
                      <img src="{{asset('img/8.png')}}" alt="">
                      <div class="t_imagen_descarga"></div>

                    </figure>
                  </a>
                </li>
                <li class="nav-item">
                  <a class="nav-link" href="/concurso">
                    <figure>    
                      <img src="{{asset('img/9.png')}}" alt="">
                      <div class="t_imagen_concurso"></div>

                    </figure>
                  </a>
                </li>
    </ul>
  </div>
</nav>
    </div>