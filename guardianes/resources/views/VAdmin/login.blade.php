<!DOCTYPE html>
<html lang="en" class="body-full-height">
    <head>        
        <!-- META SECTION -->
        <title>Guardianes Admin</title>            
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        
        <link rel="icon" href="favicon.ico" type="image/x-icon" />
        <!-- END META SECTION -->
        
        <!-- CSS INCLUDE -->        
        <link rel="stylesheet" type="text/css" id="theme" href="{{asset('admin/css/theme-default.css')}}"/>
        <!-- EOF CSS INCLUDE -->                                    
    </head>
    <body>
    <input type="hidden" name="_token" value="{{csrf_token()}}" id="token">

        <div class="login-container">
        
            <div class="login-box animated fadeInDown">
                <div class="login-logo"></div>
                <div class="login-body">
                    <div class="login-title"><strong>Bienvenido</strong>, Por favor ingrese</div>
                    <div class="form-horizontal" >
                    <div class="form-group">
                        <div class="col-md-12">
                            <input type="text" id="tv_usuario" class="form-control" placeholder="Usuario"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-md-12">
                            <input type="password" id="tv_clave" class="form-control" placeholder="Contraseña"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-md-6">
                          <div class="pull-left text_white">
                        	&copy; 2019 Guardianes del Agua
                    		</div>
                        </div>
                        <div class="col-md-6">
                            <button id="btn_login" class="btn btn-info btn-block">Ingresar</button>
                        </div>
                    </div>
                    </div>
                </div>
                
            </div>
          
            
        </div>
        
	<script src="../js/jquery.min.js"></script>
	<script src="{{asset('admin/js/controladoresjs/login.js')}}"></script>
    </body>

</html>






