@extends('Layouts.Admin_app')

@section('css')
<link rel="stylesheet" href="{{asset('admin/dropify/dist/css/dropify.css')}}"/>

@endsection



@section('menu')
<li >
                        <a href="/administracion/home"><span class="fa fa-desktop"></span> <span class="xn-text">Inicio</span></a>                        
                    </li>   
                    <li class="xn-openable active">
                        <a href="#"><span class="fa fa-file-text-o"></span> <span class="xn-text">Publicaciones</span></a>
                        <ul>
                            <li><a href="/administracion/publicaciones">Nueva publicación</a></li>
                            <li class="active"><a href="/administracion/publicaciones/administrar">Administrar</a></li>
                        </ul>
                    </li>
                    <li class="xn-title">Configuraciones</li>
                    <li>
                        <a href="/administracion/usuarios"><span class="fa fa-user"></span> <span class="xn-text">Usuarios</span></a>
                    </li>    
                    <li>
                        <a href="/administracion/categorias"><span class="fa fa-bars"></span> <span class="xn-text">Categorias</span></a>
                    </li>          
@endsection
@section('content')
<div class="row">
<div class="col-md-12">

<!-- START DEFAULT DATATABLE -->
<div class="panel panel-default">
    <div class="panel-heading">                                
        <h3 class="panel-title">Albums</h3>
        <!--<ul class="panel-controls">
            <li><a href="#" class="" data-toggle="modal" data-target="#exampleModal"><span class="fa fa-plus"></span></a></li>
        </ul>-->                             
    </div>
    <div class="panel-body">
        <table class="table datatable">
            <thead>
                <tr>
                    <th>#</th>
                    <th>Titulo</th>
                    <th># FOTOS</th>
                    <th>:::</th>
                </tr>
            </thead>
            <?php
             $cont=1;
             $cont2=0;
            ?>
            <tbody>
               @foreach($albumes as $a)
                <tr>
                    <td><?php echo $cont; ?></td>
                    <td>{{$a->titulo}}</td>
                    @foreach($album as $al)
                        @if($a->id==$al->id_album)
                            <?php $cont2++; ?>
                        @endif
                    @endforeach
                    <td>{{$cont2}}</td>
                    <?php $cont2=0; ?>
                    <td><div class="btn-group">
                            <a href="#" data-toggle="dropdown" class="btn btn-primary dropdown-toggle">Opciones <span class="caret"></span></a>
                                <ul class="dropdown-menu" role="menu">
                                    <li><a href="#" onclick="update_album({{$a->id}})">Actualizar Contenido</a></li>
                                    <li><a href="#" onclick="change_album({{$a->id}})">Cambiar fotografias</a></li>
                                    <li><a href="#" onclick="delete_album({{$a->id}});">Eliminar</a></li>                     
                                </ul>
                        </div></td>
                </tr>
                <?php $cont++; ?>
                @endforeach
            </tbody>
        </table>
    </div>
</div>
<!-- END DEFAULT DATATABLE -->


</div>  
</div>

<!-- Modal -->
<div class="modal fade" id="m_update_album" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
 <!--<form id="form" method="post" action="" enctype="multipart/form-data">-->
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
        <h5 class="modal-title" id="exampleModalLabel">Actualizar Album</h5>
      </div>
      <div class="modal-body">
        <div class="row">
            <div class="form-group">
                <input type="hidden" id="id_album">
                <label for="">Titulo:</label>
                <input type="text" id="titulo" class="form-control" placeholder="Ingrese el titulo">
                <br>
            </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
        <button type="button" class="btn btn-primary" id="btn_guardar" >Guardar cambios</button>
      </div>
    </div>
  </div>
    <!--</form>-->
</div>

<!-- Modal -->
<div class="modal fade" id="d_update_album" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
        <h5 class="modal-title" id="exampleModalLabel">Eliminar Registro</h5>
      </div>
      <div class="modal-body">
        <div class="row">
            <div class="form-group">
                <input type="hidden" id="ide_album">
                <label for="">Titulo:</label>
                <input type="text" id="dtitulo" class="form-control" placeholder="Ingrese el titulo">
                <br>
            </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
        <button type="button" class="btn btn-danger" onclick="dl_album()">Eliminar</button>
      </div>
    </div>
  </div>
</div>

@endsection

@section('js')
<script type="text/javascript" src="{{asset('admin/js/plugins/datatables/jquery.dataTables.min.js')}}"></script>    
<script type="text/javascript" src="{{asset('admin/js/adminpublicacionesjs/admin_albums.js')}}"></script>
<script type="text/javascript">
    $('#btn_guardar').click(function(){
        //alert('funciona');
        var idalbum = $('#id_album').val();
        var titulo = $('#titulo').val();
        var token = $("#token").val();
        if(titulo=="")
        {
            alert('Ingrese un titulo');
        }
        else
        {
            up_album(idalbum,titulo, token);
        }
    });
</script>
@endsection