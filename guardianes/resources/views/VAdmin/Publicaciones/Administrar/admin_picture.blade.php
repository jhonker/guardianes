@extends('Layouts.Admin_app')

@section('css')
<link rel="stylesheet" href="{{asset('admin/dropify/dist/css/dropify.css')}}"/>

@endsection



@section('menu')
<li >
                        <a href="/administracion/home"><span class="fa fa-desktop"></span> <span class="xn-text">Inicio</span></a>                        
                    </li>      
                    <li class="xn-openable active">
                        <a href="#"><span class="fa fa-file-text-o"></span> <span class="xn-text">Publicaciones</span></a>
                        <ul>
                            <li><a href="/administracion/publicaciones">Nueva publicación</a></li>
                            <li class="active"><a href="/administracion/publicaciones/administrar">Administrar</a></li>
                        </ul>
                    </li>
                    <li class="xn-title">Configuraciones</li>
                    <li>
                        <a href="/administracion/usuarios"><span class="fa fa-user"></span> <span class="xn-text">Usuarios</span></a>
                    </li>    
                    <li>
                        <a href="/administracion/categorias"><span class="fa fa-bars"></span> <span class="xn-text">Categorias</span></a>
                    </li>          
@endsection
@section('content')
<div class="row">
<div class="col-md-12">

<!-- START DEFAULT DATATABLE -->
<div class="panel panel-default">
    <div class="panel-heading">                                
        <h3 class="panel-title">Fotografias</h3>
        <!--<ul class="panel-controls">
            <li><a href="#" class="" data-toggle="modal" data-target="#exampleModal"><span class="fa fa-plus"></span></a></li>
        </ul>-->                             
    </div>
    <div class="panel-body">
        <table class="table datatable">
            <thead>
                <tr>
                    <th>#</th>
                    <th>Categoria</th>
                    <th>Titulo</th>
                    <th>Contenido</th>
                    <th>Link Imagen</th>
                    <th>:::</th>
                </tr>
            </thead>
            <?php
             $cont=1;
            ?>
            <tbody>
               @foreach($adminpicture as $a)
                <tr>
                    <td><?php echo $cont; ?></td>
                    <td>{{$a->categoria}}</td>
                    <td>{{$a->titulo}}</td>
                    @if($a->contenido=='')
                    <th>----</th>
                    @else
                    <td>{{$a->contenido}}</td>
                    @endif
                    <td>{{$a->foto}}</td>
                    <td><div class="btn-group">
                            <a href="#" data-toggle="dropdown" class="btn btn-primary dropdown-toggle">Opciones <span class="caret"></span></a>
                                <ul class="dropdown-menu" role="menu">
                                    <li><a href="#" onclick="update_foto({{$a->id}})">Actualizar Contenido</a></li>
                                    <li><a href="#" onclick="change_foto({{$a->id}})">Cambiar Imagen</a></li>
                                    <li><a href="#" onclick="delete_foto({{$a->id}});">Eliminar</a></li>                     
                                </ul>
                        </div></td>
                </tr>
                <?php $cont++; ?>
                @endforeach
            </tbody>
        </table>
    </div>
</div>
<!-- END DEFAULT DATATABLE -->


</div>  
</div>

<!-- Modal -->
<div class="modal fade" id="m_update_foto" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
 <!--<form id="form" method="post" action="" enctype="multipart/form-data">-->
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
        <h5 class="modal-title" id="exampleModalLabel">Actualizar Foto</h5>
      </div>
      <div class="modal-body">
        <div class="row">
            <div class="form-group">
                <input type="hidden" id="id_publicacion">
                <input type="hidden" id="id_fotografia">
                <label for="">Titulo:</label>
                <input type="text" id="titulo" class="form-control" placeholder="Ingrese el titulo">
                <br>
                <label for="">Contenido:</label>
                <input type="text" id="contenido" class="form-control" placeholder="Ingrese el contenido">
                <br>
                <label for="">Categoria</label>
                <select name="" id="categoria" class="form-control">
                    <option value="0">Seleccione una categoria</option>
                     @foreach($categorias as $d)
                    <option value="{{$d->id}}">{{$d->categoria}}</option>
                    @endforeach
                </select><br>
                <input type="hidden" id="imagen" name="imagen" class="form-control"> 
                <input type="hidden" class="dropify" name="archivo" id="archivo">
                <br>
                <button class="btn btn-primary" id="change" onclick="cambiar()">Cambiar Foto</button>
                <button class="btn btn-danger" id="cancel" onclick="cancelar()">Cancelar</button>    
            </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
        <button type="button" class="btn btn-primary" id="btn_guardar" >Guardar cambios</button>
      </div>
    </div>
  </div>
    <!--</form>-->
</div>

<!-- Modal -->
<div class="modal fade" id="d_update_foto" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
        <h5 class="modal-title" id="exampleModalLabel">Eliminar Registro</h5>
      </div>
      <div class="modal-body">
        <div class="row">
            <div class="form-group">
                <input type="hidden" id="ide_fotografia">
                <label for="">Titulo:</label>
                <input type="text" id="dtitulo" class="form-control" placeholder="Ingrese el titulo">
                <br>
                <label for="">Contenido:</label>
                <input type="text" id="dcontenido" class="form-control" placeholder="Ingrese el contenido">
                <br>
            </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
        <button type="button" class="btn btn-danger" onclick="dl_foto()">Eliminar</button>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="c_update_foto" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <form id="form" method="post" action="" enctype="multipart/form-data">
   <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
        <h5 class="modal-title" id="exampleModalLabel">Cambiar Imagen</h5>
      </div>
      <div class="modal-body">
        <div class="row">
            <div class="form-group">
                <input type="hidden" name="idc_fotografia" id="idc_fotografia">
                <label for="">Fotografia:</label>
                <br>
                <input type="file" class="dropify" name="archivo" id="archivo">
            </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
        <button type="button" class="btn btn-primary" id="btn_change">Guardar Cambios</button>
      </div>
    </div>
  </div>
  </form>
</div>
@endsection

@section('js')
<script type="text/javascript" src="{{asset('admin/js/plugins/datatables/jquery.dataTables.min.js')}}"></script>    
<script type="text/javascript" src="{{asset('admin/js/adminpublicacionesjs/admin_publicaciones.js')}}"></script>
<script type="text/javascript">
    document.getElementById('cancel').style.display= 'none';
     document.getElementById('change').style.display= 'none';
    function cambiar()
    {
        document.getElementById('imagen').style.display= 'none';
        $('#archivo').show();
        $('#cancel').show();
    }
    function cancelar()
    {
        var ar="";
        $('#archivo').val(ar);
        document.getElementById('cancel').style.display= 'none';
        document.getElementById('archivo').style.display= 'none';
        $('#imagen').show();
    }
    $('#archivo').click(function(){
        var ar="";
        $('#imagen').val(ar);
    });
    $('#btn_change').click(function(){
        //alert('funciona');
        //change_foto();
        var idfoto= $('#idc_fotografia').val();
        //alert(idfoto);
        var token = $("#token").val();
        var datos  = new FormData($("#form")[0]);
        $.ajax({
            url:'/administracion/publicaciones/admin_picture_dos',
            type:'POST',
            dataType:'json',
            headers :{'X-CSRF-TOKEN': token},
            data: datos,
            contentType: false,
            processData: false,
            success:function(res){
            if(res.RES){
                alert("Publicacion Guardada");
                    window.location = '/administracion/publicaciones/administrar/admin_picture';
                }else{
                    alert("Error al Guardar");
                }
            }
        })
    });
    $('#btn_guardar').click(function(){
        //alert('funciona');
        var foto;
        var idpublicacion = $('#id_publicacion').val();
        var idfoto= $('#id_fotografia').val();
        var categoria = $('#categoria').val();
        var titulo = $('#titulo').val();
        var contenido = $('#contenido').val();
        var token = $("#token").val();
        var archivo = $('#archivo').val();
        var img = $('#imagen').val();
        if(img!="")
        {
            foto=img;
            if(categoria==0)
            {
                alert('Seleccione una categoria');
            }
            else
            {
                if(titulo=="")
                {alert('Ingrese un titulo');}
                else
                {
                    up_foto(categoria, titulo, contenido, foto, idpublicacion, idfoto, token);
                }
            }
        }
    });
</script>
@endsection