@extends('Layouts.Admin_app')

@section('css')
<link rel="stylesheet" href="{{asset('admin/dropify/dist/css/dropify.css')}}"/>

@endsection



@section('menu')
<li >
                        <a href="/administracion/home"><span class="fa fa-desktop"></span> <span class="xn-text">Inicio</span></a>                        
                    </li>   
                    <li class="xn-openable active">
                        <a href="#"><span class="fa fa-file-text-o"></span> <span class="xn-text">Publicaciones</span></a>
                        <ul>
                            <li><a href="/administracion/publicaciones">Nueva publicación</a></li>
                            <li class="active"><a href="/administracion/publicaciones/administrar">Administrar</a></li>
                        </ul>
                    </li>
                    <li class="xn-title">Configuraciones</li>
                    <li>
                        <a href="/administracion/usuarios"><span class="fa fa-user"></span> <span class="xn-text">Usuarios</span></a>
                    </li>    
                    <li>
                        <a href="/administracion/categorias"><span class="fa fa-bars"></span> <span class="xn-text">Categorias</span></a>
                    </li>          
@endsection
@section('content')
<div class="row">
<div class="col-md-12">

<!-- START DEFAULT DATATABLE -->
<div class="panel panel-default">
    <div class="panel-heading">                                
        <h3 class="panel-title">Noticias</h3>
        <!--<ul class="panel-controls">
            <li><a href="#" class="" data-toggle="modal" data-target="#exampleModal"><span class="fa fa-plus"></span></a></li>
        </ul>-->                             
    </div>
    <div class="panel-body">
        <table class="table datatable">
            <thead>
                <tr>
                    <th>#</th>
                    <th>Titulo</th>
                    <th>Contenido</th>
                    <th># FOTOS</th>
                    <th>Link Video</th>
                    <th>:::</th>
                </tr>
            </thead>
            <?php
             $cont=1;
             $cont2=0;
            ?>
            <tbody>
               @foreach($albumes as $a)
                <tr>
                    <td><?php echo $cont; ?></td>
                    <td>{{$a->titulo}}</td>
                    @foreach($album as $al)
                        @if($a->id==$al->id_noticia)
                            <?php $cont2++; ?>
                        @endif
                    @endforeach
                    <td style="text-align: justify;">{{$a->contenido}}</td>
                    <td>{{$cont2}}</td>
                    <td>------</td>
                    <?php $cont2=0; ?>
                    <td><div class="btn-group">
                            <a href="#" data-toggle="dropdown" class="btn btn-primary dropdown-toggle">Opciones <span class="caret"></span></a>
                                <ul class="dropdown-menu" role="menu">
                                    <li><a href="#" onclick="update_noticia({{$a->id}})">Actualizar Contenido</a></li>
                                    <li><a href="#" onclick="change_album({{$a->id}})">Cambiar fotografias</a></li>
                                    <li><a href="#" onclick="delete_noticia({{$a->id}});">Eliminar</a></li>                     
                                </ul>
                        </div></td>
                </tr>
                <?php $cont++; ?>
                @endforeach
                @foreach($adminvideo as $a)
                <tr>
                    <td><?php echo $cont; ?></td>
                    <td>{{$a->titulo}}</td>
                    @if($a->contenido=='')
                    <th>----</th>
                    @else
                    <td>{{$a->contenido}}</td>
                    @endif
                    <td>----</td>
                    <td  WIDTH="50">{{$a->link}}</td>
                    <td><div class="btn-group">
                            <a href="#" data-toggle="dropdown" class="btn btn-primary dropdown-toggle">Opciones <span class="caret"></span></a>
                                <ul class="dropdown-menu" role="menu">
                                    <li><a href="#" onclick="update_noticia({{$a->idnoticia}})">Actualizar Contenido</a></li>
                                    <li><a href="#" onclick="change_video({{$a->id}})">Cambiar Video</a></li>
                                    <li><a href="#" onclick="delete_noticia({{$a->idnoticia}});">Eliminar</a></li>                     
                                </ul>
                        </div></td>
                </tr>
                <?php $cont++; ?>
                @endforeach
            </tbody>
        </table>
    </div>
</div>
<!-- END DEFAULT DATATABLE -->


</div>  
</div>

<!-- Modal -->
<div class="modal fade" id="m_update_noticia" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
 <!--<form id="form" method="post" action="" enctype="multipart/form-data">-->
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
        <h5 class="modal-title" id="exampleModalLabel">Actualizar Noticia</h5>
      </div>
      <div class="modal-body">
        <div class="row">
            <div class="form-group">
                <input type="hidden" id="id_noticia">
                <label for="">Titulo:</label>
                <input type="text" id="titulo" class="form-control" placeholder="Ingrese el titulo">
                <br>
                <label for="">Contenido:</label>
                <textarea id="contenido" name="contenido" class="form-control" cols="20" rows="5"></textarea>
                <br>
            </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
        <button type="button" class="btn btn-primary" id="btn_guardar" >Guardar cambios</button>
      </div>
    </div>
  </div>
    <!--</form>-->
</div>

<!-- Modal -->
<div class="modal fade" id="d_update_noticia" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
        <h5 class="modal-title" id="exampleModalLabel">Eliminar Registro</h5>
      </div>
      <div class="modal-body">
        <div class="row">
            <div class="form-group">
                <input type="hidden" id="ide_noticia">
                <label for="">Titulo:</label>
                <input type="text" id="dtitulo" class="form-control" placeholder="Ingrese el titulo" >
                <br>
            </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
        <button type="button" class="btn btn-danger" onclick="dl_noticia()">Eliminar</button>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="c_update_video" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  
   <div class="modal-dialog" role="document">
   <div class="modal-content" id="modalseleccion">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
        <h5 class="modal-title" id="exampleModalLabel">Cambiar Video</h5>
      </div>
      <div class="modal-body">
       <div class="row">
           <label for="">¿Desea subir un nuevo video o un nuevo enlace?</label>
               <br>
                <label class="check">
                    <input name="check" id="check" value="1" type="checkbox" class="icheckbox" onchange="javascript:mostrar()"> Video
                </label> &nbsp;
                <label class="check">
                    <input name="check1" id="check1" value="1" type="checkbox" class="icheckbox"/> Enlace
                </label>
                <br><br>
                <button class="btn btn-primary" id="btn_siguiente" onclick="siguiente()">Seleccionar</button>
       </div>
   </div>
    </div>
    <div class="modal-content" id="modalvideo">
        <form id="form" method="post" action="" enctype="multipart/form-data">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
        <h5 class="modal-title" id="exampleModalLabel">Cambiar Video</h5>
      </div>
      <div class="modal-body">
        <div class="row">
            <div class="form-group">
                <input type="hidden" name="idc_video" id="idc_video">
                <label for="">Video:</label>
                <br>
                <input type="file" class="dropify" name="archivoev" id="archivoev">
            </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
        <button type="button" class="btn btn-primary" id="btn_change">Guardar Cambios</button>
      </div>
  </form>
    </div>
    
    <div class="modal-content" id="modalenlace">
        <form id="form1" method="post" action="" enctype="multipart/form-data">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
        <h5 class="modal-title" id="exampleModalLabel">Cambiar Video</h5>
      </div>
      <div class="modal-body">
        <div class="row">
            <div class="form-group">
                <input type="hidden" name="idce_video" id="idce_video">
                <label for="">Enlace de Video:</label>
                <br>
                <textarea id="contenid" name="contenid" class="form-control" cols="20" rows="5"></textarea>
            </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
        <button type="button" class="btn btn-primary" id="btn_change_enlace">Guardar Cambios</button>
      </div>
  </form>
    </div>
    
  </div>
</div>

@endsection

@section('js')
<script type="text/javascript" src="{{asset('admin/js/plugins/datatables/jquery.dataTables.min.js')}}"></script>    
<script type="text/javascript" src="{{asset('admin/js/adminpublicacionesjs/admin_noticias.js')}}"></script>
<script type="text/javascript">
    document.getElementById('modalvideo').style.display= 'none';
    document.getElementById('modalenlace').style.display= 'none';
    $('#btn_guardar').click(function(){
        //alert('funciona');
        var idnoticia = $('#id_noticia').val();
        var titulo = $('#titulo').val();
        var contenido= $('#contenido').val();
        var token = $("#token").val();
        if(titulo=="")
        {
            alert('Ingrese un titulo');
        }
        else
        {
            if(contenido=="")
            {

            }
            else
            {
                //alert(idnoticia+' '+titulo+' '+contenido);
                up_noticia(idnoticia,titulo, contenido, token);
            }
        }
    });

    function siguiente()
    {
        if( $('#check').prop('checked') ) {
            if( $('#check1').prop('checked') ) {
                alert('Seleccione solo una opcion');
            }
            else
            {
                document.getElementById('modalseleccion').style.display= 'none';
                $('#modalvideo').show();
            }
        }
        else
        {
             if( $('#check1').prop('checked') ) {
                document.getElementById('modalseleccion').style.display= 'none';
                $('#modalenlace').show();
            }
            else
            {
               alert('Seleccione una opcion'); 
            }
        }
            
    }
    $('#btn_change_enlace').click(function(){
        //alert('funciona');
        var contenido = $('#contenid').val();
        if(contenido!="")
        {
            var token = $("#token").val();
            var datos  = new FormData($("#form1")[0]);
            $.ajax({
                url:'/administracion/publicaciones/admin_video_dos_enlace_noticia',
                type:'POST',
                dataType:'json',
                headers :{'X-CSRF-TOKEN': token},
                data: datos,
                contentType: false,
                processData: false,
                success:function(res){
                if(res.RES){
                    alert("Enlace Guardado");
                        window.location = '/administracion/publicaciones/administrar/admin_noticia';
                    }else{
                        alert("Error al Guardar");
                    }
                }
            })
        }
        else
        {
            alert('Ingrese un contenido');
        }
    })
    
    $('#btn_change').click(function(){
        //alert('funciona');
        //alert(idfoto);
        var token = $("#token").val();
        var datos  = new FormData($("#form")[0]);
        $.ajax({
            url:'/administracion/publicaciones/admin_video_dos_n_enlace',
            type:'POST',
            dataType:'json',
            headers :{'X-CSRF-TOKEN': token},
            data: datos,
            contentType: false,
            processData: false,
            success:function(res){
            if(res.RES){
                alert("Video Guardado");
                    window.location = '/administracion/publicaciones/administrar/admin_noticia';
                }else{
                    alert("Error al Guardar");
                }
            }
        })
    });
</script>
@endsection