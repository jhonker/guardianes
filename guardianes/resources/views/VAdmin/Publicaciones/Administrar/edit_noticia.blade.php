@extends('Layouts.Admin_app')

@section('css')
<link rel="stylesheet" href="{{asset('admin/dropify/dist/css/dropify.css')}}"/>
<link rel="stylesheet" href="{{asset('krajee/css/fileinput.css')}}"/>

@endsection



@section('menu')
<li >
    <a href="/administracion/home"><span class="fa fa-desktop"></span> <span class="xn-text">Inicio</span></a>                        
</li>                    
                    <li class="xn-openable active">
                        <a href="#"><span class="fa fa-file-text-o"></span> <span class="xn-text">Publicaciones</span></a>
                        <ul>
                            <li><a href="/administracion/publicaciones">Nueva publicación</a></li>
                            <li><a href="/administracion/publicaciones/administrar">Administrar</a></li>
                        </ul>
                    </li>
                    <li class="xn-title">Configuraciones</li>
                    <li>
                        <a href="/administracion/usuarios"><span class="fa fa-user"></span> <span class="xn-text">Usuarios</span></a>
                    </li>    
                    <li >
                        <a href="/administracion/categorias"><span class="fa fa-bars"></span> <span class="xn-text">Categorias</span></a>
                    </li>          
@endsection
@section('content')
    <!-- START WIDGETS --> 
    <input type="hidden" value="{{$id}}">
    <div class="row">
       <div class="col-md-12">
        <button type="button" class="btn btn-danger" id="regresar">Regresar</button>
        </div>
    </div>
    <br>   
    <div class="row" id="seleccion">
        <div class="col-md-12">
            <div class="panel panel-default">
            <div class="panel-body">
                <label for="">Titulo del Album:</label>
                @foreach($album as $a)
                <input id="titulo" name="titulo" type="text" value="{{$a->titulo}}" class="form-control" placeholder="Ingrese un titulo a la fotografia"/>
                @endforeach
                <br>
                <h3><span class="fa fa-file-image-o"></span> Fotografias</h3>
                    <div class="col-12 grid-margin">
                        <label for="">¿Desea eliminar fotografias o agregas mas fotos?</label>
                           <br>
                            <label class="check">
                                <input name="check" id="check" value="1" type="checkbox" class="icheckbox" onchange="javascript:mostrar()"> Eliminar
                            </label> &nbsp;
                            <label class="check">
                                <input name="check1" id="check1" value="1" type="checkbox" class="icheckbox"/> Agregar
                            </label>
                            <br>
                            <button class="btn btn-primary" id="btn_siguiente" onclick="siguiente()">Seleccionar</button>
                            <style type="text/css">
                                #btn_siguiente{
                                    float: right;
                                }
                           </style>
                    </div>
                    <br>
            </div>
            </div>                        
        </div> 
    </div>                   
    <div class="row" id="agregar">
    <form id="form" method="post" enctype="multipart/form-data">     
        <div class="col-md-12">
            <div class="panel panel-default">
            <div class="panel-body">
                <label for="">Titulo del Album:</label>
                @foreach($album as $a)
                <input type="hidden" id="idnoticia" name="idnoticia" value="{{$a->id}}" class="form-control">
                <input id="titulo" name="titulo" type="text" value="{{$a->titulo}}" class="form-control" placeholder="Ingrese un titulo a la fotografia"/>
                @endforeach
                <br>
                <h3><span class="fa fa-upload"></span> Fotografias</h3>
                    <div class="col-12 grid-margin">
                        <div class="file-loading">
                            <label>Preview File Icon</label>
                            <input name="archivo[]" id="archivo" type="file" multiple>
                        </div>
                    </div>
                    <br>
                <button type="button" id="btn_subir"  class="btn btn-primary">SUBIR ARCHIVO</button>
            </div>
            </div>                        
        </div>          
    </form>                                                 
    </div>  
     <div class="row" id="eliminar">
    <!--<form id="form" method="post" enctype="multipart/form-data"> -->   
        <div class="col-md-12">
            <div class="panel panel-default">
            <div class="panel-body">
                <label for="">Titulo del Album:</label>
                @foreach($album as $a)
                <input id="titulo2" name="titulo2" type="text" value="{{$a->titulo}}" class="form-control" placeholder="Ingrese un titulo a la fotografia"/>
                @endforeach
                <br>
                <h3><span class="fa fa-search"></span> Fotografias</h3><br>
                <h4><span class="fa fa-info-circle"> Seleccione una imagen para eliminar..!</span></h4>
                <br>
                <div id="aniimated-thumbnials">
                    @foreach($fotos as $a)
                        <a href="javascript:void()" onclick="delete_foto({{$a->id}})" >
                            <img src="{{asset($a->foto)}}" title="{{$a->titulo}}" style=" height: 30em; width: 20em; margin: 10px 10px 0 0;"/>
                        </a>
                    @endforeach
                </div>
            </div>
            </div>                        
        </div>          
   <!-- </form>-->                                                 
    </div>               
    <!-- END DASHBOARD CHART -->
<!-- Modal -->
<div class="modal fade" id="m_update_album" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
 <!--<form id="form" method="post" action="" enctype="multipart/form-data">-->
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
        <h5 class="modal-title" id="exampleModalLabel">Eliminar Fotografia del Album</h5>
      </div>
      <div class="modal-body">
        <div class="row">
            <div class="form-group">
                <input type="hidden" id="id_album">
                <input type="hidden" id="id_fotografia">
                <div class="alert alert-info" role="alert">
                  <h4 class="alert-heading">¿Estas seguro que deseas eliminar esta foto?</h4>
                </div>
            </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
        <button type="button" class="btn btn-danger" id="btn_eliminar" >Eliminar</button>
      </div>
    </div>
  </div>
    <!--</form>-->
</div>                  
@endsection

@section('js')
<!--<script type="text/javascript" src="{{asset('admin/js/plugins/dropzone/dropzone.min.js')}}"></script>-->
<script src="{{asset('admin/dropify/dist/js/dropify.min.js')}}"></script>
<script type="text/javascript" src="{{asset('admin/js/plugins/fileinput/fileinput.min.js')}}"></script>     
<script type="text/javascript" src="{{asset('admin/js/adminpublicacionesjs/admin_edit_noticia.js')}}"></script>
<script type="text/javascript" src="{{asset('krajee/js/fileinput.js')}}"></script>

<script src="{{asset('admin/js/dropzone.js')}}"></script>
<script>
    $("#archivo").fileinput();
    $('.dropify').dropify({
      messages: {
          'default': 'Arrastra aqui sus fotos',
          'replace': 'Arrastra y suelta o haz clic para reemplazar las fotos',
          'remove':  'Eliminar',
          'error':   'Ooops, algo malo paso.'
      }
  });
</script>
<script type="text/javascript">
    document.getElementById('agregar').style.display= 'none';
    document.getElementById('eliminar').style.display= 'none';
    $('#regresar').click(function(){
        window.location = '/administracion/publicaciones/administrar/admin_noticia';
    });
    $('#btn_eliminar').click(function(){
        //alert('funciona');
        var idfotografia = $('#id_fotografia').val();
        var token= $('#token').val();
        //alert(idfotografia);
        eliminar_foto(idfotografia, token)
    });
    
     $('#btn_subir').click(function(){
        //alert('funciona');
        var img = $('#archivo').val();
        var token = $("#token").val();
        var datos  = new FormData($("#form")[0]);
        if(img=="")
        {
            alert('Seleccione imagenes');
        }
        else
        {
            $.ajax({
                url:'/administracion/publicaciones/store_album_dos_noticia',
                type:'POST',
                dataType:'json',
                headers :{'X-CSRF-TOKEN': token},
                data: datos,
                contentType: false,
                processData: false,
                success:function(res){
                    if(res.RES){
                        alert("Publicacion Guardada");
                        window.location = '/administracion/publicaciones/administrar/admin_noticia';
                    }else{
                        alert("Error al Guardar");
                    }
                }
            })      
        } 
    });
    
    function siguiente()
    {
        if( $('#check').prop('checked') ) {
            if( $('#check1').prop('checked') ) {
                alert('Seleccione solo una opcion');
            }
            else
            {
               $('#eliminar').show();
                document.getElementById('seleccion').style.display= 'none'; 
                document.getElementById('seleccion').style.display= 'none'; 
            }
        }
        else
        {
             if( $('#check1').prop('checked') ) {
                 $('#agregar').show();
                document.getElementById('seleccion').style.display= 'none'; 
            }
            else
            {
                alert('Seleccione una opcion');
            }
        }
            
    }
</script>
@endsection