@extends('Layouts.Admin_app')

@section('css')

@endsection



@section('menu')
<li >
                        <a href="/administracion/home"><span class="fa fa-desktop"></span> <span class="xn-text">Inicio</span></a>                        
                    </li>                    
                    
                    <li class="xn-openable active">
                        <a href="#"><span class="fa fa-file-text-o"></span> <span class="xn-text">Publicaciones</span></a>
                        <ul>
                            <li><a href="/administracion/publicaciones">Nueva publicación</a></li>
                            <li><a href="/administracion/publicaciones/administrar">Administrar</a></li>
                        </ul>
                    </li>
                    <li class="xn-title">Configuraciones</li>
                    <li>
                        <a href="/administracion/usuarios"><span class="fa fa-user"></span> <span class="xn-text">Usuarios</span></a>
                    </li>    
                    <li >
                        <a href="/administracion/categorias"><span class="fa fa-bars"></span> <span class="xn-text">Categorias</span></a>
                    </li>          
@endsection
@section('content')
     <!-- START WIDGETS -->                    
   <div class="row">
                      
                        <div class="col-md-3">
                            
                            <!-- START WIDGET MESSAGES -->
                            <div class="widget widget-default widget-item-icon" onclick="location.href='publicaciones/new_picture';">
                                <div class="widget-item-left">
                                    <span class="fa fa-camera"></span>
                                </div>                             
                                <div class="widget-data">
                                    <div class="widget-title">Subir Fotografias </div>
                                    <div class="widget-subtitle"></div>
                                </div>      
                                <div class="widget-controls">                                
                                </div>
                            </div>                            
                            <!-- END WIDGET MESSAGES -->
                            
                        </div>
                        <div class="col-md-3">
                            
                            <!-- START WIDGET REGISTRED -->
                            <div class="widget widget-default widget-item-icon" onclick="location.href='publicaciones/new_video';">
                                <div class="widget-item-left">
                                    <span class="fa fa-film"></span>
                                </div>
                                <div class="widget-data">
                                    <div class="widget-int num-count"></div>
                                    <div class="widget-title">Subir un Video</div>
                                    <div class="widget-subtitle"></div>
                                </div>
                                <div class="widget-controls">                                
                                </div>                            
                            </div>                            
                            <!-- END WIDGET REGISTRED -->
                            
                        </div>
                        <div class="col-md-3">
                            
                            <!-- START WIDGET REGISTRED -->
                            <div class="widget widget-default widget-item-icon" onclick="location.href='publicaciones/new_album';">
                                <div class="widget-item-left">
                                    <span class="fa fa-picture-o"></span>
                                </div>
                                <div class="widget-data">
                                    <div class="widget-int num-count"></div>
                                    <div class="widget-title">Subir un Album</div>
                                    <div class="widget-subtitle">a la galeria</div>
                                </div>
                                <div class="widget-controls">                                
                                </div>                            
                            </div>                            
                            <!-- END WIDGET REGISTRED -->
                            
                        </div>
                        <div class="col-md-3">
                            
                            <!-- START WIDGET MESSAGES -->
                            <div class="widget widget-default widget-item-icon" onclick="location.href='publicaciones/new_noticia';">
                                <div class="widget-item-left">
                                    <span class="fa fa-comment"></span>
                                </div>                             
                                <div class="widget-data">
                                    <div class="widget-title">Subir Noticias </div>
                                    <div class="widget-subtitle"></div>
                                </div>      
                                <div class="widget-controls">                                
                                </div>
                            </div>                            
                            <!-- END WIDGET MESSAGES -->
                            
                        </div>
                    </div>
                <div class="row">
                    <div class="col-md-3">
                        <div class="widget widget-default widget-item-icon" onclick="location.href='publicaciones/new_concurso';">
                            <div class="widget-item-left">
                                <span class="fa fa-trophy"></span>
                            </div>                             
                            <div class="widget-data">
                                <div class="widget-title">Subir Concurso </div>
                                <div class="widget-subtitle"></div>
                            </div>      
                            <div class="widget-controls">                                
                            </div>
                        </div>                       
                    </div>
                </div>
                    <!-- END WIDGETS -->                    
                    <!-- START DASHBOARD CHART -->
					<div class="chart-holder" id="dashboard-area-1" style="height: 200px;"></div>
					<div class="block-full-width">
                                                                       
                    </div>                    
                    <!-- END DASHBOARD CHART -->
                  
@endsection

@section('js')

@endsection