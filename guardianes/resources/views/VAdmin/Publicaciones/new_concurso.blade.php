@extends('Layouts.Admin_app')

@section('css')
<link rel="stylesheet" href="{{asset('admin/dropify/dist/css/dropify.css')}}"/>

@endsection



@section('menu')
<li >
                        <a href="/administracion/home"><span class="fa fa-desktop"></span> <span class="xn-text">Inicio</span></a>                        
                    </li>                    
                    
                    <li class="xn-openable active">
                        <a href="#"><span class="fa fa-file-text-o"></span> <span class="xn-text">Publicaciones</span></a>
                        <ul>
                            <li><a href="/administracion/publicaciones">Nueva publicación</a></li>
                            <li><a href="/administracion/publicaciones/administrar">Administrar</a></li>
                        </ul>
                    </li>
                    <li class="xn-title">Configuraciones</li>
                    <li>
                        <a href="/administracion/usuarios"><span class="fa fa-user"></span> <span class="xn-text">Usuarios</span></a>
                    </li>    
                    <li >
                        <a href="/administracion/categorias"><span class="fa fa-bars"></span> <span class="xn-text">Categorias</span></a>
                    </li>          
@endsection
@section('content')
    <!-- START WIDGETS -->                    
    <div class="row">
    <form id="formconcurso" method="post" action="" enctype="multipart/form-data">  
        <div class="col-md-4">
            <div class="panel panel-default">
                <div class="panel-body">
                    <div class="form-group">
                        <div class="col-md-12">
                            <label>Categoria:</label>
                            <select class="form-control" id="categoriac" name="categoriac">
                                <option value="0">Seleccione una categoria</option>
                                @foreach($categorias as $c)
                                <option value="{{$c->id}}">{{$c->categoria}}</option>
                                @endforeach
                            </select>
                            <br>
                            <label>Contenido:</label>
                            <textarea id="contenidocon" name="contenidocon" class="form-control" cols="20" rows="10"></textarea>
                            <br>
                            <button type="button" id="btn_subirc" onclick="save_concurso()" class="btn btn-primary">SUBIR ARCHIVO</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-8">
            <div class="panel panel-default">
            <div class="panel-body">
                <h3><span class="fa fa-upload"></span> Fotografias</h3>
                    <div class="col-12 grid-margin">
                        <h5 class="card-title mb-4">Subir Archivos</h5>
                            <input type="file" class="dropify" name="archivoc" id="archivoc">
                            <br>
                    </div>
            </div>
        </div>                        
    </div>          
    </form>                                                  
    </div>                    
    <!-- END DASHBOARD CHART -->
                  
@endsection

@section('js')
<!--<script type="text/javascript" src="{{asset('admin/js/plugins/dropzone/dropzone.min.js')}}"></script>-->
<script src="{{asset('admin/dropify/dist/js/dropify.min.js')}}"></script>
<script type="text/javascript" src="{{asset('admin/js/publicacionesjs/control_publicaciones.js')}}"></script>
<script>
  $('.dropify').dropify({
      messages: {
          'default': 'Arrastra aqui sus archivos',
          'replace': 'Arrastra y suelta o haz clic para reemplazar',
          'remove':  'Eliminar',
          'error':   'Ooops, algo malo paso.'
      }
  });
</script>
<script type="text/javascript">
    $('#archivoc').click(function(){
        var ar= $('#archivoc').val();
        //alert(ar);
    });
</script>

@endsection