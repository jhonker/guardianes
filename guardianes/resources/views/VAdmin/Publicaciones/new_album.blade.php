@extends('Layouts.Admin_app')

@section('css')
<link rel="stylesheet" href="{{asset('admin/dropify/dist/css/dropify.css')}}"/>
<link rel="stylesheet" href="{{asset('krajee/css/fileinput.css')}}"/>

@endsection



@section('menu')
<li >
    <a href="/administracion/home"><span class="fa fa-desktop"></span> <span class="xn-text">Inicio</span></a>                        
</li>                    
                    <li class="xn-openable active">
                        <a href="#"><span class="fa fa-file-text-o"></span> <span class="xn-text">Publicaciones</span></a>
                        <ul>
                            <li><a href="/administracion/publicaciones">Nueva publicación</a></li>
                            <li><a href="/administracion/publicaciones/administrar">Administrar</a></li>
                        </ul>
                    </li>
                    <li class="xn-title">Configuraciones</li>
                    <li>
                        <a href="/administracion/usuarios"><span class="fa fa-user"></span> <span class="xn-text">Usuarios</span></a>
                    </li>    
                    <li >
                        <a href="/administracion/categorias"><span class="fa fa-bars"></span> <span class="xn-text">Categorias</span></a>
                    </li>          
@endsection
@section('content')
    <!-- START WIDGETS -->                    
    <div class="row">
    <form id="form" method="post" enctype="multipart/form-data">     
        <div class="col-md-12">
            <div class="panel panel-default">
            <div class="panel-body">
                <label for="">Titulo del Album:</label>
                <input id="titulo" name="titulo" type="text" class="form-control" placeholder="Ingrese un titulo a la fotografia"/>
                <br>
                <h3><span class="fa fa-upload"></span> Fotografias</h3>
                    <div class="col-12 grid-margin">
                        <div class="file-loading">
                            <label>Preview File Icon</label>
                            <input name="archivo[]" id="archivo" type="file" multiple>
                        </div>
                    </div>
                    <br>
                <button type="button" id="btn_subir" onclick="save_album()" class="btn btn-primary">SUBIR ARCHIVO</button>
            </div>
            </div>                        
        </div>          
    </form>                                                 
    </div>                    
    <!-- END DASHBOARD CHART -->
                  
@endsection

@section('js')
<!--<script type="text/javascript" src="{{asset('admin/js/plugins/dropzone/dropzone.min.js')}}"></script>-->
<script src="{{asset('admin/dropify/dist/js/dropify.min.js')}}"></script>
<script type="text/javascript" src="{{asset('admin/js/plugins/fileinput/fileinput.min.js')}}"></script>        
<script type="text/javascript" src="{{asset('admin/js/publicacionesjs/control_albumes.js')}}"></script>
<script type="text/javascript" src="{{asset('krajee/js/fileinput.js')}}"></script>

<script src="{{asset('admin/js/dropzone.js')}}"></script>
<script>
    $("#archivo").fileinput();
    $('.dropify').dropify({
      messages: {
          'default': 'Arrastra aqui sus fotos',
          'replace': 'Arrastra y suelta o haz clic para reemplazar las fotos',
          'remove':  'Eliminar',
          'error':   'Ooops, algo malo paso.'
      }
  });
</script>
@endsection