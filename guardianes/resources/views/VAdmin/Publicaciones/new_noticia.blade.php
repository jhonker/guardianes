@extends('Layouts.Admin_app')

@section('css')
<link rel="stylesheet" href="{{asset('admin/dropify/dist/css/dropify.css')}}"/>
<link rel="stylesheet" href="{{asset('krajee/css/fileinput.css')}}"/>

@endsection



@section('menu')
<li >
    <a href="/administracion/home"><span class="fa fa-desktop"></span> <span class="xn-text">Inicio</span></a>                        
</li>                    
                    <li class="xn-openable active">
                        <a href="#"><span class="fa fa-file-text-o"></span> <span class="xn-text">Publicaciones</span></a>
                        <ul>
                            <li><a href="/administracion/publicaciones">Nueva publicación</a></li>
                            <li><a href="/administracion/publicaciones/administrar">Administrar</a></li>
                        </ul>
                    </li>
                    <li class="xn-title">Configuraciones</li>
                    <li>
                        <a href="/administracion/usuarios"><span class="fa fa-user"></span> <span class="xn-text">Usuarios</span></a>
                    </li>    
                    <li >
                        <a href="/administracion/categorias"><span class="fa fa-bars"></span> <span class="xn-text">Categorias</span></a>
                    </li>          
@endsection
@section('content')
    <!-- START WIDGETS -->
    <div class="row">
        <div class="col-md-5" id="seleccion">
            <div class="panel panel-default">
                <div class="panel-body">
                    <div class="form-group">
                       <div class="col-md-12">
                           <label for="">Elija una Opcion</label>
                           <br>
                            <label class="check">
                                <input name="check" id="check" value="1" type="checkbox" class="icheckbox"> Video
                            </label> &nbsp;
                            <label class="check">
                                <input name="check1" id="check1" value="1" type="checkbox" class="icheckbox"/> Fotografias
                            </label>
                            <br>
                            <button class="btn btn-primary" id="btn_siguiente" onclick="siguiente()">Seleccionar</button>
                            <style type="text/css">
                                #btn_siguiente{
                                    float: right;
                                }
                           </style>
                        </div>
                    </div>
                </div>
            </div>
        </div> 
    </div>
    <div class="row">
       <div class="col-md-5" id="seleccionvideo">
           <div class="panel panel-default">
               <div class="panel-body">
                       <div class="form-group">
           <div class="col-md-12">
               <label for="">¿Desea subir un video o un enlace?</label>
               <br>
                <label class="check">
                    <input name="check" id="check2" value="1" type="checkbox" class="icheckbox" onchange="javascript:mostrar()"> Video
                </label> &nbsp;
                <label class="check">
                    <input name="check1" id="check3" value="1" type="checkbox" class="icheckbox"/> Enlace
                </label>
                <br>
                <button class="btn btn-primary" id="btn_siguiente" onclick="siguiente2()">Seleccionar</button>
                <style type="text/css">
                    #btn_siguiente{
                        float: right;
                    }
               </style>
            </div>
        </div>
               </div>
           </div>
       </div> 
    </div>
    <br>
    <div class="row">
        <div class="col-md-4">
            <button class="btn btn-danger" id="btn_regresar" onclick="regresar()">Regresar</button>
        </div>
    </div>
    <br>                 
    <div class="row">
    <form id="form" method="post" enctype="multipart/form-data">     
        <div class="col-md-12">
            <div class="panel panel-default">
            <div class="panel-body">
                <div class="row">
                    <label for="">Titulo de la Noticia:</label>
                    <input id="titulonoticia" name="titulonoticia" type="text" class="form-control" placeholder="Ingrese un titulo a la noticia"/>
                </div>
                <br>
                <div class="row">
                    <label>Contenido:</label>
                    <textarea id="contenidonoticia" name="contenidonoticia" class="form-control" cols="20" rows="5"></textarea>
                </div>
                <br>
                <div class="row" id="fotografia">
                    <div class="col-md-12">
                        <h3><span class="fa fa-upload"></span> Fotografias</h3>
                        <div class="col-12 grid-margin">
                            <div class="file-loading">
                                <label>Preview File Icon</label>
                                <input name="archivo[]" id="archivo" type="file" multiple>
                            </div>
                        </div>
                    </div>
                    <br>
                    <button type="button" id="btn_subir" onclick="save_noticia()" class="btn btn-primary" style="    margin-top: 1%;">SUBIR ARCHIVO</button>
                </div>
                <div class="col-md-12" id="contenido_enlace">
                    <label>Enlace:</label>
                    <textarea id="enlacen" name="enlacen"  class="form-control" cols="20" rows="5"></textarea>
                    <br>
                    <button type="button" id="btn_subir_enlace" onclick="save_noticia_enlace()" class="btn btn-primary" style="    margin-top: 1%;">SUBIR ENLACE</button>                        
                </div> 
                <div class="col-md-8" id="contenido_video">
                    <div class="panel panel-default">
                    <div class="panel-body">
                        <h3><span class="fa fa-upload"></span> Videos</h3>
                            <div class="col-12 grid-margin">
                                <h5 class="card-title mb-4">Subir Archivos</h5>
                                    <input type="file" class="dropify" name="archivov" id="archivov">
                                    <br>
                        </div>
                    </div>
                    </div>
                    <br>
                    <button type="button" id="btn_subir_video" onclick="save_noticia_video()" class="btn btn-primary" style="    margin-top: 1%;">SUBIR VIDEO</button>                       
                </div> 
                <br>
            </div>
            </div>                        
        </div>          
    </form>                                                 
    </div>                    
    <!-- END DASHBOARD CHART -->
                  
@endsection

@section('js')
<!--<script type="text/javascript" src="{{asset('admin/js/plugins/dropzone/dropzone.min.js')}}"></script>-->
<script src="{{asset('admin/dropify/dist/js/dropify.min.js')}}"></script>
<script type="text/javascript" src="{{asset('admin/js/plugins/fileinput/fileinput.min.js')}}"></script>        
<script type="text/javascript" src="{{asset('admin/js/publicacionesjs/control_noticias.js')}}"></script>
<script type="text/javascript" src="{{asset('krajee/js/fileinput.js')}}"></script>

<script src="{{asset('admin/js/dropzone.js')}}"></script>
<script>
    $("#archivo").fileinput();
    $('.dropify').dropify({
      messages: {
          'default': 'Arrastra aqui sus fotos',
          'replace': 'Arrastra y suelta o haz clic para reemplazar las fotos',
          'remove':  'Eliminar',
          'error':   'Ooops, algo malo paso.'
      }
  });
    document.getElementById('fotografia').style.display= 'none';
    document.getElementById('seleccionvideo').style.display= 'none';
    document.getElementById('contenido_enlace').style.display= 'none';
    document.getElementById('contenido_video').style.display= 'none';
    document.getElementById('btn_regresar').style.display= 'none';
    function siguiente()
    {
        if( $('#check').prop('checked') ) {
            if( $('#check1').prop('checked') ) {
                alert('Seleccione solo una opcion');
                $('#fotografia').hide();
                $('#seleccionvideo').hide();
            }
            else
            {
                $('#fotografia').hide();
                $('#seleccionvideo').show();
                //alert('Video');
            }
        }
        else
        {
             if( $('#check1').prop('checked') ) {
                //alert('Fotografias');
                $('#fotografia').show();
                $('#seleccionvideo').hide();
            }
            else
            {
                $('#fotografia').hide();
                $('#seleccionvideo').hide();
                alert('Seleccione una opcion');
            }
        }
            
    }

    function siguiente2()
    {
        if( $('#check2').prop('checked') ) {
            if( $('#check3').prop('checked') ) {
                alert('Seleccione solo una opcion');
                $('#contenido_video').hide();
                $('#contenido_enlace').hide();
                $('#btn_regresar').hide();
            }
            else
            {
                //alert('Video');
                $('#contenido_enlace').hide();
                $('#contenido_video').show();
                $('#btn_regresar').show();
            }
        }
        else
        {
             if( $('#check3').prop('checked') ) {
                //alert('Enlace');
                $('#contenido_enlace').show();
                $('#contenido_video').hide();
                $('#btn_regresar').show();
            }
            else
            {
                alert('Seleccione una opcion');
                $('#contenido_video').hide();
                $('#contenido_enlace').hide();
                $('#btn_regresar').hide();
            }
        }
            
    }

    function regresar()
    {
        //window.location = '/administracion/publicaciones/new_video';
        document.getElementById('contenido_enlace').style.display= 'none';
        document.getElementById('contenido_video').style.display= 'none';
        //document.getElementById('contenido').style.display= 'none';
        document.getElementById('btn_regresar').style.display= 'none';
        $('#seleccionvideo').show();
    }
</script>
@endsection