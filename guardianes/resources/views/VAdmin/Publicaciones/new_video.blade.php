@extends('Layouts.Admin_app')

@section('css')
<link rel="stylesheet" href="{{asset('admin/dropify/dist/css/dropify.css')}}"/>

@endsection



@section('menu')
<li >
                        <a href="/administracion/home"><span class="fa fa-desktop"></span> <span class="xn-text">Inicio</span></a>                        
                    </li>                    
                    
                    <li class="xn-openable active">
                        <a href="#"><span class="fa fa-file-text-o"></span> <span class="xn-text">Publicaciones</span></a>
                        <ul>
                            <li><a href="/administracion/publicaciones">Nueva publicación</a></li>
                            <li><a href="/administracion/publicaciones/administrar">Administrar</a></li>
                        </ul>
                    </li>
                    <li class="xn-title">Configuraciones</li>
                    <li>
                        <a href="/administracion/usuarios"><span class="fa fa-user"></span> <span class="xn-text">Usuarios</span></a>
                    </li>    
                    <li >
                        <a href="/administracion/categorias"><span class="fa fa-bars"></span> <span class="xn-text">Categorias</span></a>
                    </li>          
@endsection
@section('content')
    <!-- START WIDGETS -->
    <div id="load" class="load">
        <div class="load-1">
            <img src="{{asset('admin/imagenes/cargando_1.gif')}}" class="img-load" alt="">
        </div>
    </div> 
    <div class="row">
       <div class="col-md-4" id="seleccion">
           <div class="panel panel-default">
               <div class="panel-body">
                       <div class="form-group">
           <div class="col-md-12">
               <label for="">¿Desea subir un video o un enlace?</label>
               <br>
                <label class="check">
                    <input name="check" id="check" value="1" type="checkbox" class="icheckbox" onchange="javascript:mostrar()"> Video
                </label> &nbsp;
                <label class="check">
                    <input name="check1" id="check1" value="1" type="checkbox" class="icheckbox"/> Enlace
                </label>
                <br>
                <button class="btn btn-primary" id="btn_siguiente" onclick="siguiente()">Seleccionar</button>
                <style type="text/css">
                    #btn_siguiente{
                        float: right;
                    }
               </style>
            </div>
        </div>
               </div>
           </div>
       </div> 
    </div>
    <div class="row">
        <div class="col-md-4">
            <button class="btn btn-danger" id="btn_regresar" onclick="regresar()">Regresar</button>
        </div>
    </div>
    <br>                   
    <div class="row">
        <style type="text/css">
            .load{
                display: none;
                height: 69vh;
                position: absolute;
                width: 100%;
                z-index: 1000000;
                background: #fffffffa;
            }
            .load-1{
                height: 85vh;
                display:  flex;
                justify-content:  center;
                /*flex-direction: column;*/
                flex-direction: row;
                margin-left: 1%;
                margin-top: -6%;
            }
            .img-load{
              height: 12em;
              display: flex;
              align-self: center;
            }
        </style>
    
    <form id="form" method="post" action="" enctype="multipart/form-data">     
        <div class="col-md-4" id="contenido">
            <div class="panel panel-default">
                <div class="panel-body">
                    <div class="form-group">
                        <div class="col-md-12">
                            <label>Categoria:</label>
                            <select class="form-control" id="categoria" name="categoria">
                                <option value="0">Seleccione una categoria</option>
                                @foreach($categorias as $c)
                                <option value="{{$c->id}}">{{$c->categoria}}</option>
                                @endforeach
                            </select>
                            <br>
                            <label>Titulo:</label>
                            <input id="titulo" name="titulo" type="text" class="form-control" placeholder="Ingrese un titulo a la fotografia"/>
                            <br>
                            <label>Contenido:</label>
                            <textarea id="contenid" name="contenid" class="form-control" cols="20" rows="5"></textarea>
                            <br>
                            <button type="button" id="btn_subir" onclick="save_video()"  class="btn btn-primary">SUBIR ARCHIVO</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-8" id="contenido_enlace">
            <div class="panel panel-default">
            <div class="panel-body">
                <div class="form-group">
                    <label>Enlace:</label>
                        <textarea id="enlace" name="enlace"  class="form-control" cols="20" rows="5"></textarea>
                </div>
            </div>
        </div>                        
        </div> 
        <div class="col-md-8" id="contenido_video">
            <div class="panel panel-default">
            <div class="panel-body">
                <h3><span class="fa fa-upload"></span> Videos</h3>
                    <div class="col-12 grid-margin">
                        <h5 class="card-title mb-4">Subir Archivos</h5>
                            <input type="file" class="dropify" name="archivo" id="archivo">
                            <br>
                    </div>
            </div>
        </div>                        
    </div>          
    </form>                                            
    </div>                    
    <!-- END DASHBOARD CHART -->
                  
@endsection

@section('js')
<!--<script type="text/javascript" src="{{asset('admin/js/plugins/dropzone/dropzone.min.js')}}"></script>-->
<script src="{{asset('admin/dropify/dist/js/dropify.min.js')}}"></script>
<script type="text/javascript" src="{{asset('admin/js/publicacionesjs/control_videos.js')}}"></script>
<script>
  $('.dropify').dropify({
      messages: {
          'default': 'Arrastra aqui sus videos',
          'replace': 'Arrastra y suelta o haz clic para reemplazar el video',
          'remove':  'Eliminar',
          'error':   'Ooops, algo malo paso.'
      }
  });
</script>
<script type="text/javascript">	
    document.getElementById('contenido_enlace').style.display= 'none';
    document.getElementById('contenido_video').style.display= 'none';
    document.getElementById('contenido').style.display= 'none';
    document.getElementById('btn_regresar').style.display= 'none';
    function siguiente()
    {
        if( $('#check').prop('checked') ) {
            if( $('#check1').prop('checked') ) {
                alert('Seleccione solo una opcion');
            }
            else
            {
                $('#contenido_video').show();
                $('#contenido').show();
                $('#btn_regresar').show();
                document.getElementById('contenido_enlace').style.display= 'none';
                document.getElementById('seleccion').style.display= 'none';
            }
        }
        else
        {
             if( $('#check1').prop('checked') ) {
                $('#contenido_enlace').show();
                $('#contenido').show();
                  $('#btn_regresar').show();
                 document.getElementById('contenido_video').style.display= 'none';
                 document.getElementById('seleccion').style.display= 'none';
            }
            else
            {
                document.getElementById('contenido_enlace').style.display= 'none';
                document.getElementById('contenido_video').style.display= 'none';
                document.getElementById('contenido').style.display= 'none';
                document.getElementById('btn_regresar').style.display= 'none';
                alert('Seleccione una opcion');
            }
        }
            
    }
    function regresar()
    {
        //window.location = '/administracion/publicaciones/new_video';
        document.getElementById('contenido_enlace').style.display= 'none';
        document.getElementById('contenido_video').style.display= 'none';
        document.getElementById('contenido').style.display= 'none';
        document.getElementById('btn_regresar').style.display= 'none';
        $('#seleccion').show();
    }
</script>

@endsection