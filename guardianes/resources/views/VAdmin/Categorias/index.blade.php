@extends('Layouts.Admin_app')

@section('css')

@endsection



@section('menu')
<li >
                        <a href="/administracion/home"><span class="fa fa-desktop"></span> <span class="xn-text">Inicio</span></a>                        
                    </li>  
                    <li class="xn-openable">
                        <a href="#"><span class="fa fa-file-text-o"></span> <span class="xn-text">Publicaciones</span></a>
                        <ul>
                            <li><a href="/administracion/publicaciones">Nueva publicación</a></li>
                            <li><a href="/administracion/publicaciones/administrar">Administrar</a></li>
                        </ul>
                    </li>
                    <li class="xn-title">Configuraciones</li>
                    <li>
                        <a href="/administracion/usuarios"><span class="fa fa-user"></span> <span class="xn-text">Usuarios</span></a>
                    </li>    
                    <li class="active">
                        <a href="/administracion/categorias"><span class="fa fa-bars"></span> <span class="xn-text">Categorias</span></a>
                    </li>          
@endsection
@section('content')
<div class="row">
<div class="col-md-12">

<!-- START DEFAULT DATATABLE -->
<div class="panel panel-default">
    <div class="panel-heading">                                
        <h3 class="panel-title">Categorias</h3>
        <ul class="panel-controls">
            <li><a href="#" class="" data-toggle="modal" data-target="#exampleModal"><span class="fa fa-plus"></span></a></li>
        </ul>                                
    </div>
    <div class="panel-body">
        <table class="table datatable">
            <thead>
                <tr>
                    <th>#</th>
                    <th>Categoria</th>
                    <th>:::</th>
                </tr>
            </thead>
            <tbody>
                @foreach($categorias as $c)
                <tr>
                    <td>{{$c->id}}</td>
                    <td>{{$c->categoria}}</td>
                    <td><div class="btn-group">
                            <a href="#" data-toggle="dropdown" class="btn btn-primary dropdown-toggle">Opciones <span class="caret"></span></a>
                                <ul class="dropdown-menu" role="menu">
                                    <li><a href="#" onclick="update_categoria({{$c->id}});">Actualizar</a></li>
                                    <li><a href="#" onclick="delete_categoria({{$c->id}});">Eliminar</a></li>                                                    
                                </ul>
                        </div></td>
                </tr>
                @endforeach
                
            </tbody>
        </table>
    </div>
</div>
<!-- END DEFAULT DATATABLE -->


</div>  
</div>


<!-- Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
        <h5 class="modal-title" id="exampleModalLabel">Crear Categoria</h5>
      </div>
      <div class="modal-body">
        <div class="row">
            <div class="form-group">
                <label for="">Categoria:</label>
                <input type="text" id="ip_categoria" class="form-control" placeholder="Ingrese la categoria">
            </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
        <button type="button" class="btn btn-primary" onclick="save_categoria()">Guardar cambios</button>
      </div>
    </div>
  </div>
</div>
<!-- Modal -->
<div class="modal fade" id="m_update_categorias" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
        <h5 class="modal-title" id="exampleModalLabel">Actualizar Categorias</h5>
      </div>
      <div class="modal-body">
        <div class="row">
            <div class="form-group">
                <label for="">Categoria:</label>
                <input type="hidden" id="ip_idcategoria">
                <input type="text" id="ip_categoria_a" class="form-control" placeholder="Ingrese la categoria">
            </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
        <button type="button" class="btn btn-primary" onclick="up_categoria()">Guardar cambios</button>
      </div>
    </div>
  </div>
</div>

<!-- Modal -->
<div class="modal fade" id="d_update_categorias" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
        <h5 class="modal-title" id="exampleModalLabel">Eliminar Categorias</h5>
      </div>
      <div class="modal-body">
        <div class="row">
            <div class="form-group">
                <label for="">Categoria:</label>
                <input type="hidden" id="ipd_idcategoria">
                <input type="text" id="ipd_categoria_a" class="form-control" placeholder="Ingrese la categoria">
            </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
        <button type="button" class="btn btn-danger" onclick="dl_categoria()">Eliminar</button>
      </div>
    </div>
  </div>
</div>
@endsection

@section('js')
<script type="text/javascript" src="{{asset('admin/js/plugins/datatables/jquery.dataTables.min.js')}}"></script>    
<script type="text/javascript" src="{{asset('admin/js/controladoresjs/control_categorias.js')}}"></script>
@endsection