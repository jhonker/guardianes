
<!DOCTYPE html>
<html>
	<head>

		<!-- Basic -->
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">	

		<title>GotiAhorros</title>	

		<meta name="keywords" content="Portoaguas Guardianesdelagua Gotin Gotina #ungestocuenta gotiahorros" />
		<meta name="description" content="Porque todos son guardianes del agua">
		<meta name="author" content="www.portoaguas.gob.ec">

		<!-- Favicon -->
		<link rel="shortcut icon" href="img/device.png" type="image/x-icon" />
		<link rel="apple-touch-icon" href="img/apple-touch-icon.png">

		<!-- Mobile Metas -->
		<meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1.0, shrink-to-fit=no">

		<!-- Web Fonts  -->
		<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800%7CShadows+Into+Light" rel="stylesheet" type="text/css">

		<!-- Vendor CSS -->
		<link rel="stylesheet" href="css/bootstrap.min.css">
		<link rel="stylesheet" href="css/fontawesome-all.min.css">
		<link rel="stylesheet" href="css/animate.min.css">
		<link rel="stylesheet" href="css/simple-line-icons.min.css">
		<link rel="stylesheet" href="css/owl.carousel.min.css">
		<link rel="stylesheet" href="css/owl.theme.default.min.css">
		<link rel="stylesheet" href="css/magnific-popup.min.css">

		<!-- Theme CSS -->
		<link rel="stylesheet" href="css/theme.css">
		<link rel="stylesheet" href="css/theme-elements.css">
		<link rel="stylesheet" href="css/theme-blog.css">
		<link rel="stylesheet" href="css/theme-shop.css">
		
		<!-- Demo CSS -->


		<!-- Skin CSS -->
		<link rel="stylesheet" href="css/default.css">	
		<script src="js/style.switcher.localstorage.js"></script> 


		<!-- Theme Custom CSS -->
		<link rel="stylesheet" href="css/custom.css">

		<!-- Head Libs -->
		<script src="js/modernizr.min.js"></script>

	</head>
	<body>

		<div class="body">
			<header id="header" data-plugin-options="{'stickyEnabled': true, 'stickyEnableOnBoxed': true, 'stickyEnableOnMobile': true, 'stickyStartAt': 55, 'stickySetTop': '-13px', 'stickyChangeLogo': true}">
				<div class="header-body">
					<div class="header-container container">
						<div class="header-row">
							<div class="header-column">
								<div class="header-row">
									<div class="header-logo">
										<a href="index.html">
                                                <img alt="Porto" width="111" height="54" data-sticky-width="120" data-sticky-height="50" data-sticky-top="0" src="img/logo.png">
										</a>
									</div>
								</div>
							</div>
							<div class="header-column justify-content-end">
								<div class="header-row pt-3">
								
									<div class="header-search d-none d-md-block">
                                            <a href="#" class="btn btn-lg btn-primary">Registrate!</a>
									</div>
								</div>
								<div class="header-row">
									<div class="header-nav">
										<div class="header-nav-main header-nav-main-effect-1 header-nav-main-sub-effect-1">
											<nav class="collapse">
												<ul class="nav nav-pills" id="mainNav">
													<li>
														<a href="/">
															Inicio
														</a>
													</li>
													<li>
														<a  href="/#home-intro">
																Conocenos
														</a>
													</li>
													<li class="dropdown dropdown-mega">
                                                            <a class="dropdown-item dropdown-toggle active"  href="#">
                                                                Multimedia
                                                            </a>
                                                            <ul class="dropdown-menu">
                                                                <li>
                                                                    <div class="dropdown-mega-content">
                                                                        <div class="row">
                                                                            <div class="col-lg-3">
                                                                                <span class="dropdown-mega-sub-title">GotiAhorros</span>
                                                                                <ul class="dropdown-mega-sub-nav">
                                                                                    <li><a class="dropdown-item" href="gotiahorro">Ver mas..</a></li>
                                                                                </ul>
                                                                            </div>
                                                                            <div class="col-lg-3">
                                                                                <span class="dropdown-mega-sub-title">GotiGaleria</span>
                                                                                <ul class="dropdown-mega-sub-nav">
                                                                                    <li><a class="dropdown-item" href="gotigaleria">Ver mas..</a></li>
                                                                                </ul>
                                                                            </div>
                                                                            <div class="col-lg-3">
                                                                                <span class="dropdown-mega-sub-title">GotiVideos</span>
                                                                                <ul class="dropdown-mega-sub-nav">
                                                                                    <li><a class="dropdown-item" href="gotivideo">Ver mas..</a></li>
                                                                                </ul>
                                                                            </div>
                                                                            <div class="col-lg-3">
                                                                                <span class="dropdown-mega-sub-title">Descarga y Diviertete</span>
                                                                                <ul class="dropdown-mega-sub-nav">
                                                                                    <li><a class="dropdown-item" href="descarga-y-diviertete">Ver mas..</a></li>
                                                                                </ul>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </li>
                                                            </ul>
                                                        </li>
													
													
													<li>
															<a href="/#galeria">
																Galeria
															</a>
														</li>
													<li >
														<a  href="#footer">
															CONTACTO
														</a>
													</li>
												</ul>
											</nav>
										</div>
										<ul class="header-social-icons social-icons d-none d-sm-block">
											<li class="social-icons-facebook"><a href="http://www.facebook.com/" target="_blank" title="Facebook"><i class="fab fa-facebook-f"></i></a></li>
											<li class="social-icons-twitter"><a href="http://www.twitter.com/" target="_blank" title="Twitter"><i class="fab fa-twitter"></i></a></li>
										</ul>
										<button class="btn header-btn-collapse-nav" data-toggle="collapse" data-target=".header-nav-main nav">
											<i class="fas fa-bars"></i>
										</button>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</header>

			<div role="main" class="main">

				<section class="page-header">
					<div class="container">
						<div class="row">
							<div class="col">
								<ul class="breadcrumb">
									<li><a href="#">Inicio</a></li>
									<li class="active">Multimedia</li>
								</ul>
							</div>
						</div>
						<div class="row">
							<div class="col">
								<h1>Descarga y Diviertete</h1>
							</div>
						</div>
					</div>
				</section>
				<div class="container">
					<div class="row">
						<div class="col">
							<h2 class="mb-0">Pinta y Juega con los Guadianes del agua 
							</h2>
						</div>
					</div>
				</div>
				<div class="container">
					<div class="row">
						<div class="col">
							<hr class="tall">
							<div class="masonry-loader masonry-loader-showing">
								<div class="masonry" data-plugin-masonry data-plugin-options="{'itemSelector': '.masonry-item'}">
								 @foreach($fotos as $a)
									<div class="masonry-item">
											<span class="thumb-info thumb-info-centered-info thumb-info-no-borders">
												<span class="thumb-info-wrapper">
													<img src="{{asset($a->foto)}}" class="img-fluid" alt="" width="200" height="200">
													<span class="thumb-info-title">
														<!--<span class="thumb-info-inner">{{$a->titulo}}</span>-->
														<a target="_blank" href="descarga-y-diviertete-download/{{$a->id}}">{{$a->titulo}}</a>
													</span>
													<span class="thumb-info-action" id="">
														<!--<span class="thumb-info-action-icon"><i class="fas fa-download"></i></span>-->
                                                        <a class="thumb-info-action-icon" href="descarga-y-diviertete/{{$a->id}}" target="_blank" onclick="vista({{$a->id}})"><i class="fas fa-download"></i></a>
													</span>
												</span>
											</span>
											<br>
									</div>
									@endforeach
									<!--<div class="masonry-item">
											<span class="thumb-info thumb-info-centered-info thumb-info-no-borders">
												<span class="thumb-info-wrapper">
													<img src="img/pintar1.jpg" class="img-fluid" alt="">
													<span class="thumb-info-title">
														<span class="thumb-info-inner">PINTA AGUA</span>
													</span>
													<span class="thumb-info-action" id="link_descarga">
														<span class="thumb-info-action-icon"><i class="fas fa-download"></i></span>
													</span>
												</span>
											</span>
									</div>
									<div class="masonry-item">
											<span class="thumb-info thumb-info-centered-info thumb-info-no-borders">
												<span class="thumb-info-wrapper">
													<img id="img1" src="img/pintar1.jpg" class="img-fluid" alt="1">
													<span class="thumb-info-title">
														<span class="thumb-info-inner">PINTA AGUA</span>
													</span>
													<span class="thumb-info-action" id="link_descarga">
														<span class="thumb-info-action-icon"><i class="fas fa-download"></i></span>
													</span>
												</span>
											</span>
									</div>-->
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<footer id="footer">
					<div class="container">
						<div class="row">
							<div class="col-lg-6">
								<div class="contact-details">
									<h4>Contacto</h4>
									<ul class="contact">
										<li><p><i class="fas fa-map-marker-alt"></i> <strong>Dirección:</strong> Ciudadela el Maestro calle san francisco y km 1/2 via crucita</p></li>
										<li><p><i class="fas fa-phone"></i> <strong>Teléfono:</strong> 053701960 ext 630</p></li>
										<li><p><i class="far fa-envelope"></i> <strong>Email:</strong> <a href="mailto:info@portoaguas.gob.ec">info@portoaguas.gob.ec</a></p></li>
									</ul>
								</div>
							</div>
							<div class="col-lg-3">
								<h4>Redes Sociales</h4>
								<ul class="social-icons">
									<li class="social-icons-facebook"><a href="https://www.facebook.com/PortoaguasEP/?ref=br_rs" target="_blank" title="Facebook"><i class="fab fa-facebook-f"></i></a></li>
									<li class="social-icons-twitter"><a href="https://twitter.com/Portoaguas?lang=es" target="_blank" title="Twitter"><i class="fab fa-twitter"></i></a></li>
									<li class="social-icons-youtube"><a href="https://www.youtube.com/channel/UCrLZJOnCWXaCmpb6CcoPZBg/videos" target="_blank" title="Youtube"><i class="fab fa-youtube"></i></a></li>
								</ul>
							</div>
							<div class="col-lg-3">
								<a href="http://portoviejo.gob.ec" target="Portoviejo"><img src="img/municipio.png" style="width: 175px; "></a>
								<a href="http://portoaguas.gob.ec" target="Portoaguas"><img src="img/portoaguas-footer.png" style="width: 135px;height:  35px;"></a>
							</div>
						</div>
					</div>
				</footer>
		</div>


		<!-- Vendor -->
		<script src="js/jquery.min.js"></script>
		<script src="js/jquery.appear.min.js"></script>
		<script src="js/jquery.easing.min.js"></script>
		<script src="js/jquery-cookie.min.js"></script>
		<script src="js/popper.min.js"></script>
		<script src="js/bootstrap.min.js"></script>
		<script src="js/common.min.js"></script>
		<script src="js/jquery.validation.min.js"></script>
		<script src="js/jquery.gmap.min.js"></script>
		<script src="js/jquery.lazyload.min.js"></script>
		<script src="js/jquery.isotope.min.js"></script>
		<script src="js/owl.carousel.min.js"></script>
		<script src="js/jquery.magnific-popup.min.js"></script>
		<script src="js/vide.min.js"></script>
		<!-- Theme Base, Components and Settings -->
		<script src="js/theme.js"></script>
		
		<!-- Theme Custom -->
		<script src="js/custom.js"></script>
		
		<!-- Theme Initialization Files -->
		<script src="js/theme.init.js"></script>

		<!-- Examples -->
		<script src="js/examples.gallery.js"></script>	

		<script>
			$("#link_descarga").click(function(){
				//alert("descargando.....");
                var img1 = $('#img1').val();
                alert(img1);
			})
            function vista(id)
            {
                $.ajax({
                    url:'/vista/{id}',
                    type:'POST',
                    dataType:'json',
                    headers :{'X-CSRF-TOKEN': token},
                });
            }
		</script>
		<script src="js/analytics.js"></script>

	</body>
</html>
