<!DOCTYPE html>
<html lang="es">
    <head>
        <title>Descarga</title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1, maximum-scale=1, minimum-scale=1">
    </head>
    <body>
        <div class="masonry-item">
            <span class="thumb-info thumb-info-centered-info thumb-info-no-borders">
                <span class="thumb-info-wrapper">
                    @foreach($album as $a)
                <img src="{{asset($a->foto)}}" alt="" width="400" height="500">
                <br>
                <!--<a class="thumb-info-action-icon" href="vista/{{$a->id}}">Descargar</a>-->
                @endforeach
            </span>
            </span>
        </div>
    </body>
    <script type="text/javascript">
        function descargar(id)
        {
             $.ajax({
                url:'descarga/'+id,
                type:'GET',
                dataType:'json'
            })
        }
    </script>
</html>