<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1.0, shrink-to-fit=no">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Guardianes del Agua</title>
    <meta name="keywords" content="Portoaguas Guardianesdelagua Gotin Gotina #ungestocuenta" />
	<meta name="description" content="Porque todos son guardianes del agua">
	<meta name="author" content="www.portoaguas.gob.ec">
		<script src="{{asset('js/modernizr.min.js')}}"></script>
    <link rel="stylesheet" href="{{asset('css/bootstrap.min.css')}}">
	<link rel="stylesheet" href="{{asset('css/fontawesome-all.min.css')}}">
    <link rel="stylesheet" href="{{asset('css/guardianes.css')}}">
    <link rel="stylesheet" href="{{asset('lightgallery/css/lightgallery.css')}}">

</head>
<body>
    
@include('Layouts.menu')
    <div class="container">
                <h4 class="i-tilulo">Fotografia</h4>
                <hr class="tall">
				<div id="aniimated-thumbnials" class="galeria">
					@foreach($gotiahorro as $g)
					    <a href="{{$g->foto}}" >
    					    <img src="{{$g->foto}}" class="gotiaohorro-img"/>
    					</a>
                    @endforeach
                </div>
            <h4 class="i-tilulo">Videos</h4>
            <hr class="tall">
            <div  class="galeria">
                <div class="row">
                @foreach($gotiahorrovideo as $v)
                <div class="col-lg-6 col-md-12" id="gav">
					<video  width="auto" height="215" controls >
	    				<source src="{{asset($v->link)}}" type="video/mp4">
                    </video>
                </div>
                @endforeach
                </div>
            </div>
    </div>

            @include('Layouts.logos')

		<script src="{{asset('js/jquery.min.js')}}"></script>
        <script src="{{asset('js/bootstrap.min.js')}}"></script>
        <script src="{{asset('lightgallery/js/lightgallery.js')}}"></script>
		<script src="{{asset('lightgallery/modules/lg-thumbnail.min.js')}}"></script>
		<script src="{{asset('lightgallery/modules/lg-fullscreen.min.js')}}"></script>
        
        <script>
        	$('#aniimated-thumbnials').lightGallery({
    thumbnail:true
}); </script>
</body>
</html>