<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1.0, shrink-to-fit=no">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Guardianes del Agua</title>
    <meta name="keywords" content="Portoaguas Guardianesdelagua Gotin Gotina #ungestocuenta" />
	<meta name="description" content="Porque todos son guardianes del agua">
	<meta name="author" content="www.portoaguas.gob.ec">
		<script src="js/modernizr.min.js"></script>
    <link rel="stylesheet" href="{{asset('css/bootstrap.min.css')}}">
	<link rel="stylesheet" href="{{asset('css/fontawesome-all.min.css')}}">
    <link rel="stylesheet" href="{{asset('css/guardianes.css')}}">
    <link rel="stylesheet" href="{{asset('lightgallery/css/lightgallery.css')}}">
    	<!-- Theme Custom CSS -->
		<link rel="stylesheet" href="{{asset('css/custom.css')}}">
</head>
<body>
@include('Layouts.menu')
    <div class="container">
        <div class="row">
			<div class="col">
				<!--<h3 style="text-align: center;">Descarga</h3>-->
                <h2 class="titulo_p gotivideo-t">Descarga y Diviertete</h2>
				<hr class="tall">
                <div class="row">
                    <div id="aniimated-thumbnials" class="animationt">
					    @foreach($album as $a)
					    <a href="{{asset($a->foto)}}" >
						    <img src="{{asset($a->foto)}}" class="{{$a->titulo}}" style=" height: 10em; width: 20%; margin: 10px 10px 0 0;"/>
						</a>
						@endforeach
					</div>
                </div>
			</div>
		</div>
    </div>
    <div style="height: 101px;"></div>
    
@include('Layouts.logos')

		<script src="{{asset('js/jquery.min.js')}}"></script>
        <script src="{{asset('js/bootstrap.min.js')}}"></script>
        <script src="{{asset('lightgallery/js/lightgallery.js')}}"></script>
		<script src="{{asset('lightgallery/modules/lg-thumbnail.min.js')}}"></script>
		<script src="{{asset('lightgallery/modules/lg-fullscreen.min.js')}}"></script>
        
        <script>
        	$('#aniimated-thumbnials').lightGallery({
    thumbnail:true
}); </script>
</body>
</html>