<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1.0, shrink-to-fit=no">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Guardianes del Agua</title>
    <meta name="keywords" content="Portoaguas Guardianesdelagua Gotin Gotina #ungestocuenta" />
	<meta name="description" content="Porque todos son guardianes del agua">
	<meta name="author" content="www.portoaguas.gob.ec">
		<script src="js/modernizr.min.js"></script>
    <link rel="stylesheet" href="css/bootstrap.min.css">
	<link rel="stylesheet" href="css/fontawesome-all.min.css">
    <link rel="stylesheet" href="css/guardianes.css">
    <link rel="stylesheet" href="lightgallery/css/lightgallery.css">

</head>
<body>
    
@include('Layouts.menu')
<div class="container">
    <h2 class="titulo_p gotivideo-t">Gotivideos</h2>
		<div class="row galeria">
			@foreach($gotivideo as $gv)
				@if($gv->estado_link=='1' && $gv->id_noticia=='')
					<div class="col-md-6" id="govi">
						<video  width="460" height="215" controls >
							<source src="{{asset($gv->link)}}" type="video/mp4">
						</video>
					</div>
				@endif
				<!--<div class="col-md-6" id="govi">
				@if($gv->estado_link=='1')
	    			{!! $gv->link !!}
	    			<video  width="460" height="215" controls >
						<source src="{{asset($gv->link)}}" type="video/mp4">
					</video>
				@else
					//<video  width="460" height="215" controls >
						<source src="{{asset($gv->link)}}" type="video/mp4">
					</video>//
				@endif
				</div>-->
            @endforeach
	</div>					
</div>

@include('Layouts.logos')

		<script src="js/jquery.min.js"></script>
        <script src="js/bootstrap.min.js"></script>
        <script src="lightgallery/js/lightgallery.js"></script>
		<script src="lightgallery/modules/lg-thumbnail.min.js"></script>
		<script src="lightgallery/modules/lg-fullscreen.min.js"></script>
        
        <script>
        	$('#aniimated-thumbnials').lightGallery({
    thumbnail:true
}); </script>
</body>
</html>