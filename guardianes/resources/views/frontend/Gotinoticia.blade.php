<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1.0, shrink-to-fit=no">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Guardianes del Agua</title>
    <meta name="keywords" content="Portoaguas Guardianesdelagua Gotin Gotina #ungestocuenta" />
	<meta name="description" content="Porque todos son guardianes del agua">
	<meta name="author" content="www.portoaguas.gob.ec">
		<script src="js/modernizr.min.js"></script>


	<link rel="stylesheet" href="css/bootstrap.css">
    <link rel="stylesheet" href="css/fonts.css">
    <link rel="stylesheet" href="css/style.css">

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">


    <link rel="stylesheet" href="css/bootstrap.min.css">
	<link rel="stylesheet" href="css/fontawesome-all.min.css">
    <link rel="stylesheet" href="css/guardianes.css">
    <link rel="stylesheet" href="lightgallery/css/lightgallery.css">

</head>
<body>
    
@include('Layouts.menu')
<div class="container">
    <h2 class="titulo_p gotivideo-t">Gotinoticias</h2>
    <?php 
    	$cont=0; $valv=''; $confot=0; $p=0;
	?>
	<div class="row galeria">
		@foreach($gotinoticia as $gn)
		
		<div class="col-md-6 wow-outer">
			<article class="post-modern wow slideInLeft rainbow-button"><div class="post-modern-media">
			@foreach($fotos as $ft)
				@if($ft->id_noticia == $gn->id)
					<?php $confot++;?>
					@if($cont==0)
					<!--<img src="{{$ft->foto}}" alt="" width="571" height="353" style="width: 65%; margin-top: inherit;height: 200px;">-->
					<img src="{{$ft->foto}}" alt="" width="571" height="353">
						<a id="vmas<?php echo $p; ?>" href="javascript:void(0)" onclick="photo({{$gn->id}})" type="button" class="btn btn-primary" style="width:65%;text-decoration: none;">Ver mas...</a>
					</img>
					<?php $cont=1; ?>
					@endif
				@endif
			@endforeach
				<?php $cont=0; $confot=0;?>
				@foreach($gotivideo as $gv)
				@if($gv->id_noticia== $gn->id)
				<?php 
				$valv= $gv->link;
				$int= substr($valv, 0, 6); 
				?>
					@if($int=='videos')
					<!--<video style="height: 259px;width: 439px;margin-top: 10%;margin-left: -70px;" controls>-->
					<video controls>
						<source src="{{asset($gv->link)}}" type="video/mp4">
					</video>
					@else
					<!--<video style="height: 240px;width: 439px;margin-top: 10%;margin-left: -70px;" controls>-->
					<video controls>
						<source src="{{asset($gv->link)}}" type="video/mp4">
					</video>
					@endif
				@endif
				@endforeach
			</div>
	            <h4 class="post-modern-title">
	                <a class="post-modern-title" href="javascript:void(0)">{{$gn->titulo}}</a>
	            </h4>
	            <!--<p class="post-modern-meta">{{$gn->contenido}}
	            	<?php //echo $lon= strlen($gn->contenido); ?>
	            </p>-->
	            <div id="obj1" class="post-modern-meta">
	            	<p id="p1<?php echo $p;?>">
	            		<?php
	            		$cade= $gn->contenido;
	            		$lon= strlen($cade);
	            		if($lon<=410)
	            		{
	            			echo $cade;
	            		}
	            		else
	            		{
	            			//$men= substr($cade, 0, 350);
	            			$men= substr($cade, 0, 408);
	            			$men.="...";
	            			echo $men.'
					          <a onclick="mostrar('.$p.')" id="mostrar'.$p.'" href="javascript:void(0)" class="btn-ghost blue secundary round">continuar</a>
					        ';
	            		}
	            		 ?>
	            	</p>
	            	<p id="p2<?php echo $p;?>">
	            	<?php 
	            		$cade1= $gn->contenido;
	            		$lon1= strlen($cade);
	            		if($lon1<=410)
	            		{
	            			echo $cade1;
	            		}
	            		else
	            		{
	            		//$men1= substr($cade, 350, $lon1);
	            		echo $cade1.'
					          <a onclick="ocultar('.$p.')" href="javascript:void(0)" class="btn-ghost blue round">ocultar</a>
					        ';
					    }
	            	?>
	            	</p>
	            </div>
	        </article>
	        <?php $cade=''; $men=''; $cade1=''; $men1=''; $p++; ?>
    	</div>
    	<br>
        @endforeach
	</div>					
</div>

<!-- Modal -->
<div class="modal fade" id="photos" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
        <h5 class="modal-title" id="exampleModalLabel">Crear Usuario</h5>
      </div>
      <div class="modal-body">
        <div class="row">
            <input type="text" name="" id="idphoto" class="form-control">
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
        <button type="button" class="btn btn-primary">Guardar cambios</button>
      </div>
    </div>
  </div>
</div>


@include('Layouts.logos')

		<script src="js/jquery.min.js"></script>
        <script src="js/bootstrap.min.js"></script>
        <script src="lightgallery/js/lightgallery.js"></script>
		<script src="lightgallery/modules/lg-thumbnail.min.js"></script>
		<script src="lightgallery/modules/lg-fullscreen.min.js"></script>
        
        <script>
        	$('#aniimated-thumbnials').lightGallery({
    thumbnail:true
}); </script>

	<script type="text/javascript">
		//document.getElementById('p2').style.display='none';
		var pp= '<?php echo $p; ?>';
		//console.log(pp);
		for(var i=0; i<pp; i++)
		{
			document.getElementById('p2'+i).style.display='none';
		}
		function ocultar(p)
		{
			//alert('Funciona');
			$('#p1'+p).show();
			document.getElementById('p2'+p).style.display='none';
		}

		function mostrar(p)
		{
			//alert('funciona');
			$('#p2'+p).show();
			document.getElementById('p1'+p).style.display='none';
		}

		function photo(p)
		{
			/*$('#idphoto').val(p);
			$('#photos').modal('show');*/
			window.location='/gotinoticia/'+utf8_to_b64(p);
		}
		function utf8_to_b64( str ) {
	      return window.btoa(unescape(encodeURIComponent( str )));
	    }
	</script>
</body>
</html>