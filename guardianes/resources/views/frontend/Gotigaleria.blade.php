<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1.0, shrink-to-fit=no">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Guardianes del Agua</title>
    <meta name="keywords" content="Portoaguas Guardianesdelagua Gotin Gotina #ungestocuenta" />
	<meta name="description" content="Porque todos son guardianes del agua">
	<meta name="author" content="www.portoaguas.gob.ec">
		<script src="js/modernizr.min.js"></script>
    <link rel="stylesheet" href="css/bootstrap.min.css">
	<link rel="stylesheet" href="css/fontawesome-all.min.css">
    <link rel="stylesheet" href="css/guardianes.css">
    <link rel="stylesheet" href="lightgallery/css/lightgallery.css">
    	<!-- Theme Custom CSS -->
		<link rel="stylesheet" href="css/custom.css">
</head>
<body>
@include('Layouts.menu')
<div class="container">
	<div class="row ">
		<div class="col">
            <h2 class="titulo_p gotivideo-t">Nuestros Albumes</h2>
	    	<hr class="tall">
            <div class="row galeria">
		    	@foreach($galerias as $a)
                <div class="col-md-6">
                    <div style="    margin-top: 20px;">
                        <a href="gotigaleria/{{$a->id}}">
                        <div class="_2gxa" style="width:100%;height:202px;">
			    			<div class="uiScaledImageContainer" style="width:100%;height:202px;">
							@foreach($foto_portada as $fp )
				            @if($fp->id_album==$a->id)
							    <img class="scaledImageFitWidth img" src="{{$fp->foto}}" style="top:-76px;" alt="La imagen puede contener: Kerly Lissette Valdiviezo y Jhony Guaman, personas sonriendo, primer plano" width="404" height="403">
							@endif
							@endforeach
                            </div>
                            <div class="_2gxb">
                                <div>
                                    <div class="_2gxd _2pis">
                                        <span class="_2iem _50f7">{{$a->titulo}}</span>
                                    </div>
                                </div>
                            </div>
                            <div class="_4f0s"></div>
                        </div>
                        </a>
                    </div>
				</div>
				@endforeach
            </div>
		</div>
	</div>
</div>
@include('Layouts.logos')

		<script src="js/jquery.min.js"></script>
        <script src="js/bootstrap.min.js"></script>
        <script src="lightgallery/js/lightgallery.js"></script>
		<script src="lightgallery/modules/lg-thumbnail.min.js"></script>
		<script src="lightgallery/modules/lg-fullscreen.min.js"></script>
        
        <script>
        	$('#aniimated-thumbnials').lightGallery({
    thumbnail:true
}); </script>
</body>
</html>