<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1.0, shrink-to-fit=no">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Guardianes del Agua</title>
    <meta name="keywords" content="Portoaguas Guardianesdelagua Gotin Gotina #ungestocuenta" />
	<meta name="description" content="Porque todos son guardianes del agua">
	<meta name="author" content="www.portoaguas.gob.ec">
		<script src="../js/modernizr.min.js"></script>


	<link rel="stylesheet" href="../css/bootstrap.css">
    <link rel="stylesheet" href="../css/fonts.css">
    <link rel="stylesheet" href="../css/style.css">

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">


    <link rel="stylesheet" href="../css/bootstrap.min.css">
	<link rel="stylesheet" href="../css/fontawesome-all.min.css">
    <link rel="stylesheet" href="../css/guardianes.css">
    <link rel="stylesheet" href="../lightgallery/css/lightgallery.css">

</head>
<body>
    
@include('Layouts.menu')
<style type="text/css">
	img:hover{
		transition: all 0.5s linear;
		transform: scale(0.9);
		border-radius: 30px;
	}

	
</style>
<div class="container">
    <h2 class="titulo_p gotivideo-t" onClick="redirigir()">Gotinoticias</h2>

    <?php 
    	$cont=0; $valv=''; $confot=0; $p=0;
	?>
	<div class="row galeria" id="viewn">
		<!--@foreach($gotinoticia as $gn)
		@foreach($fotos as $ft)
			@if($ft->id_noticia == $gn->id)
				<div class="separador2"></div>
				<img src="{{asset($ft->foto)}}" alt="" width="571" height="353">
    			<div class="separador"></div>
    		@endif
    	@endforeach
    	<br>
        @endforeach-->
    @foreach($gotinoticia as $gn)
	<h3 style="text-align: center;position: absolute;margin-left: 23%;width: 29%;">{{$gn->titulo}}</h3>
	@endforeach
	<div style="height: 25px;"></div>
    <div class="row">
        <div class="col-md-12 wow-outer">
			<div class="post-modern-media-modificado">
				<div id="aniimated-thumbnials">
					@foreach($album as $a)
					    <a href="{{asset($a->foto)}}" >
						    <img src="{{asset($a->foto)}}" alt="" width="571" height="353">
						</a>
					@endforeach
        		</div>
        	</div>
		</div>
	</div>
	</div>

	<div class="row galeria" id="viewm">
	@foreach($gotinoticia as $gn)
	<h3 style="text-align: center;position: absolute;margin-left: 10%;width: 226px;">{{$gn->titulo}}</h3>
	@endforeach
	<div style="height: 20px;"></div>
    <div class="row">
		<div class="col-md-12 wow-outer">
			<div class="post-modern-media">
				<div id="aniimated-thumbnials_2">
					@foreach($album as $a)
					    <a href="{{asset($a->foto)}}" >
						    <img src="{{asset($a->foto)}}" alt="" width="571" height="353">
						</a>
					@endforeach
        		</div>
        	</div>
		</div>
	</div>
	</div>						
</div>

<!-- Modal -->
<div class="modal fade" id="photos" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
        <h5 class="modal-title" id="exampleModalLabel">Crear Usuario</h5>
      </div>
      <div class="modal-body">
        <div class="row">
            <input type="text" name="" id="idphoto" class="form-control">
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
        <button type="button" class="btn btn-primary">Guardar cambios</button>
      </div>
    </div>
  </div>
</div>

<div class="sep"></div>
<style type="text/css">
	.sep{
		height: 70px;
	}
</style>
@include('Layouts.logos')

		<script src="../js/jquery.min.js"></script>
        <script src="../js/bootstrap.min.js"></script>
        <script src="../lightgallery/js/lightgallery.js"></script>
		<script src="../lightgallery/modules/lg-thumbnail.min.js"></script>
		<script src="../lightgallery/modules/lg-fullscreen.min.js"></script>
        
        <script>
        	$('#aniimated-thumbnials').lightGallery({
			    thumbnail:true
			}); 
			$('#aniimated-thumbnials_2').lightGallery({
			    thumbnail:true
			}); 
		</script>

	<script type="text/javascript">
		
		function redirigir()
		{
			window.location='/gotinoticia';
		}
	</script>
</body>
</html>