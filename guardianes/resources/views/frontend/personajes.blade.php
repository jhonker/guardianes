<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1.0, shrink-to-fit=no">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Guardianes del Agua</title>
    <meta name="keywords" content="Portoaguas Guardianesdelagua Gotin Gotina #ungestocuenta" />
	<meta name="description" content="Porque todos son guardianes del agua">
	<meta name="author" content="www.portoaguas.gob.ec">
		<script src="js/modernizr.min.js"></script>
    <link rel="stylesheet" href="css/bootstrap.min.css">
	<link rel="stylesheet" href="css/fontawesome-all.min.css">
    <link rel="stylesheet" href="css/guardianes.css">
		<link rel="stylesheet" href="css/animate.min.css">

</head>
<body>
    @include('Layouts.menu')

   
    <div class="banner-azul">
      <div class="row padding-top-3">
        <div class="col-md-12 center linea">
          <p class="text_am">Nuestros Personajes</p>
        </div>
      </div>
    </div>

    <div class="row margin_c">
        <div class=" offset-md-2 col-md-8 flex">
            <div id="gotina" class="gotina appear-animation" data-appear-animation="fadeInLeft"></div>
            <!--<span class="m_gotina">Hola Soy Gotina!</span>-->
            <div id="gotin" class="gotin"></div>
            <div id="goton" class="goton"></div>
        </div>
    </div>
    @include('Layouts.logos')

		<script src="js/jquery.min.js"></script>
		<script src="js/bootstrap.min.js"></script>
    <script>
    $("#gotina").click(function(){
      $("#Mgotina").modal("show");
    });

    $("#gotin").click(function(){
      $("#Mgotin").modal("show");
    })

    $("#goton").click(function(){
      $("#Mgoton").modal("show");
    })

    /*$("#gotina").hover(
      function(){
        alert("Hola soy Gotina!");
      },
      function(){
        alert("Bye!");
      });*/
    </script>
<div class="modal fade bd-example-modal-lg"  id="Mgotina" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-body">
      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
        <div class="info_gotina"><div class="info_gotina_descripcion">
          Es una hermosa heroina, le encata el color fucsia, muestra de ellos es que su lazo, botas y capas son de este color. Es de ojos expresivos le encanta conversar con los niños y adora a su hermano Gotín. <br>
          Gotina tiene una mochila fucsia, donde guarda sus "herramientas de ahorro" y las comparte con sus amiguitos. <br>
          Gotina es una niña muy feliz, su lema es "Un gesto cuenta" y lo multiplica con el resto de niños y niñas. Le pone triste cuando ve que alguna persona desperdicia el agua, o lancen basura a la calle.
        </div></div>
        
      </div>
    </div>
  </div>
</div>
<div class="modal fade bd-example-modal-lg"  id="Mgoton" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-body">
      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
        <div class="info_goton"><div class="info_goton_descripcion">
          Es el villano de los Guardianes del Agua, tiene un aspecto malvado con su traje oscuro, este villano nace en el año 2017. <br>
          Goton representa el desperdicio y el mal uso del agua potable.
        </div></div>
      </div>
    </div>
  </div>
</div>
<div class="modal fade bd-example-modal-lg"  id="Mgotin" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-body">
      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
        <div class="info_gotin"><div class="info_gotin_descripcion">
          Gotín es hermano de Gotina, ambos comparten el mismo lema y mision, promover el cuidado y ahorro del agua potable. <br>
          Gotín se distingue por su capa de héroe color verde limón, sus botas y mochila son del mismo color. Siempre está cuidando a su hermana Gotina, la protege y le enseña tips de ahorro. <br>
          Al igual que Gotina tiene su mochila donde guarda sus "herramientas de ahorro". Es alegre y muy respetuoso con todos los niños.  <br>
          Estos hermanos se llevan estupendamente, y mientras cumplen su misión, se divierten en cada centro educativo que van. <br>
          Desde el 2017 emprendieron una campaña de concienciación en la ciudad de Portoviejo, con los niños y niñas de los centros educativos.
        </div></div>
      </div>
    </div>
  </div>
</div>

</body>
</html>