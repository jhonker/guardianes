<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1.0, shrink-to-fit=no">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Guardianes del Agua</title>
    <meta name="keywords" content="Portoaguas Guardianesdelagua Gotin Gotina #ungestocuenta" />
	<meta name="description" content="Porque todos son guardianes del agua">
	<meta name="author" content="www.portoaguas.gob.ec">
		<script src="js/modernizr.min.js"></script>


	<link rel="stylesheet" href="css/bootstrap.css">
    <link rel="stylesheet" href="css/fonts.css">
    <link rel="stylesheet" href="css/style.css">

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">


    <link rel="stylesheet" href="css/bootstrap.min.css">
	<link rel="stylesheet" href="css/fontawesome-all.min.css">
    <link rel="stylesheet" href="css/guardianes.css">
    <link rel="stylesheet" href="lightgallery/css/lightgallery.css">

</head>
<body>
    
@include('Layouts.menu')
<div class="container">
    <h2 class="titulo_p gotivideo-t">Concursos</h2>
    <?php 
    	$cont=0; $valv=''; $confot=0; $p=0;
	?>
	<div class="row galeria">
		@foreach($publi as $gn)
		
		<div class="col-md-6 wow-outer">
			<article class="post-modern wow slideInLeft rainbow-button"><div class="post-modern-media-concurso">
			@foreach($fotos as $ft)
				@if($ft->id_publicacion == $gn->id)
					<div id="aniimated-thumbnials<?php echo $cont; ?>">
						<a href="{{asset($ft->foto)}}" >
						<img src="{{$ft->foto}}" alt="" width="571" height="353">
						</img>
						</a>
					</div>
					<div class="fecha-publicacion">
	            		<h3>Fecha Publicacion: {{$ft->fecha_publicacion}}</h3>
	            	</div>
	            	<?php $cont++; ?>
				@endif

			@endforeach
			</div>
	            <div id="obj1" class="post-modern-meta-concurso">
	            	<p id="p1<?php echo $p;?>">
	            		<?php
	            		$cade= $gn->contenido;
	            		$lon= strlen($cade);
	            		echo $cade;
	            		 ?>
	            	</p>
	            	
	            </div>
	        </article>
	        <?php $cade=''; $men=''; $cade1=''; $men1=''; $p++; ?>
    	</div>
    	<br>
        @endforeach
	</div>					
</div>

<!-- Modal -->
<div class="modal fade" id="photos" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
        <h5 class="modal-title" id="exampleModalLabel">Crear Usuario</h5>
      </div>
      <div class="modal-body">
        <div class="row">
            <input type="text" name="" id="idphoto" class="form-control">
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
        <button type="button" class="btn btn-primary">Guardar cambios</button>
      </div>
    </div>
  </div>
</div>


@include('Layouts.logos')

		<script src="js/jquery.min.js"></script>
        <script src="js/bootstrap.min.js"></script>
        <script src="lightgallery/js/lightgallery.js"></script>
		<script src="lightgallery/modules/lg-thumbnail.min.js"></script>
		<script src="lightgallery/modules/lg-fullscreen.min.js"></script>
        
        <script>
        	var c= '<?php echo $cont; ?>';
        	for(i=0; i<c;i++)
        	{
	        	$('#aniimated-thumbnials'+i).lightGallery({
				    thumbnail:true
				}); 
			}
		</script>

	<script type="text/javascript">
		
		function utf8_to_b64( str ) {
	      return window.btoa(unescape(encodeURIComponent( str )));
	    }
	</script>
</body>
</html>