<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1.0, shrink-to-fit=no">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Guardianes del Agua</title>
    <meta name="keywords" content="Portoaguas Guardianesdelagua Gotin Gotina #ungestocuenta" />
	<meta name="description" content="Porque todos son guardianes del agua">
	<meta name="author" content="www.portoaguas.gob.ec">
		<script src="js/modernizr.min.js"></script>
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/guardianes.css">
</head>
<body>
    @include('Layouts.menu')
    <div class="row center-m0">
    <h2 class="titulo_p">¿Conoces bien el Ciclo Integral del Agua?</h2>
        <div class="proceso"></div>        
    </div>
    @include('Layouts.logos')
		<script src="js/jquery.min.js"></script>
		<script src="js/bootstrap.min.js"></script>

</body>
</html>