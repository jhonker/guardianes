
<!DOCTYPE html>
<html>
	<head>

		<!-- Basic -->
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">	

		<title>GotiGaleria</title>	

		<meta name="keywords" content="Portoaguas Guardianesdelagua Gotin Gotina #ungestocuenta gotiahorros" />
		<meta name="description" content="Porque todos son guardianes del agua">
		<meta name="author" content="www.portoaguas.gob.ec">

		<!-- Favicon -->
		<link rel="shortcut icon" href="img/device.png" type="image/x-icon" />
		<link rel="apple-touch-icon" href="img/apple-touch-icon.png">

		<!-- Mobile Metas -->
		<meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1.0, shrink-to-fit=no">

		<!-- Web Fonts  -->
		<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800%7CShadows+Into+Light" rel="stylesheet" type="text/css">

		<!-- Vendor CSS -->
		<link rel="stylesheet" href="{{asset('css/bootstrap.min.css')}}">
		<link rel="stylesheet" href="{{asset('css/fontawesome-all.min.css')}}">
		<link rel="stylesheet" href="{{asset('css/animate.min.css')}}">
		<link rel="stylesheet" href="{{asset('css/simple-line-icons.min.css')}}">
		<link rel="stylesheet" href="{{asset('css/owl.carousel.min.css')}}">
		<link rel="stylesheet" href="{{asset('css/owl.theme.default.min.css')}}">
		<link rel="stylesheet" href="{{asset('css/magnific-popup.min.css')}}">

		<!-- Theme CSS -->
		<link rel="stylesheet" href="{{asset('css/theme.css')}}">
		<link rel="stylesheet" href="{{asset('css/theme-elements.css')}}">
		<link rel="stylesheet" href="{{asset('css/theme-blog.css')}}">
		<link rel="stylesheet" href="{{asset('css/theme-shop.css')}}">
		<link rel="stylesheet" href="{{asset('lightgallery/css/lightgallery.css')}}">
		
		<!-- Demo CSS -->


		<!-- Skin CSS -->
		<link rel="stylesheet" href="{{asset('css/default.css')}}">	
		<script src="{{asset('js/style.switcher.localstorage.js')}}"></script> 


		<!-- Theme Custom CSS -->
		<link rel="stylesheet" href="{{asset('css/custom.css')}}">

		<!-- Head Libs -->
		<script src="{{asset('js/modernizr.min.js')}}"></script>

	</head>
	<body>

		<div class="body">
			<header id="header" data-plugin-options="{'stickyEnabled': true, 'stickyEnableOnBoxed': true, 'stickyEnableOnMobile': true, 'stickyStartAt': 55, 'stickySetTop': '-13px', 'stickyChangeLogo': true}">
				<div class="header-body">
					<div class="header-container container">
						<div class="header-row">
							<div class="header-column">
								<div class="header-row">
									<div class="header-logo">
										<a href="/">
                                                <img alt="Guardianes del agua" width="111" height="54" data-sticky-width="120" data-sticky-height="50" data-sticky-top="0" src="{{asset('img/logo.png')}}">
										</a>
									</div>
								</div>
							</div>
							<div class="header-column justify-content-end">
								<div class="header-row pt-3">
								
									<div class="header-search d-none d-md-block">
                                            <a href="http://portoaguas.gob.ec/online/formulario.php" target='black' class="btn btn-lg btn-primary">Registrate!</a>
									</div>
								</div>
								<div class="header-row">
									<div class="header-nav">
										<div class="header-nav-main header-nav-main-effect-1 header-nav-main-sub-effect-1">
											<nav class="collapse">
												<ul class="nav nav-pills" id="mainNav">
													<li>
														<a href="/">
															Inicio
														</a>
													</li>
													<li>
														<a  href="/#home-intro">
																Conocenos
														</a>
													</li>
													<li class="dropdown dropdown-mega">
														<a class="dropdown-item dropdown-toggle active"  href="#">
															Multimedia
														</a>
														<ul class="dropdown-menu">
															<li>
																<div class="dropdown-mega-content">
																	<div class="row">
																		<div class="col-lg-3">
																			<span class="dropdown-mega-sub-title">GotiAhorros</span>
																			<ul class="dropdown-mega-sub-nav">
																				<li><a class="dropdown-item" href="/gotiahorro">Ver mas..</a></li>
																			</ul>
																		</div>
																		<div class="col-lg-3">
																			<span class="dropdown-mega-sub-title">GotiGaleria</span>
																			<ul class="dropdown-mega-sub-nav">
																				<li><a class="dropdown-item" href="/gotigaleria">Ver mas..</a></li>
																			</ul>
																		</div>
																		<div class="col-lg-3">
																			<span class="dropdown-mega-sub-title">GotiVideos</span>
																			<ul class="dropdown-mega-sub-nav">
																				<li><a class="dropdown-item" href="/gotivideo">Ver mas..</a></li>
																			</ul>
																		</div>
																		<div class="col-lg-3">
																			<span class="dropdown-mega-sub-title">Descarga y Diviertete</span>
																			<ul class="dropdown-mega-sub-nav">
																				<li><a class="dropdown-item" href="/descarga-y-diviertete">Ver mas..</a></li>
																			</ul>
																		</div>
																	</div>
																</div>
															</li>
														</ul>
													</li>
													
													
													<li>
															<a href="/#galeria">
																Galeria
															</a>
														</li>
													<li >
														<a  href="#footer">
															CONTACTO
														</a>
													</li>
												</ul>
											</nav>
										</div>
										<ul class="header-social-icons social-icons d-none d-sm-block">
											<li class="social-icons-facebook"><a href="http://www.facebook.com/" target="_blank" title="Facebook"><i class="fab fa-facebook-f"></i></a></li>
											<li class="social-icons-twitter"><a href="http://www.twitter.com/" target="_blank" title="Twitter"><i class="fab fa-twitter"></i></a></li>
										</ul>
										<button class="btn header-btn-collapse-nav" data-toggle="collapse" data-target=".header-nav-main nav">
											<i class="fas fa-bars"></i>
										</button>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</header>

			<div role="main" class="main">

				<section class="page-header">
					<div class="container">
						<div class="row">
							<div class="col">
								<ul class="breadcrumb">
									<li><a href="#">Inicio</a></li>
									<li class="active">Multimedia</li>
								</ul>
							</div>
						</div>
						<div class="row">
							<div class="col">
								<h1>GotiGaleria</h1>
							</div>
						</div>
					</div>
				</section>
				<div class="container">
					<div class="row">
						<div class="col">
							
							<h3 style="text-align: center;">nombre del album</h3>
							<hr class="tall">
                            <div class="row">
                            <div id="aniimated-thumbnials">
								@foreach($album as $a)
								<a href="{{asset($a->foto)}}" >
									<img src="{{asset($a->foto)}}" class="{{$a->titulo}}" style=" height: 10em; width: 20%; margin: 10px 10px 0 0;"/>
								</a>
								@endforeach
							  </div>
                               
                            </div>
						</div>
					</div>
				</div>
				<br>
				<div class="container">
					<div class="row">
						<div class="col">
							<h3 style="text-align: center;">Otros Albumes</h3>
							<hr class="tall">
                            <div class="row">
								@foreach($galerias as $a)
                                <div class="col-md-6">
                                    <div style="    margin-top: 20px;">
                                        <a href="/gotigaleria/{{$a->id}}">
                                            <div class="_2gxa" style="width:100%;height:202px;">
											<div class="uiScaledImageContainer" style="width:100%;height:202px;">
												@foreach($foto_portada as $fp )
													@if($fp->id_album==$a->id)
														<img class="scaledImageFitWidth img" src="{{asset($fp->foto)}}" style="top:-76px;" alt="La imagen puede contener: Kerly Lissette Valdiviezo y Jhony Guaman, personas sonriendo, primer plano" width="404" height="403">
													@endif
												@endforeach
                                            </div>
                                            <div class="_2gxb">
                                                <div>
                                                    <div class="_2gxd _2pis">
                                                        <span class="_2iem _50f7">{{$a->titulo}}</span>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="_4f0s"></div>
                                            </div>
                                        </a>
                                    </div>
								</div>
								@endforeach
                               
                            </div>
						</div>
					</div>
				</div>
			</div>

			<footer id="footer">
					<div class="container">
						<div class="row">
							<div class="col-lg-6">
								<div class="contact-details">
									<h4>Contacto</h4>
									<ul class="contact">
										<li><p><i class="fas fa-map-marker-alt"></i> <strong>Dirección:</strong> Ciudadela el Maestro calle san francisco y km 1/2 via crucita</p></li>
										<li><p><i class="fas fa-phone"></i> <strong>Teléfono:</strong> 053701960 ext 630</p></li>
										<li><p><i class="far fa-envelope"></i> <strong>Email:</strong> <a href="mailto:info@portoaguas.gob.ec">info@portoaguas.gob.ec</a></p></li>
									</ul>
								</div>
							</div>
							<div class="col-lg-3">
								<h4>Redes Sociales</h4>
								<ul class="social-icons">
									<li class="social-icons-facebook"><a href="https://www.facebook.com/PortoaguasEP/?ref=br_rs" target="_blank" title="Facebook"><i class="fab fa-facebook-f"></i></a></li>
									<li class="social-icons-twitter"><a href="https://twitter.com/Portoaguas?lang=es" target="_blank" title="Twitter"><i class="fab fa-twitter"></i></a></li>
									<li class="social-icons-youtube"><a href="https://www.youtube.com/channel/UCrLZJOnCWXaCmpb6CcoPZBg/videos" target="_blank" title="Youtube"><i class="fab fa-youtube"></i></a></li>
								</ul>
							</div>
							<div class="col-lg-3">
								<a href="http://portoviejo.gob.ec" target="Portoviejo"><img src="{{asset('img/municipio.png')}}" style="width: 175px; "></a>
								<a href="http://portoaguas.gob.ec" target="Portoaguas"><img src="{{asset('img/portoaguas-footer.png')}}" style="width: 135px;height:  35px;"></a>
							</div>
						</div>
					</div>
					<!--<div class="footer-copyright">
						<div class="container">
							<div class="row">
								<div class="col-lg-1">
									<a href="index.html" class="logo">
										<img alt="#UnGestoCuenta" class="img-fluid" src="img/logo-footer.png">
									</a>
								</div>
								<div class="col-lg-7">
									<p>© Copyright 2018. All Rights Reserved.</p>
								</div>
								<div class="col-lg-4">
									<nav id="sub-menu">
										<ul>
											<li><a href="page-faq.html"><img src="img/municipio.png" style="width: 175px; "></a></li> 
											<li><a href="sitemap.html"><img src="img/portoaguas-footer.png" style="width: 135px;height:  35px;"></a></li> </ul>
									</nav>
								</div>
							</div>
						</div>
					</div>-->
				</footer>
		</div>


		<!-- Vendor -->
		<script src="{{asset('js/jquery.min.js')}}"></script>
		<script src="{{asset('js/jquery.appear.min.js')}}"></script>
		<script src="{{asset('js/jquery.easing.min.js')}}"></script>
		<script src="{{asset('js/jquery-cookie.min.js')}}"></script>
		<script src="{{asset('js/popper.min.js')}}"></script>
		<script src="{{asset('js/bootstrap.min.js')}}"></script>
		<script src="{{asset('js/common.min.js')}}"></script>
		<script src="{{asset('js/jquery.validation.min.js')}}"></script>
		<script src="{{asset('js/jquery.gmap.min.js')}}"></script>
		<script src="{{asset('js/jquery.lazyload.min.js')}}"></script>
		<script src="{{asset('js/jquery.isotope.min.js')}}"></script>
		<script src="{{asset('js/owl.carousel.min.js')}}"></script>
		<script src="{{asset('js/jquery.magnific-popup.min.js')}}"></script>
		<script src="{{asset('js/vide.min.js')}}"></script>
		<!-- Theme Base, Components and Settings -->
		<script src="{{asset('js/theme.js')}}"></script>
		
		<!-- Theme Custom -->
		<script src="{{asset('js/custom.js')}}"></script>
		
		<!-- Theme Initialization Files -->
		<script src="{{asset('js/theme.init.js')}}"></script>

		<!-- Examples -->
		<script src="{{asset('js/examples.gallery.js')}}"></script>	
		<script src="{{asset('lightgallery/js/lightgallery.js')}}"></script>
		<script src="{{asset('lightgallery/modules/lg-thumbnail.min.js')}}"></script>
		<script src="{{asset('lightgallery/modules/lg-fullscreen.min.js')}}"></script>
		<script>
			$('#aniimated-thumbnials').lightGallery({
				thumbnail:true
			}); 
		
			
			(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
			(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
			m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
			})(window,document,'script','//www.google-analytics.com/analytics.js','ga');
		
			ga('create', 'UA-42715764-5', 'auto');
			ga('send', 'pageview');

			jQuery(document).ready(function(){
			    linkInterno = $('a[href^="#"]');
    			linkInterno.on('click',function(e) {	
    			e.preventDefault();
    			var href = $(this).attr('href');
    				$('html, body').animate({ scrollTop : $( href ).offset().top }, 'slow');
    			});
			});

		</script>
		<script src="{{asset('js/analytics.js')}}"></script>

	</body>
</html>
