<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1.0, shrink-to-fit=no">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Guardianes del Agua</title>
    <meta name="keywords" content="Portoaguas Guardianesdelagua Gotin Gotina #ungestocuenta" />
	<meta name="description" content="Porque todos son guardianes del agua">
	<meta name="author" content="www.portoaguas.gob.ec">
		<script src="js/modernizr.min.js"></script>
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/guardianes.css">
</head>
<body>
    @include('Layouts.menu')

    <div class="hom-t">
        <h1 class="title-g">Conoce quiénes son los</h1>
        <div class="logo-principal"></div>
        
    </div>
    <div class="texto">
      <p class="t_lato">Son todos los niños, niñas y adolescentes de los centro educativos de Portoviejo, Interesados en formar una conciencia social cada vez más identificada con los valores de preservación del agua, el medio ambiente y el pago oportuno del agua que se consume.</p>
    </div>
    <div class="banner-azul">
      <div class="row padding-top-3">
        <div class="col-md-4 center linea">
          <span class="title_p">Misión</span>
          <p class="texto_p">"Contribuir con la gestiòn sostenible del agua potable, promoviendo la corresponsabilidad de la niñez y adolescencia de Portoviejo."</p>
        </div>
        <div class="col-md-4 center">
          <span class="title_p">Visión</span>
          <p class="texto_p">Para el año lectivo 2019 institucionalizar a los Guardianes del Agua en todos los centros educativos de Portoviejo.</p>
        </div>
        <div class="col-md-4 center">
          <span class="title_p">Objetivo</span>
          <p class="texto_p">Participar de las acciones y estrategias de nuestra sociedad, que conlleven a la salud pública de los habitantes de Portoviejo y acercarnos a la cultura del buen uso del agua, adoptarla en todos los ambitos de nuestra vida diaria.</p>
        </div>
      </div>
    </div>
    @include('Layouts.logos')

		<script src="js/jquery.min.js"></script>
		<script src="js/bootstrap.min.js"></script>

</body>
</html>