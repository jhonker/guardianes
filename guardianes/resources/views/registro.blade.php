
<!DOCTYPE html>
<html>
<head>

  <!-- Basic -->
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge"> 

  <title>GotiAhorros</title>  

  <meta name="keywords" content="Portoaguas Guardianesdelagua Gotin Gotina #ungestocuenta gotiahorros" />
  <meta name="description" content="Porque todos son guardianes del agua">
  <meta name="author" content="www.portoaguas.gob.ec">

  <!-- Favicon -->
  <link rel="shortcut icon" href="img/device.png" type="image/x-icon" />
  <link rel="apple-touch-icon" href="img/apple-touch-icon.png">

  <!-- Mobile Metas -->
  <meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1.0, shrink-to-fit=no">

  <!-- Web Fonts  -->
  <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800%7CShadows+Into+Light" rel="stylesheet" type="text/css">

  <!-- Vendor CSS -->
  <link rel="stylesheet" href="css/bootstrap.min.css">
  <link rel="stylesheet" href="css/fontawesome-all.min.css">
  <link rel="stylesheet" href="css/animate.min.css">
  <link rel="stylesheet" href="css/simple-line-icons.min.css">
  <link rel="stylesheet" href="css/owl.carousel.min.css">
  <link rel="stylesheet" href="css/owl.theme.default.min.css">
  <link rel="stylesheet" href="css/magnific-popup.min.css">

  <!-- Theme CSS -->
  <link rel="stylesheet" href="css/theme.css">
  <link rel="stylesheet" href="css/theme-elements.css">
  <link rel="stylesheet" href="css/theme-blog.css">
  <link rel="stylesheet" href="css/theme-shop.css">

  <!-- Demo CSS -->
  <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />

  <!-- Skin CSS -->
  <link rel="stylesheet" href="css/default.css">  
  <script src="js/style.switcher.localstorage.js"></script> 


  <!-- Theme Custom CSS -->
  <link rel="stylesheet" href="css/custom.css">

  <!-- Head Libs -->
  <script src="js/modernizr.min.js"></script>

</head>
<body>

  <div class="body">
    <header id="header" data-plugin-options="{'stickyEnabled': true, 'stickyEnableOnBoxed': true, 'stickyEnableOnMobile': true, 'stickyStartAt': 55, 'stickySetTop': '-13px', 'stickyChangeLogo': true}">
      <div class="header-body">
        <div class="header-container container">
          <div class="header-row">
            <div class="header-column">
              <div class="header-row">
                <div class="header-logo">
                  <a href="/">
                    <img alt="Portoaguas" width="111" height="54" data-sticky-width="120" data-sticky-height="50" data-sticky-top="0" src="img/logo.png">
                  </a>
                </div>
              </div>
            </div>
            <div class="header-column justify-content-end">
              <div class="header-row pt-3">
                <div class="header-search d-none d-md-block">
                  <a href="#" class="btn btn-lg btn-primary">Registrate!</a>
                </div>
              </div>
              <div class="header-row">
                <div class="header-nav">
                  <div class="header-nav-main header-nav-main-effect-1 header-nav-main-sub-effect-1">
                    <nav class="collapse">
                      <ul class="nav nav-pills" id="mainNav">
                        <li>
                          <a href="/">
                            Inicio
                          </a>
                        </li>
                        <li>
                          <a  href="/#home-intro">
                            Conocenos
                          </a>
                        </li>
                        <li class="dropdown dropdown-mega">
                          <a class="dropdown-item dropdown-toggle active"  href="#">
                            Multimedia
                          </a>
                          <ul class="dropdown-menu">
                            <li>
                              <div class="dropdown-mega-content">
                                <div class="row">
                                  <div class="col-lg-3">
                                    <span class="dropdown-mega-sub-title">GotiAhorros</span>
                                    <ul class="dropdown-mega-sub-nav">
                                      <li><a class="dropdown-item" href="gotiahorro.html">Ver mas..</a></li>
                                    </ul>
                                  </div>
                                  <div class="col-lg-3">
                                    <span class="dropdown-mega-sub-title">GotiGaleria</span>
                                    <ul class="dropdown-mega-sub-nav">
                                      <li><a class="dropdown-item" href="gotigaleria.html">Ver mas..</a></li>
                                    </ul>
                                  </div>
                                  <div class="col-lg-3">
                                    <span class="dropdown-mega-sub-title">GotiVideos</span>
                                    <ul class="dropdown-mega-sub-nav">
                                      <li><a class="dropdown-item" href="gotivideo.html">Ver mas..</a></li>
                                    </ul>
                                  </div>
                                  <div class="col-lg-3">
                                    <span class="dropdown-mega-sub-title">Descarga y Diviertete</span>
                                    <ul class="dropdown-mega-sub-nav">
                                      <li><a class="dropdown-item" href="descarga-y-diviertete.html">Ver mas..</a></li>
                                    </ul>
                                  </div>
                                </div>
                              </div>
                            </li>
                          </ul>
                        </li>
                        <li>
                          <a href="/#galeria">
                            Galeria
                          </a>
                        </li>
                        <li >
                          <a  href="/#footer">
                            CONTACTO
                          </a>
                        </li>
                      </ul>
                    </nav>
                  </div>
                  <ul class="header-social-icons social-icons d-none d-sm-block">
                    <li class="social-icons-facebook">
                        <a href="http://www.facebook.com/" target="_blank" title="Facebook">
                            <i class="fab fa-facebook-f"></i></a></li>
                            <li class="social-icons-twitter"><a href="http://www.twitter.com/" target="_blank" title="Twitter"><i class="fab fa-twitter"></i>
                        </a>
                    </li>
                  </ul>
                  <button class="btn header-btn-collapse-nav" data-toggle="collapse" data-target=".header-nav-main nav">
                    <i class="fas fa-bars"></i>
                  </button>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </header>

    <div role="main" class="main">

      <section class="page-header">
        <div class="container">
          <div class="row">
            <div class="col">
              <ul class="breadcrumb">
                <li><a href="#">Inicio</a></li>
                <li class="active">Multimedia</li>
              </ul>
            </div>
          </div>
          <div class="row">
            <div class="col">
              <h1>Registrate</h1>
            </div>
          </div>
        </div>
      </section>

      <div class="container">
        <div class="row">
          <div class="col">
            <h2>Registra a tu Centro de educación</h2>
          </div>
        </div>
      </div>
      <div class="container">

        <div class="row">
          <div class="col">
            <hr class="tall">
          
            <!--<h4>Masonry</h4>-->
            <div class="row">
                <div class="col-md-2"></div>
                <div class="col-md-8">
                    <div class="form-group">
                      <label for="unidad_educativa">Unidad/Centro Educativo <span class="importante">*</span></label>
                      <select class="js-example-basic-multiple form-control" name="states[]" multiple="multiple" id="unidad_educativa">
                          <option value="0">Seleccione una unidad educativa</option>
                          @foreach ($unidades as $u)
                          <option value="{{$u->codigo}}">{{$u->unidad}}</option>
                          @endforeach
                      </select>
                    </div>
                    <div class="form-group">
                      <label for="direccion_plantel">Dirección del Plantel <span class="importante">*</span></label>
                      <input type="text" class="form-control" id="direccion_plantel"  placeholder="Ingrese Direccion del Plantel" style="text-transform: uppercase;"  onkeyup="javascript:this.value=this.value.toUpperCase()">
                    </div>
                    <div class="row col-md-12 d_director">
                      <div class="form-group col-md-4 row-15">
                        <label for="email">Correo Electrónico <span class="importante">*</span></label>
                        <input type="text" class="form-control" id="email" placeholder="Ingrese Correo Electrónico">
                      </div>
                      <div class=" form-group col-md-4">
                        <label for="telefono">Teléfono <span class="importante">*</span></label>
                        <input type="text" class="form-control" id="telefono" placeholder="Ingrese teléfono" onkeyPress="return solo_numeros(event)">
                      </div>
                      <div class="form-group col-md-4 row-r-0">
                        <label for="numero_estudiantes">Número de estudiantes <span class="importante">*</span></label>
                        <input type="text" class="form-control" id="numero_estudiantes" aria-describedby="emailHelp" placeholder="Ingrese número de estudiantes" data-toggle="tooltip" data-placement="bottom" title="Estudiantes de 3ro a 7mo. de básica" onkeyPress="return solo_numeros(event)" >
                      </div>
                    </div>
                    <div class="row col-md-12 d_director">
                      <div class="form-group col-md-6 row-15">
                        <label for="nombre_director">Nombre del Director(a)/Rector(a) <span class="importante">*</span></label>
                        <input type="text" class="form-control" id="nombre_director" placeholder="Ingrese Nombre del Direcctor" style="text-transform: uppercase;"  onkeyup="javascript:this.value=this.value.toUpperCase()">
                      </div>
                      <div class="form-group col-md-6 row-r-0">
                        <label for="cedula_director">Número de cédula <span class="importante">*</span></label>
                        <input type="text" class="form-control" id="cedula_director" placeholder="Ingrese número de cédula" required size="13" maxlength="13" onkeyPress="return solo_numeros(event)">
                      </div>
                    </div>
                    <strong> <p class="text-center">Datos del Solicitante</p></strong>
                    <div class="row col-md-12 d_director">
                      <div class="form-group col-md-6 row-15">
                        <label for="nombre_solicitante">Nombres <span class="importante">*</span></label>
                        <input type="text" class="form-control" id="nombre_solicitante" placeholder="Ingrese nombre del solicitante" style="text-transform: uppercase;"  onkeyup="javascript:this.value=this.value.toUpperCase()">
                      </div>
                      <div class=" form-group col-md-6 row-r-0">
                        <label for="cedula_solicitante">Número de cédula <span class="importante">*</span></label>
                        <input type="text" class="form-control" id="cedula_solicitante" placeholder="Ingrese cédula del solicitante" required size="13" maxlength="13" onkeyPress="return solo_numeros(event)">
                      </div>

                    </div>
                    <div class="form-group col-md-12 d_director">
                      <label for="cargo">Cargo dentro de la Institución <span class="importante">*</span></label>
                      <input type="text" class="form-control" id="cargo" placeholder="Ingrese cargo dentro la Institución" data-toggle="tooltip" data-placement="bottom" title="Cargo dentro de la Institución" style="text-transform: uppercase;"  onkeyup="javascript:this.value=this.value.toUpperCase()">
                    </div>
                    <div class="aling-button">
                      <button type="button" id="btn_registrar" class=" btn btn-info col-md-offset-3 col-md-6">Registrar</button>
                    </div>
                  </div>
                  
            </div>          
          </div>
        </div>
      </div>
    </div>

          <!--<div class="row pb-4">
            <div class="col">
              <hr class="tall">

              <h4 class="mb-0">Thumb Gallery</h4>
              <p>Check the examples file <code>examples.gallery.js</code> for more information.</p>

              <div class="row justify-content-center mt-4">
                <div class="col-lg-4 mx-auto">
                  <div class="thumb-gallery">
                    <div class="owl-carousel owl-theme manual thumb-gallery-detail show-nav-hover" id="thumbGalleryDetail">
                      <div>
                        <span class="img-thumbnail d-block">
                          <img alt="Project Image" src="img/project.jpg" class="img-fluid">
                        </span>
                      </div>
                      <div>
                        <span class="img-thumbnail d-block">
                          <img alt="Project Image" src="img/project-2.jpg" class="img-fluid">
                        </span>
                      </div>
                      <div>
                        <span class="img-thumbnail d-block">
                          <img alt="Project Image" src="img/project-4.jpg" class="img-fluid">
                        </span>
                      </div>
                      <div>
                        <span class="img-thumbnail d-block">
                          <img alt="Project Image" src="img/project-5.jpg" class="img-fluid">
                        </span>
                      </div>
                      <div>
                        <span class="img-thumbnail d-block">
                          <img alt="Project Image" src="img/project-6.jpg" class="img-fluid">
                        </span>
                      </div>
                      <div>
                        <span class="img-thumbnail d-block">
                          <img alt="Project Image" src="img/project-2.jpg" class="img-fluid">
                        </span>
                      </div>
                      <div>
                        <span class="img-thumbnail d-block">
                          <img alt="Project Image" src="img/project-4.jpg" class="img-fluid">
                        </span>
                      </div>
                    </div>
                    <div class="owl-carousel owl-theme manual thumb-gallery-thumbs mt" id="thumbGalleryThumbs">
                      <div>
                        <span class="img-thumbnail d-block cur-pointer">
                          <img alt="Project Image" src="img/project.jpg" class="img-fluid">
                        </span>
                      </div>
                      <div>
                        <span class="img-thumbnail d-block cur-pointer">
                          <img alt="Project Image" src="img/project-2.jpg" class="img-fluid">
                        </span>
                      </div>
                      <div>
                        <span class="img-thumbnail d-block cur-pointer">
                          <img alt="Project Image" src="img/project-4.jpg" class="img-fluid">
                        </span>
                      </div>
                      <div>
                        <span class="img-thumbnail d-block cur-pointer">
                          <img alt="Project Image" src="img/project-5.jpg" class="img-fluid">
                        </span>
                      </div>
                      <div>
                        <span class="img-thumbnail d-block cur-pointer">
                          <img alt="Project Image" src="img/project-6.jpg" class="img-fluid">
                        </span>
                      </div>
                      <div>
                        <span class="img-thumbnail d-block cur-pointer">
                          <img alt="Project Image" src="img/project-2.jpg" class="img-fluid">
                        </span>
                      </div>
                      <div>
                        <span class="img-thumbnail d-block cur-pointer">
                          <img alt="Project Image" src="img/project-4.jpg" class="img-fluid">
                        </span>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="col-lg-4 mx-auto">
                  <div class="thumb-gallery">
                    <div class="owl-carousel owl-theme manual thumb-gallery-detail" id="thumbGalleryDetail2">
                      <div>
                        <span class="img-thumbnail d-block">
                          <img alt="Project Image" src="img/project.jpg" class="img-fluid">
                        </span>
                      </div>
                      <div>
                        <span class="img-thumbnail d-block">
                          <img alt="Project Image" src="img/project-2.jpg" class="img-fluid">
                        </span>
                      </div>
                      <div>
                        <span class="img-thumbnail d-block">
                          <img alt="Project Image" src="img/project-4.jpg" class="img-fluid">
                        </span>
                      </div>
                      <div>
                        <span class="img-thumbnail d-block">
                          <img alt="Project Image" src="img/project-5.jpg" class="img-fluid">
                        </span>
                      </div>
                      <div>
                        <span class="img-thumbnail d-block">
                          <img alt="Project Image" src="img/project-6.jpg" class="img-fluid">
                        </span>
                      </div>
                      <div>
                        <span class="img-thumbnail d-block">
                          <img alt="Project Image" src="img/project-2.jpg" class="img-fluid">
                        </span>
                      </div>
                      <div>
                        <span class="img-thumbnail d-block">
                          <img alt="Project Image" src="img/project-4.jpg" class="img-fluid">
                        </span>
                      </div>
                    </div>
                    <div class="owl-carousel owl-theme manual thumb-gallery-thumbs mt" id="thumbGalleryThumbs2">
                      <div>
                        <span class="img-thumbnail d-block cur-pointer">
                          <img alt="Project Image" src="img/project.jpg" class="img-fluid">
                        </span>
                      </div>
                      <div>
                        <span class="img-thumbnail d-block cur-pointer">
                          <img alt="Project Image" src="img/project-2.jpg" class="img-fluid">
                        </span>
                      </div>
                      <div>
                        <span class="img-thumbnail d-block cur-pointer">
                          <img alt="Project Image" src="img/project-4.jpg" class="img-fluid">
                        </span>
                      </div>
                      <div>
                        <span class="img-thumbnail d-block cur-pointer">
                          <img alt="Project Image" src="img/project-5.jpg" class="img-fluid">
                        </span>
                      </div>
                      <div>
                        <span class="img-thumbnail d-block cur-pointer">
                          <img alt="Project Image" src="img/project-6.jpg" class="img-fluid">
                        </span>
                      </div>
                      <div>
                        <span class="img-thumbnail d-block cur-pointer">
                          <img alt="Project Image" src="img/project-2.jpg" class="img-fluid">
                        </span>
                      </div>
                      <div>
                        <span class="img-thumbnail d-block cur-pointer">
                          <img alt="Project Image" src="img/project-4.jpg" class="img-fluid">
                        </span>
                      </div>
                    </div>
                  </div>
                </div>
              </div>

            </div>
          </div>-->

        </div>

      </div>

      <footer id="footer">
        <div class="container">
          <div class="row">
              <!--<div class="footer-ribbon">
                <span>#UnGestoCuenta</span>
              </div>
              <div class="col-lg-3">
                <div class="newsletter">
                  <h4>Suscribete</h4>
                  <div class="alert alert-success d-none" id="newsletterSuccess">
                    <strong>Success!</strong> You've been added to our email list.
                  </div>
                  <div class="alert alert-danger d-none" id="newsletterError"></div>
                  <form id="newsletterForm" action="php/newsletter-subscribe.php" method="POST">
                    <div class="input-group">
                      <input class="form-control form-control-sm" placeholder="Correo Electronico" name="newsletterEmail" id="newsletterEmail" type="text">
                      <span class="input-group-append">
                        <button class="btn btn-light" type="submit">Enviar</button>
                      </span>
                    </div>
                  </form>
                </div>
              </div>-->
              <div class="col-lg-6">
                <div class="contact-details">
                  <h4>Contacto</h4>
                  <ul class="contact">
                    <li><p><i class="fas fa-map-marker-alt"></i> <strong>Dirección:</strong> Ciudadela el Maestro calle san francisco y km 1/2 via crucita</p></li>
                    <li><p><i class="fas fa-phone"></i> <strong>Teléfono:</strong> 053701960 ext 630</p></li>
                    <li><p><i class="far fa-envelope"></i> <strong>Email:</strong> <a href="mailto:info@portoaguas.gob.ec">info@portoaguas.gob.ec</a></p></li>
                  </ul>
                </div>
              </div>
              <div class="col-lg-3">
                <h4>Redes Sociales</h4>
                <ul class="social-icons">
                  <li class="social-icons-facebook"><a href="https://www.facebook.com/PortoaguasEP/?ref=br_rs" target="_blank" title="Facebook"><i class="fab fa-facebook-f"></i></a></li>
                  <li class="social-icons-twitter"><a href="https://twitter.com/Portoaguas?lang=es" target="_blank" title="Twitter"><i class="fab fa-twitter"></i></a></li>
                  <li class="social-icons-youtube"><a href="https://www.youtube.com/channel/UCrLZJOnCWXaCmpb6CcoPZBg/videos" target="_blank" title="Youtube"><i class="fab fa-youtube"></i></a></li>
                </ul>
              </div>
              <div class="col-lg-3">
                <a href="http://portoviejo.gob.ec" target="Portoviejo"><img src="img/municipio.png" style="width: 175px; "></a>
                <a href="http://portoaguas.gob.ec" target="Portoaguas"><img src="img/portoaguas-footer.png" style="width: 135px;height:  35px;"></a>
              </div>
            </div>
          </div>
          <!--<div class="footer-copyright">
            <div class="container">
              <div class="row">
                <div class="col-lg-1">
                  <a href="index.html" class="logo">
                    <img alt="#UnGestoCuenta" class="img-fluid" src="img/logo-footer.png">
                  </a>
                </div>
                <div class="col-lg-7">
                  <p>© Copyright 2018. All Rights Reserved.</p>
                </div>
                <div class="col-lg-4">
                  <nav id="sub-menu">
                    <ul>
                      <li><a href="page-faq.html"><img src="img/municipio.png" style="width: 175px; "></a></li> 
                      <li><a href="sitemap.html"><img src="img/portoaguas-footer.png" style="width: 135px;height:  35px;"></a></li> </ul>
                  </nav>
                </div>
              </div>
            </div>
          </div>-->
        </footer>
      </div>


      <!-- Vendor -->
      <script src="js/jquery.min.js"></script>
      <script src="js/jquery.appear.min.js"></script>
      <script src="js/jquery.easing.min.js"></script>
      <script src="js/jquery-cookie.min.js"></script>
      <script src="js/popper.min.js"></script>
      <script src="js/bootstrap.min.js"></script>
      <script src="js/common.min.js"></script>
      <script src="js/jquery.validation.min.js"></script>
      <script src="js/jquery.gmap.min.js"></script>
      <script src="js/jquery.lazyload.min.js"></script>
      <script src="js/jquery.isotope.min.js"></script>
      <script src="js/owl.carousel.min.js"></script>
      <script src="js/jquery.magnific-popup.min.js"></script>
      <script src="js/vide.min.js"></script>
      <!-- Theme Base, Components and Settings -->
      <script src="js/theme.js"></script>
      <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>
  
      <!-- Theme Custom -->
      <script src="js/custom.js"></script>

      <!-- Theme Initialization Files -->
      <script src="js/theme.init.js"></script>

      <!-- Examples -->
      <script src="js/examples.gallery.js"></script>  

      <script>
        (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
          (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
          m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
        })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

        ga('create', 'UA-42715764-5', 'auto');
        ga('send', 'pageview');

        jQuery(document).ready(function(){
          linkInterno = $('a[href^="#"]');
          linkInterno.on('click',function(e) {  
            e.preventDefault();
            var href = $(this).attr('href');
            $('html, body').animate({ scrollTop : $( href ).offset().top }, 'slow');
          });
        });

        $('.js-example-basic-multiple').select2({
  			maximumSelectionLength: 1
		});
      </script>
      <script src="js/analytics.js"></script>

    </body>
    </html>
