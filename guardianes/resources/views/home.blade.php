<!DOCTYPE html>
<html>
	<head>

		<!-- Basic -->
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">	

		<title>Guardianes del Agua</title>	

		<meta name="keywords" content="Portoaguas Guardianesdelagua Gotin Gotina #ungestocuenta" />
		<meta name="description" content="Porque todos son guardianes del agua">
		<meta name="author" content="www.portoaguas.gob.ec">

		<!-- Favicon -->
		<link rel="shortcut icon" href="img/device.png" type="image/x-icon" />
		<link rel="apple-touch-icon" href="img/apple-touch-icon.png">

		<!-- Mobile Metas -->
		<meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1.0, shrink-to-fit=no">

		<!-- Web Fonts  
		<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800%7CShadows+Into+Light" rel="stylesheet" type="text/css">-->

		<link href="https://fonts.googleapis.com/css?family=Montserrat" rel="stylesheet">

		<!-- Estilos css -->
		<link rel="stylesheet" href="css/bootstrap.min.css">
		<link rel="stylesheet" href="css/fontawesome-all.min.css">
		<link rel="stylesheet" href="css/animate.min.css">
		<link rel="stylesheet" href="css/simple-line-icons.min.css">
		<link rel="stylesheet" href="css/owl.carousel.min.css">
		<link rel="stylesheet" href="css/owl.theme.default.min.css">
		<link rel="stylesheet" href="css/magnific-popup.min.css">

		<link rel="stylesheet" href="css/theme.css">
		<link rel="stylesheet" href="css/theme-elements.css">
		<!--<link rel="stylesheet" href="css/theme-blog.css">
		<link rel="stylesheet" href="css/theme-shop.css">-->

		<!-- Current Page CSS -->
		<link rel="stylesheet" href="css/settings.css">
		<link rel="stylesheet" href="css/layers.css">
		<link rel="stylesheet" href="css/navigation.css">
		
		<!-- Demo CSS -->


		<!-- Skin CSS -->
		<link rel="stylesheet" href="css/default.css">	

		<!--<script src="js/style.switcher.localstorage.js"></script> -->

		<!-- Theme Custom CSS -->
		<link rel="stylesheet" href="css/custom.css">

		<!-- Head Libs -->
		<script src="js/modernizr.min.js"></script>

	</head>
	<body>

		<div class="body">
			<header id="header" data-plugin-options="{'stickyEnabled': true, 'stickyEnableOnBoxed': true, 'stickyEnableOnMobile': true, 'stickyStartAt': 55, 'stickySetTop': '-30px', 'stickyChangeLogo': true}">
				<div class="header-body">
					<div class="header-container container">
						<div class="header-row">
							<div class="header-column">
								<div class="header-row">
									<div class="header-logo">
										<a href="/">
											<img alt="Guardianes" width="150" height="55" data-sticky-width="100" data-sticky-height="45" data-sticky-top="15" src="img/logo1.png">
										</a>
									</div>
								</div>
							</div>
							<div class="header-column justify-content-end">
								<div class="header-row pt-3">
								</div>
								<div class="header-row">
									<div class="header-nav">
										<div class="header-nav-main header-nav-main-effect-1 header-nav-main-sub-effect-1">
											<nav class="collapse">
												<ul class="nav nav-pills" id="mainNav">
													<li>
														<a class="active" href="#header">
															Inicio
														</a>
													</li>
													<li>
														<a  href="#home-intro">
															Conocenos
														</a>
													</li>
													<li class="dropdown dropdown-mega">
															<a class="dropdown-item dropdown-toggle"  href="#">
																Multimedia
															</a>
															<ul class="dropdown-menu">
																<li>
																	<div class="dropdown-mega-content">
																		<div class="row">
																			<div class="col-lg-3">
																				<span class="dropdown-mega-sub-title">GotiAhorros</span>
																				<ul class="dropdown-mega-sub-nav">
																					<li><a class="dropdown-item" href="gotiahorro">Ver mas..</a></li>
																				</ul>
																			</div>
																			<div class="col-lg-3">
																				<span class="dropdown-mega-sub-title">GotiGaleria</span>
																				<ul class="dropdown-mega-sub-nav">
																					<li><a class="dropdown-item" href="gotigaleria">Ver mas..</a></li>
																				</ul>
																			</div>
																			<div class="col-lg-3">
																				<span class="dropdown-mega-sub-title">GotiVideos</span>
																				<ul class="dropdown-mega-sub-nav">
																					<li><a class="dropdown-item" href="gotivideo">Ver mas..</a></li>
																				</ul>
																			</div>
																			<div class="col-lg-3">
																				<span class="dropdown-mega-sub-title">Descarga y Diviertete</span>
																				<ul class="dropdown-mega-sub-nav">
																					<li><a class="dropdown-item" href="descarga-y-diviertete">Ver mas..</a></li>
																				</ul>
																			</div>
																		</div>
																	</div>
																</li>
															</ul>
														</li>
													
													<li class="dropdown">
														<a href="#galeria">
															Eventos
														</a>
													</li>
													<li >
														<a  href="#footer">
															CONTACTO
														</a>
													</li>
												</ul>
											</nav>
										</div>
										<ul class="header-social-icons social-icons d-none d-sm-block">
											<li class="social-icons-facebook"><a href="https://www.facebook.com/PortoaguasEP/?ref=br_rs" target="_blank" title="Facebook"><i class="fab fa-facebook-f"></i></a></li>
											<li class="social-icons-twitter"><a href="https://twitter.com/Portoaguas?lang=es" target="_blank" title="Twitter"><i class="fab fa-twitter"></i></a></li>
											<li class="social-icons-youtube"><a href="https://www.youtube.com/channel/UCrLZJOnCWXaCmpb6CcoPZBg/videos" target="_blank" title="Youtube"><i class="fab fa-youtube"></i></a></li>
											<!--<li class="social-icons-linkedin"><a href="http://www.linkedin.com/" target="_blank" title="Linkedin"><i class="fab fa-linkedin-in"></i></a></li>-->
										</ul>
										<button class="btn header-btn-collapse-nav" data-toggle="collapse" data-target=".header-nav-main nav">
											<i class="fas fa-bars"></i>
										</button>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</header>

			<div role="main" class="main" id="main">

				<div class="slider-container light rev_slider_wrapper">
					<div id="revolutionSlider" class="slider rev_slider" data-version="5.4.7" data-plugin-revolution-slider data-plugin-options="{'delay': 9000, 'gridwidth': 1170, 'gridheight': 500, 'responsiveLevels': [4096,1200,992,500]}">
						<ul>
							<li data-transition="fade">
								<img src="img/fondo.jpg"  
									alt=""
									data-bgposition="center center" 
									data-bgfit="cover" 
									data-bgrepeat="no-repeat" 
									class="rev-slidebg">

								<div class="tp-caption"
									data-x="720"
									data-y="175"
									data-start="1000"
									data-transform_in="x:[-300%];opacity:0;s:500;"><img src="img/favourites.png" alt=""></div>

								<div class="tp-caption top-label"
									data-x="755"
									data-y="180"
									data-start="500"
									data-transform_in="y:[-300%];opacity:0;s:500;">PORQUE TODOS SON</div>

								<div class="tp-caption"
									data-x="1025"
									data-y="175 "
									data-start="1000"
									data-transform_in="x:[300%];opacity:0;s:500;"><img src="img/favourites.png" alt=""></div>

								<div class="tp-caption main-label"
									data-x="675"
									data-y="210"
									data-start="1500"
									data-whitespace="nowrap"						 
									data-transform_in="y:[100%];s:500;"
									data-transform_out="opacity:0;s:500;"
									data-mask_in="x:0px;y:0px;">GUARDIANES</div>

								<div class="tp-caption bottom-label"
									data-x="820"
									data-y="280"
									data-start="2000"
									data-fontsize="['20','20','20','30']"
									data-transform_in="y:[100%];opacity:0;s:500;">DEL AGUA.</div>

							
							</li>

							<li data-transition="fade">
								<img src="img/fondo2.jpg"  
									alt=""
									data-bgposition="center center" 
									data-bgfit="cover" 
									data-bgrepeat="no-repeat"   
									class="rev-slidebg" data-no-retina>
							
								<div class="tp-caption main-label"
									data-x="725"
									data-y="190"
									data-start="1800"
									data-whitespace="nowrap"	
									data-fontsize="58"					 
									data-transform_in="y:[100%];s:500;"
									data-transform_out="opacity:0;s:500;"
									data-mask_in="x:0px;y:0px;">#UnGestoCuenta</div>
				
								<div class="tp-caption bottom-label"
									data-x="1030"
									data-y="250"
									data-start="2000"
									data-fontsize="['20','20','20','30']"
									data-lineheight="['20','20','20','30']"
									data-transform_idle="o:1;"
									data-transform_in="y:[100%];z:0;rZ:-35deg;sX:1;sY:1;skX:0;skY:0;s:600;e:Power4.easeInOut;"
									data-transform_out="opacity:0;s:500;"
									data-mask_in="x:0px;y:0px;s:inherit;e:inherit;"
									data-splitin="chars" 
									data-splitout="none" 
									data-responsive_offset="on" 
									data-elementdelay="0.05">#AhorroelAgua</div>
				
							</li> 
						</ul>
					</div>
				</div>

				<div id="home-intro" class="home-intro light">
					<div class="container">

						<div class="row align-items-center">
							<div class="col-lg-8">
								<p>
									Te invitamos a que te unas a este gran proyecto del cuidado y ahorro del agua potable, a través de los <em>Guardianes del Agua. </em>
								</p>
							</div>
							<div class="col-lg-4">
								<div class="get-started text-left text-lg-right">
									<a href="http://portoaguas.gob.ec/online/formulario.php" target='black' class="btn btn-lg btn-light">Registrate!</a>
								</div>
							</div>
						</div>

					</div>
				</div>

				<div class="container" id="conocenos">
					<div class="row text-center">
						<div class="col">
							<h1 class="word-rotator-title mb-2">
								<em>#UnGestoCuenta</em>
							</h1>
							<p class="lead">
								Son todos los niños, niñas y adolescentes de los centros educativos de Portoviejo, interesados en formar una conciencia social cada vez más identificada con los valores de preservación del agua, el medio ambiente y el pago oportuno del agua que se consume.
							</p>
						</div>
					</div>
					<div class="row mt-5">
						<div class="col-lg-4">
							<div class="feature-box">
								<div class="feature-box-icon">
									<i class="fas fa-users"></i>
								</div>
								<div class="feature-box-info">
									<h4 class="heading-primary mb-1">Misión</h4>
									<p>“Contribuir con la gestión sostenible del agua potable, promoviendo la corresponsabilidad de la niñez y adolescencia de Portoviejo”.</p>
								</div>
							</div>
						</div>
						<div class="col-lg-4" style="background: #f4f4f4; ">
							<div class="feature-box">
								<div class="feature-box-icon">
									<i class="fas fa-eye"></i>
								</div>
								<div class="feature-box-info">
									<h4 class="heading-primary mb-1">Visión</h4>
									<p>Para el año lectivo 2019 institucionalizar a los Guardianes del Agua en todos los centros y unidades educativas de Portoviejo.</p>
								</div>
							</div>
						</div>
						<div class="col-lg-4">
							<div class="feature-box">
								<div class="feature-box-icon">
									<i class="fas fa-handshake"></i>
								</div>
								<div class="feature-box-info">
									<h4 class="heading-primary mb-1">Objetivo</h4>
									<p>Participar de las acciones y estrategias de nuestra sociedad, que conlleven a la salud pública de los habitantes de Portoviejo y acercarnos a la cultura del buen uso del agua, adoptarla en todos los ámbitos de nuestra vida diaria.</p>
								</div>
							</div>
						</div>
					</div>
				</div>

				<section class="section section-default">
					<div class="container">
						<div class="row">
							<div class="col-lg-6">
								<h2 class="mt-4">Quién es <strong style="color: #bac644; ">Gotín </strong>y<strong style="color: #ff77ad; "> Gotina</strong></h2> 
								<p class="mt-4">
								Gotina es una hermosa heroína, le encanta el color fucsia, muestra de ello es que su lazo, botas y capa son de este color.
								
								
								</p>
								<p class="mt-4">
									Gotina tiene una mochila fucsia, donde guarda sus “herramientas de ahorro” y las comparte con sus amiguitos.
								</p>

								<p class="mt-4">
									Gotín es hermano de Gotina, ambos comparten el mismo lema y misión, promover el cuidado y ahorro del agua potable.

									
								</p>
								<p class="mt-4">
									Gotín se distingue por su capa de héroe color verde limón, sus botas  y mochila son del mismo color. Siempre está cuidando a su hermana Gotina, la protege y le enseña tips de ahorro.
								</p>
							</div>
							<div class="col-lg-6 mt-4">
								<img class="img-fluid appear-animation" src="img/device.png"  data-appear-animation="zoomIn" alt="">
							</div>
						</div>
					</div>
				</section>

				<div class="container">
					<div class="featured-boxes">
						<div class="row" style="display:  flex;justify-content:  center;">
							<div class="col-lg-5 col-sm-6 ">
								<div class="featured-box featured-box-primary appear-animation" data-appear-animation="fadeInLeft">
									<div class="box-content">
										<a href="gotiahorro">
											<img src="img/ahorros.png" class="img-responsive img-circle" style="width: 100%; height: auto;" alt="">
										</a>
									</div>
								</div>
							</div>
							<div class="col-lg-5 col-sm-6">
								<div class="featured-box featured-box-secondary appear-animation" data-appear-animation="fadeInRight">
									<div class="box-content">
										<a href="descarga-y-diviertete">
											<img src="img/descargas.png" class="img-responsive img-circle" style="width: 100%; height: auto;" alt="">
										</a>
									</div>
								</div>
							</div>
							<div class="col-lg-5 col-sm-6">
								<div class="featured-box featured-box-tertiary appear-animation" data-appear-animation="fadeInLeft">
									<div class="box-content">
										<a href="gotigaleria">
											<img src="img/galeria.png" class="img-responsive img-circle" style="width: 100%; height: auto;" alt="">
										</a>
									</div>
								</div>
							</div>
							<div class="col-lg-5 col-sm-6">
								<div class="featured-box featured-box-quaternary appear-animation" data-appear-animation="fadeInRight">
									<div class="box-content">
										<a href="gotivideo">
											<img src="img/videos.png" class="img-responsive img-circle" style="width: 100%; height: auto;" alt="">
										</a>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="row" id="galeria">
						<div class="col">
							<hr class="tall">
						</div>
					</div>
					<div class="row">
						<div class="col">
							<h2>Ultimos <strong>Eventos</strong> </h2>
							<div class="owl-carousel owl-theme" data-plugin-options="{'responsive': {'0': {'items': 1}, '479': {'items': 1}, '768': {'items': 2}, '979': {'items': 3}, '1199': {'items': 4}}, 'items': 4, 'margin': 10, 'loop': false, 'nav': false, 'dots': true}">
								@foreach($fotos as $f)
								<div style=" width: 185px;">
									<div class="recent-posts">
										<article class="post">
											<img src="{{asset($f->foto)}}" alt=""> <a href="gotigaleria/{{$f->id_album}}" class="read-more">Leer mas<i class="fas fa-angle-right"></i></a>
										</article>
									</div>
								</div>
								@endforeach
						</div>
					</div>
				</div>

			</div>

			<footer id="footer">
				<div class="container">
					<div class="row">
						<!--<div class="footer-ribbon">
							<span>#UnGestoCuenta</span>
						</div>
						<div class="col-lg-3">
							<div class="newsletter">
								<h4>Suscribete</h4>
								<div class="alert alert-success d-none" id="newsletterSuccess">
									<strong>Success!</strong> You've been added to our email list.
								</div>
								<div class="alert alert-danger d-none" id="newsletterError"></div>
								<form id="newsletterForm" action="php/newsletter-subscribe.php" method="POST">
									<div class="input-group">
										<input class="form-control form-control-sm" placeholder="Correo Electronico" name="newsletterEmail" id="newsletterEmail" type="text">
										<span class="input-group-append">
											<button class="btn btn-light" type="submit">Enviar</button>
										</span>
									</div>
								</form>
							</div>
						</div>-->
						<div class="col-lg-6">
							<div class="contact-details">
								<h4>Contacto</h4>
								<ul class="contact">
									<li><p><i class="fas fa-map-marker-alt"></i> <strong>Dirección:</strong> Ciudadela el Maestro calle san francisco y km 1/2 via crucita</p></li>
									<li><p><i class="fas fa-phone"></i> <strong>Teléfono:</strong> 053701960 ext 630</p></li>
									<li><p><i class="far fa-envelope"></i> <strong>Email:</strong> <a href="mailto:info@portoaguas.gob.ec">info@portoaguas.gob.ec</a></p></li>
								</ul>
							</div>
						</div>
						<div class="col-lg-3">
							<h4>Redes Sociales</h4>
							<ul class="social-icons">
								<li class="social-icons-facebook"><a href="https://www.facebook.com/PortoaguasEP/?ref=br_rs" target="_blank" title="Facebook"><i class="fab fa-facebook-f"></i></a></li>
								<li class="social-icons-twitter"><a href="https://twitter.com/Portoaguas?lang=es" target="_blank" title="Twitter"><i class="fab fa-twitter"></i></a></li>
								<li class="social-icons-youtube"><a href="https://www.youtube.com/channel/UCrLZJOnCWXaCmpb6CcoPZBg/videos" target="_blank" title="Youtube"><i class="fab fa-youtube"></i></a></li>
							</ul>
						</div>
						<div class="col-lg-3">
							<a href="http://portoviejo.gob.ec" target="Portoviejo"><img src="img/municipio.png" style="width: 175px; "></a>
							<a href="http://portoaguas.gob.ec" target="Portoaguas"><img src="img/portoaguas-footer.png" style="width: 135px;height:  35px;"></a>
						</div>
					</div>
				</div>
				<!--<div class="footer-copyright">
					<div class="container">
						<div class="row">
							<div class="col-lg-1">
								<a href="index.html" class="logo">
									<img alt="#UnGestoCuenta" class="img-fluid" src="img/logo-footer.png">
								</a>
							</div>
							<div class="col-lg-7">
								<p>© Copyright 2018. All Rights Reserved.</p>
							</div>
							<div class="col-lg-4">
								<nav id="sub-menu">
									<ul>
										<li><a href="page-faq.html"><img src="img/municipio.png" style="width: 175px; "></a></li> 
										<li><a href="sitemap.html"><img src="img/portoaguas-footer.png" style="width: 135px;height:  35px;"></a></li> </ul>
								</nav>
							</div>
						</div>
					</div>
				</div>-->
			</footer>
		</div>

		<!-- Vendor -->
		<script src="js/jquery.min.js"></script>
		<script src="js/jquery.appear.min.js"></script>
		<script src="js/jquery.easing.min.js"></script>
		<script src="js/jquery-cookie.min.js"></script>
		<script src="js/popper.min.js"></script>
		<script src="js/bootstrap.min.js"></script>
		<script src="js/common.min.js"></script>
		<script src="js/jquery.validation.min.js"></script>
		<script src="js/jquery.gmap.min.js"></script>
		<script src="js/jquery.lazyload.min.js"></script>
		<script src="js/jquery.isotope.min.js"></script>
		<script src="js/owl.carousel.min.js"></script>
		<script src="js/jquery.magnific-popup.min.js"></script>
		<script src="js/vide.min.js"></script>
		
		<!-- Theme Base, Components and Settings--> 
		<script src="js/theme.js"></script>
		
		<!-- Current Page Vendor and Views -->
		<script src="js/jquery.themepunch.tools.min.js"></script>		
		<script src="js/jquery.themepunch.revolution.min.js"></script>
		
		<!-- Theme Custom -->
		<script src="js/custom.js"></script>
		
		<!-- Theme Initialization Files -->
		<script src="js/theme.init.js"></script>

		<script>
			$(document).ready(function(){
				var owl = $('.owl-carousel');
					owl.owlCarousel({
    					items:4,
    					loop:true,
    					margin:10,
    					autoplay:true,
    					autoplayTimeout:1000,
    					autoplayHoverPause:true
					});
			});
			(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
			(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
			m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
			})(window,document,'script','//www.google-analytics.com/analytics.js','ga');
		
			ga('create', 'UA-42715764-5', 'auto');
			ga('send', 'pageview');

			jQuery(document).ready(function(){
			    linkInterno = $('a[href^="#"]');
    			linkInterno.on('click',function(e) {	
    			e.preventDefault();
    			var href = $(this).attr('href');
    				$('html, body').animate({ scrollTop : $( href ).offset().top }, 'slow');
    			});
			});
		</script>
		<script src="js/analytics.js"></script>

	</body>
</html>
