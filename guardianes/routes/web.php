<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Route::get('/','HomeController@index');
Route::get('/','HomeController@index2');
Route::get('/personajes','HomeController@personajes');
Route::get('/proceso','HomeController@proceso');
Route::get('/gotiahorro','HomeController@gotiahorro');
Route::get('/gotinoticia','HomeController@gotinoticia');
Route::get('/gotinoticia/{id}','HomeController@view_photos');
Route::get('gotivideo','HomeController@gotivideo');
Route::get('gotigaleria','HomeController@gotigaleria');
Route::get('gotigaleria/{id}','HomeController@view_album');
Route::get('/descarga','HomeController@descarga_y_diviertete');
Route::get('/concurso','HomeController@concurso');
/*Route::get('registro','HomeController@registro');
Route::get('gotiahorro','HomeController@gotiahorro');
Route::get('gotigaleria','HomeController@gotigaleria');
Route::get('gotigaleria/{id}','HomeController@view_album');
Route::get('descarga-y-diviertete','HomeController@descarga_diviertete');
Route::get('descarga-y-diviertete/{id}','HomeController@descarga_diviertete_vista'); 
Route::get('vista','HomeController@vista');
Route::get('descarga-y-diviertete-download/{id}','HomeController@descarga_diviertete_vista1'); 
Route::get('vista','HomeController@vista');*/

/*RUTAS DEL  ADMIN*/

Route::get('administracion','AdminController@index');


Route::post('administracion/login','LoginController@store');
Route::get('administracion/logout','LoginController@logout');
Route::get('administracion/home','AdminController@home');

Route::get('administracion/categorias','CategoriasController@index');
Route::post('administracion/categoria/store','CategoriasController@store');
Route::get('administracion/get_categorias/{id}','CategoriasController@show');
Route::post('administracion/update_categoria','CategoriasController@update');
Route::post('administracion/delete_categoria','CategoriasController@deleter');

Route::get('administracion/usuarios','UsuarioController@index');
Route::post('administracion/usuario/store','UsuarioController@store');
Route::get('administracion/get_usuarios/{id}','UsuarioController@show');
Route::post('administracion/update_usuario','UsuarioController@update');
Route::post('administracion/delete_usuario','UsuarioController@deleter');
Route::post('administracion/clave_usuario','UsuarioController@update2');

/*  rutas de publicaciones*/

Route::get('administracion/publicaciones','PublicacionesController@index');
Route::get('administracion/publicaciones/new_picture','PublicacionesController@create_picture');
Route::post('administracion/publicaciones/store_picture','PublicacionesController@store_picture');

/*RUTAS DE PUBLICACIONES VIDEOS*/
Route::get('administracion/publicaciones/new_video','PublicacionesController@create_video');
Route::post('administracion/publicaciones/store_video','PublicacionesController@store_video');
Route::post('administracion/publicaciones/store_video_enlace','PublicacionesController@store_video_enlace');

/*RUTAS DE PUBLICACIONES NOTICIAS*/
Route::get('administracion/publicaciones/new_noticia','PublicacionesController@create_noticia');
Route::post('administracion/publicaciones/store_noticia','PublicacionesController@store_noticia');
Route::post('administracion/publicaciones/store_noticia_enlace','PublicacionesController@store_noticia_enlace');

/*RUTAS DE PUBLICACIONES CONCURSOS*/
Route::get('administracion/publicaciones/new_concurso','PublicacionesController@create_concurso');
Route::post('administracion/publicaciones/store_concurso','PublicacionesController@store_concurso');


/*RUTAS DE PUBLICACION UN ALBUM*/
Route::get('administracion/publicaciones/new_album','PublicacionesController@create_album');
Route::post('administracion/publicaciones/store_album','PublicacionesController@store_album');

/*RUTAS DE ADMINISTRAR PUBLICACIONES*/
Route::get('administracion/publicaciones/administrar','PublicacionesController@administrarindex');
Route::get('administracion/publicaciones/administrar/admin_picture','PublicacionesController@adminpicture');
Route::get('administracion/get_fotografias/{id}','PublicacionesController@show');
Route::post('administracion/publicaciones/admin_picture','PublicacionesController@admin_picture');
Route::post('administracion/publicaciones/admin_picture_dos','PublicacionesController@admin_picture_dos');
Route::post('administracion/publicaciones/delete_picture','PublicacionesController@delete_picture');

/*RUTAS DE ADMINISTRAR VIDEOS*/
Route::get('administracion/publicaciones/administrar/admin_video','PublicacionesController@adminvideo');
Route::get('administracion/get_videos/{id}','PublicacionesController@showvideo');
Route::post('administracion/publicaciones/admin_video','PublicacionesController@admin_video');
Route::post('administracion/publicaciones/admin_video_dos','PublicacionesController@admin_video_dos');
Route::post('administracion/publicaciones/admin_video_dos_enlace','PublicacionesController@admin_video_dos_enlace');
Route::post('administracion/publicaciones/delete_video','PublicacionesController@delete_video');

/*RUTAS DE ADMINISTRAR CONCURSOS*/
Route::get('administracion/publicaciones/administrar/admin_concurso','PublicacionesController@adminconcurso');
Route::post('administracion/publicaciones/admin_concurso','PublicacionesController@admin_concurso');
Route::post('administracion/publicaciones/admin_concurso_dos','PublicacionesController@admin_concurso_dos');


/*RUTAS DE ADMINISTRAR ALBUM*/
Route::get('administracion/publicaciones/administrar/admin_album','PublicacionesController@adminalbum');
Route::get('administracion/get_albums/{id}','PublicacionesController@showalbum');
Route::post('administracion/publicaciones/admin_album','PublicacionesController@admin_album');
Route::post('administracion/publicaciones/delete_album','PublicacionesController@delete_album');

/*RUTAS DE EDITAR ALBUM*/
Route::get('administracion/publicaciones/administrar/edit_album/{id}','PublicacionesController@editalbum');
Route::get('administracion/get_fotografias_album/{id}','PublicacionesController@showfotografia');
Route::post('administracion/publicaciones/eliminar_picture','PublicacionesController@eliminar_picture');
Route::post('administracion/publicaciones/store_album_dos','PublicacionesController@store_album_dos');

/*RUTAS DE ADMINISTRAR NOTICIAS*/
Route::get('administracion/publicaciones/administrar/admin_noticia','PublicacionesController@adminnoticia');
Route::get('administracion/get_noticia/{id}','PublicacionesController@shownoticia');
Route::post('administracion/publicaciones/admin_noticia','PublicacionesController@admin_noticia');
Route::post('administracion/publicaciones/delete_noticia','PublicacionesController@delete_noticia');
Route::post('administracion/publicaciones/store_noticia_video','PublicacionesController@store_noticia_video');
Route::post('administracion/publicaciones/admin_video_dos_enlace_noticia','PublicacionesController@admin_video_dos_enlace_noticia');
Route::post('administracion/publicaciones/admin_video_dos_n_enlace','PublicacionesController@admin_video_dos_n_enlace');

/*RUTAS DE EDITAR ALBUM NOTICIA*/
Route::get('administracion/publicaciones/administrar/edit_noticia/{id}','PublicacionesController@editnoticia');
/*Route::get('administracion/get_fotografias_album/{id}','PublicacionesController@showfotografia');*/
Route::post('administracion/publicaciones/eliminar_picturen','PublicacionesController@eliminar_picture');
Route::post('administracion/publicaciones/store_album_dos_noticia','PublicacionesController@store_album_dos_noticia');