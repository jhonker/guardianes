<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'nombre' => 'JHONY J. GUAMAN IZA',
            'cedula' => '1312320466',
            'password' =>  bcrypt('11'),
            'privilegio' => '1',
        ]);
    }
}
