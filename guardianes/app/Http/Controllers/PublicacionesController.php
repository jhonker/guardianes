<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Carbon\Carbon;
use Image;
use Storage;
use Hash;
use Illuminate\Support\Facades\Validator;

class PublicacionesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('VAdmin.Publicaciones.index');
    }

    public function create_picture(){
        $categorias = DB::table('categorias')->get();
        return view('VAdmin.Publicaciones.new_picture',compact('categorias'));

    }

    public function create_video(){
        $categorias = DB::table('categorias')->get();
        return view('VAdmin.Publicaciones.new_video',compact('categorias'));
    }

    public function create_album(){
        $categorias = DB::table('categorias')->get();
        return view('VAdmin.Publicaciones.new_album',compact('categorias'));   
    }

    public function create_noticia(){
        $categorias = DB::table('categorias')->get();
        return view('VAdmin.Publicaciones.new_noticia',compact('categorias'));
    }

    public function create_concurso(){
        //$categorias = DB::table('categorias')->get();
        $categorias= DB::select('select * from categorias where categoria="Concursos" ');
        return view('VAdmin.Publicaciones.new_concurso',compact('categorias'));

    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    //public function store_picture(Request $request)
    public function store_picture(Request $request)
    {
        $date = Carbon::now();
        $estado=1;
        $spublicacion = DB::table('publicaciones')->insertGetId(['id_categoria' => $request->categoria,
                             'contenido' => $request->contenido,
                             'fecha_publicacion' => $date,
                             'created_at'=>$date
                            ]);
        if($spublicacion)
        {
            $archivo = $request->file('archivo');
            $input  = array('image' => $archivo) ;
            $reglas = array('image' => 'required|image|mimes:jpeg,jpg,bmp,png,gif');
            $validacion = Validator::make($input,  $reglas);
            if ($validacion->fails()){
               return response()->json(array('error_imagen'=>false));      
            }else {
                $nombre_original=$archivo->getClientOriginalName();
                $extension=$archivo->getClientOriginalExtension();
                $nuevo_nombre="Portoaguas-".$nombre_original;
                $r1=Storage::disk('local')->put($nuevo_nombre,  \File::get($archivo) );
                $rutadelaimagen="fotografias/".$nuevo_nombre;
                if ($r1){
                    $fotografias = DB::table('fotografias')->insertGetId(['id_publicacion' => $spublicacion,
                            'foto' => $rutadelaimagen,
                             'titulo' => $request->titulo,
                            'estado_publicacion' => $estado,
                             'fecha_publicacion' => $date,
                             'created_at'=>$date
                            ]);
                    if($fotografias){
                    return response()->json(["RES"=>true]);
                    }
                    else
                    {
                        return response()->json(["RES"=>false]);    
                    }
                }else{

                }
            }
        }
        else
        {
            return response()->json(["RES"=>false]);
        }
    }
    
    public function store_video(Request $request)
    {
        $date = Carbon::now();
        $estado=1;
        $idc_video='';

        $archivo = $request->file('archivo');
        $nombre_original=$archivo->getClientOriginalName();
        $extension=$archivo->getClientOriginalExtension();

        $max= DB::select("SELECT MAX(id) as id FROM publicaciones");
        foreach ($max as $k) {
            $idc_video= $k->id;
        }

        $idc_video= (integer)$idc_video+1;

        $lon= strlen($nombre_original);
        $nlon= (integer)$lon-4;
        $new_nombre= substr($nombre_original, 0, $nlon);
        $new_nombre= $new_nombre.'_'.$idc_video;
        
        $new_nombre_1= substr($nombre_original, $nlon, $lon);
        $nuevo_nombre= "Portoaguas-".$new_nombre.$new_nombre_1;
        //$nuevo_nombre="Portoaguas-".$nombre_original;
        $r1=Storage::disk('localvideo')->put($nuevo_nombre,  \File::get($archivo) );
        $rutadelaimagen="videos/".$nuevo_nombre;
        if($r1){
            $spublicacion = DB::table('publicaciones')->insertGetId(['id_categoria' => $request->categoria,
            'contenido' => $request->contenid,
            'fecha_publicacion' => $date,
            'created_at'=>$date
           ]);
                if($spublicacion){
                    $video = DB::table('videos')->insert(['id_publicacion' => $spublicacion,
                    'link' => $rutadelaimagen,
                    'estado_link' => $estado,
                    'estado_publico' => '1',
                    'titulo' => $request->titulo,
                    'created_at'=>$date
                    ]);
                    return response()->json(["RES"=>true]);
                }else{
                    return response()->json(["RES"=>false]);
                }
        }else{
            return response()->json(["RES"=>false]);
        }
              
    }
    public function store_video_enlace(Request $request)
    {
        $date = Carbon::now();
        $estado=1;
        $spublicacion = DB::table('publicaciones')->insertGetId(['id_categoria' => $request->categoria,
                             'contenido' => $request->contenid,
                             'fecha_publicacion' => $date,
                             'created_at'=>$date
                            ]);
        if($spublicacion)
        {
            $enlace = DB::table('videos')->insertGetId(['id_publicacion' => $spublicacion,
                'link' => $request->enlace,
                'estado_link' => $estado,
                'estado_publico' => $estado,
                'titulo' => $request->titulo,
                'created_at'=>$date
                ]);
            if($enlace){
                return response()->json(["RES"=>true]);
            }
            else
            {
                return response()->json(["RES"=>false]);    
            }
        }
        else
        {
            return response()->json(["RES"=>false]);
        }
    }
    
    public function store_album(Request $request)
    {
        $date = Carbon::now();
        $estado=1;
        $cont=0;
        //$archivo = ;
        $t_archivos= sizeof($request->file('archivo'));

        $spublicacion = DB::table('albums')->insertGetId(['titulo' => $request->titulo,
            'fecha_publicacion' => $date,
            'created_at'=>$date
        ]);
        if($spublicacion){
                foreach($request->file('archivo') as $file) {
                    $nombre_original=$file->getClientOriginalName();
                    //$destinationPath = public_path() . '/fotografias';
                    //$upload_success = $file->move($destinationPath, $nombre_original);*/
                    $rutadelaimagen="fotografias/".$nombre_original;
                    $r1=Storage::disk('local')->put($nombre_original,  \File::get($file) );
                    if($r1){
                        $fotografias = DB::table('fotografias')->insertGetId(['id_album' => $spublicacion,
                        'foto' => $rutadelaimagen,
                         'titulo' => $request->titulo,
                        'estado_publicacion' => $estado,
                         'fecha_publicacion' => $date,
                         'created_at'=>$date
                        ]);
                        
                    }
                    //printf($file->getClientOriginalName());
                    $cont++;
                }

                if($t_archivos==$cont){
                    return response()->json(["RES"=>true]);
                }else {
                    return response()->json(["RES"=>false]);    
                }
                
            //return response()->json(["RES"=>true]);
        }else{
            return response()->json(["RES"=>false]);
        }
        
        /*// getting all of the post data
        $files = $request->file('archivo');
        // recorremos cada archivo y lo subimos individualmente
        foreach($files as $file) {
            $nombre_original=$file->getClientOriginalName();
            $destinationPath = public_path() . '/fotografias';
            $upload_success = $file->move($destinationPath, $nombre_original);
        }*/
      
    }

    public function store_noticia(Request $request)
    {
        $date = Carbon::now();
        $estado=1;
        $cont=0;
        //$archivo = ;
        $t_archivos= sizeof($request->file('archivo'));

        $spublicacion = DB::table('noticias')->insertGetId(['titulo' => $request->titulonoticia,
            'contenido' => $request->contenidonoticia,
            'fecha_publicacion' => $date,
            'created_at'=>$date
        ]);
        if($spublicacion){
                foreach($request->file('archivo') as $file) {
                    $nombre_original=$file->getClientOriginalName();
                    //$destinationPath = public_path() . '/fotografias';
                    //$upload_success = $file->move($destinationPath, $nombre_original);
                    $rutadelaimagen="fotografias/".$nombre_original;
                    $r1=Storage::disk('local')->put($nombre_original,  \File::get($file) );
                    if($r1){
                        $fotografias = DB::table('fotografias')->insertGetId(['id_noticia' => $spublicacion,
                        'foto' => $rutadelaimagen,
                         'titulo' => $request->titulonoticia,
                        'estado_publicacion' => $estado,
                         'fecha_publicacion' => $date,
                         'created_at'=>$date
                        ]);
                        
                    }
                    //printf($file->getClientOriginalName());
                    $cont++;
                }

                if($t_archivos==$cont){
                    return response()->json(["RES"=>true]);
                }else {
                    return response()->json(["RES"=>false]);    
                }
                
            //return response()->json(["RES"=>true]);
        }else{
            return response()->json(["RES"=>false]);
        }
      
    }

    public function store_noticia_enlace(Request $request)
    {
        $date = Carbon::now();
        $estado=1;
        $cont=0;
        //$archivo = ;
        $t_archivos= sizeof($request->file('archivo'));

        $spublicacion = DB::table('noticias')->insertGetId(['titulo' => $request->titulonoticia,
            'contenido' => $request->contenidonoticia,
            'fecha_publicacion' => $date,
            'created_at'=>$date
        ]);
        if($spublicacion){
                $enlace1 = DB::table('videos')->insertGetId(['id_noticia' => $spublicacion,
                'link' => $request->enlacen,
                'estado_link' => $estado,
                'estado_publico' => $estado,
                'titulo' => $request->titulonoticia,
                'created_at'=>$date
                ]);
            if($enlace1){
                return response()->json(["RES"=>true]);
            }
            else
            {
                return response()->json(["RES"=>false]);    
            }
                
            //return response()->json(["RES"=>true]);
        }else{
            return response()->json(["RES"=>false]);
        }
      
    }
    public function store_noticia_video(Request $request)
    {
        $date = Carbon::now();
        $estado=1;
        
        $archivo = $request->file('archivov');
        $nombre_original=$archivo->getClientOriginalName();
        $extension=$archivo->getClientOriginalExtension();
        $nuevo_nombre="Portoaguas-".$nombre_original;
        $r1=Storage::disk('localvideo')->put($nuevo_nombre,  \File::get($archivo) );
        $rutadelaimagen="videos/".$nuevo_nombre;
        if($r1){
            $spublicacion = DB::table('noticias')->insertGetId(['titulo' => $request->titulonoticia,
            'contenido' => $request->contenidonoticia,
            'fecha_publicacion' => $date,
            'created_at'=>$date
        ]);
                if($spublicacion){
                    $video = DB::table('videos')->insert(['id_noticia' => $spublicacion,
                    'link' => $rutadelaimagen,
                    'estado_link' => $estado,
                    'estado_publico' => '1',
                    'titulo' => $request->titulonoticia,
                    'created_at'=>$date
                    ]);
                    return response()->json(["RES"=>true]);
                }else{
                    return response()->json(["RES"=>false]);
                }
        }else{
            return response()->json(["RES"=>false]);
        }
              
    }

     public function store_concurso(Request $request)
    {
        $date = Carbon::now();
        $date_2 = Carbon::now();
        $date_2= $date_2->toFormattedDateString();
        $estado=1;
        $spublicacion = DB::table('publicaciones')->insertGetId(['id_categoria' => $request->categoriac,
                             'contenido' => $request->contenidocon,
                             'fecha_publicacion' => $date,
                             'created_at'=>$date
                            ]);
        if($spublicacion)
        {
            $archivo = $request->file('archivoc');
            $input  = array('image' => $archivo) ;
            $reglas = array('image' => 'required|image|mimes:jpeg,jpg,bmp,png,gif');
            $validacion = Validator::make($input,  $reglas);
            if ($validacion->fails()){
               return response()->json(array('error_imagen'=>false));      
            }else {
                $nombre_original=$archivo->getClientOriginalName();
                $extension=$archivo->getClientOriginalExtension();
                $nuevo_nombre="Portoaguas-".$nombre_original;
                $r1=Storage::disk('local')->put($nuevo_nombre,  \File::get($archivo) );
                $rutadelaimagen="fotografias/".$nuevo_nombre;
                if ($r1){
                    $fotografias = DB::table('fotografias')->insertGetId(['id_publicacion' => $spublicacion,
                            'foto' => $rutadelaimagen,
                             'titulo' => 'Concurso PortoAguas '.$date_2,
                            'estado_publicacion' => $estado,
                             'fecha_publicacion' => $date,
                             'created_at'=>$date
                            ]);
                    if($fotografias){
                    return response()->json(["RES"=>true]);
                    }
                    else
                    {
                        return response()->json(["RES"=>false]);    
                    }
                }else{

                }
            }
        }
        else
        {
            return response()->json(["RES"=>false]);
        }
    }
    
    /*PAGINA PRINCIPAL DE ADMINISTRACION DE IMAGENES, ALBUMES, VIDEOS*/
    public function administrarindex()
    {
        return view('VAdmin.Publicaciones.Administrar.index');
    }
    
    /*ADMINISTRAR IMAGENES*/
    public function adminpicture(){
        $adminpicture = DB::select('select f.id, f.foto, f.titulo, p.contenido, c.categoria from fotografias f, publicaciones p, categorias c WHERE f.id_publicacion=p.id and p.id_categoria=c.id and f.estado_publicacion!=0  ');
        //return view('VAdmin.Publicaciones.admin_picture',compact('categorias'));
        $categorias= DB::select('select id, categoria from categorias ');
        return view('VAdmin.Publicaciones.Administrar.admin_picture',compact('adminpicture','categorias'));
    }
    public function show($id)
    {
        $foto = DB::select('select f.id,  f.id_publicacion, f.foto, f.titulo, p.contenido, c.id as categoria from fotografias f, publicaciones p, categorias c WHERE f.id_publicacion=p.id and p.id_categoria=c.id and f.id='.$id.'');
        return $foto;
    }
    public function admin_picture(Request $request)
    {
        $date = Carbon::now();
        $spublicacion = DB::update('UPDATE publicaciones SET id_categoria= ?, contenido=?, updated_at = ? where id = ?',[$request->categoria, $request->contenido, $date, $request->idpublicacion]);
        
        if($spublicacion)
        {
            $fotografias = DB::update('UPDATE fotografias SET id_publicacion= ?, titulo=?, foto=?, updated_at = ? where id = ?',[$request->idpublicacion, $request->titulo, $request->foto, $date, $request->idfoto]);
            if($fotografias){
                return response()->json(["RES"=>true]);
            }
            else
            {
                return response()->json(["RES"=>false]);    
            }
             //return response()->json(["RES"=>true]);
        }
        else
        {
            return response()->json(["RES"=>false]);
        }
    }
    public function admin_picture_dos(Request $request)
    {
            $date = Carbon::now();
            $archivo = $request->file('archivo');
            //return $archivo;
            $input  = array('image' => $archivo) ;
            $reglas = array('image' => 'required|image|mimes:jpeg,jpg,bmp,png,gif');
            $validacion = Validator::make($input,  $reglas);
            if ($validacion->fails()){
               return response()->json(array('error_imagen'=>false));      
            }else {
                $nombre_original=$archivo->getClientOriginalName();
                $extension=$archivo->getClientOriginalExtension();
                $nuevo_nombre="Portoaguas-".$nombre_original;
                $r1=Storage::disk('local')->put($nuevo_nombre,  \File::get($archivo) );
                $rutadelaimagen="fotografias/".$nuevo_nombre;
                if ($r1){
                     $fotografias = DB::update('UPDATE fotografias SET foto=?, updated_at = ? where id = ?',[$rutadelaimagen, $date, $request->idc_fotografia]);
                    if($fotografias){
                    return response()->json(["RES"=>true]);
                    }
                    else
                    {
                        return response()->json(["RES"=>false]);    
                    }
                }else{

                }
            }
    }
    public function delete_picture(Request $request)
    {
        $date = Carbon::now();
        $estado = 0;
        $spublicacion = DB::update('UPDATE fotografias SET estado_publicacion= ?, updated_at = ? where id = ?',[ $estado, $date, $request->idfoto]);
        
        if($spublicacion)
        {
            return response()->json(["RES"=>true]);
        }
        else
        {
            return response()->json(["RES"=>false]);
        }
    }
    
    /*ADMINISTRAR VIDEOS*/
    public function adminvideo(){
        $adminvideo = DB::select('select v.id, v.link, v.titulo, p.contenido, c.categoria from videos v, publicaciones p, categorias c WHERE v.id_publicacion=p.id and p.id_categoria= c.id and v.estado_link!=0   ');
        //return view('VAdmin.Publicaciones.admin_picture',compact('categorias'));
        $categorias= DB::select('select id, categoria from categorias ');
        return view('VAdmin.Publicaciones.Administrar.admin_video',compact('adminvideo','categorias'));
    }
    public function showvideo($id)
    {
        $video = DB::select('select v.id, v.id_publicacion, v.titulo, v.link, p.contenido, c.id as categoria from videos v, publicaciones p, categorias c WHERE v.id_publicacion=p.id and p.id_categoria=c.id and v.id='.$id.'');
        return $video;
    }
    public function admin_video(Request $request)
    {
        $date = Carbon::now();
        $spublicacion = DB::update('UPDATE publicaciones SET id_categoria= ?, contenido=?, updated_at = ? where id = ?',[$request->categoria, $request->contenido, $date, $request->idpublicacion]);
        
        if($spublicacion)
        {
            $videos = DB::update('UPDATE videos SET id_publicacion= ?, titulo=?,  updated_at = ? where id = ?',[$request->idpublicacion, $request->titulo, $date, $request->idvideo]);
            if($videos){
                return response()->json(["RES"=>true]);
            }
            else
            {
                return response()->json(["RES"=>false]);    
            }
             //return response()->json(["RES"=>true]);
        }
        else
        {
            return response()->json(["RES"=>false]);
        }
    }
    public function admin_video_dos(Request $request)
    {
        $date = Carbon::now();
        $archivo = $request->file('archivo');
        $nombre_original=$archivo->getClientOriginalName();
        $extension=$archivo->getClientOriginalExtension();
        $lon= strlen($nombre_original);
        $nlon= (integer)$lon-4;
        $new_nombre= substr($nombre_original, 0, $nlon);
        $new_nombre= $new_nombre.'_'.$request->idc_video;
        
        $new_nombre_1= substr($nombre_original, $nlon, $lon);
        $nuevo_nombre= "Portoaguas-".$new_nombre.$new_nombre_1;
        //$nuevo_nombre="Portoaguas-".$nombre_original.'_'.$request->idc_video;

        $r1=Storage::disk('localvideo')->put($nuevo_nombre,  \File::get($archivo) );
        $rutadelaimagen="videos/".$nuevo_nombre;
        if ($r1){
            $videos = DB::update('UPDATE videos SET link=?, updated_at = ? where id = ?',[$rutadelaimagen, $date, $request->idc_video]);
            if($videos){
                return response()->json(["RES"=>true]);
            }
            else
            {
                return response()->json(["RES"=>false]);    
            }
        }else{

        }
    }
    public function admin_video_dos_enlace(Request $request)
    {
        $date = Carbon::now();
        $videos = DB::update('UPDATE videos SET link=?, updated_at = ? where id = ?',[$request->contenid, $date, $request->idce_video]);
        if($videos){
            return response()->json(["RES"=>true]);
        }
        else
        {
            return response()->json(["RES"=>false]);    
        }
    }
    
    public function delete_video(Request $request)
    {
        $date = Carbon::now();
        $estado = 0;
        $spublicacion = DB::update('UPDATE videos SET estado_link= ?, updated_at = ? where id = ?',[ $estado, $date, $request->idvideo]);
        
        if($spublicacion)
        {
            return response()->json(["RES"=>true]);
        }
        else
        {
            return response()->json(["RES"=>false]);
        }
    }
    
    /*ADMINISTRAR ALBUM*/
    public function adminalbum(){
        //$adminalbum = DB::select('SELECT a.id, a.titulo, f.id as idfotografia, f.foto from albums a, fotografias f WHERE a.id=f.id_album and f.titulo=a.titulo and f.estado_publicacion!=0 ');
        //$albumes = DB::select('SELECT DISTINCT id, titulo FROM albums');
        $albumes = DB::select('SELECT DISTINCT a.id, a.titulo FROM albums a, fotografias f where f.estado_publicacion!=0 and a.id=f.id_album ');
        $album =DB::table('fotografias')->select('foto','titulo','id_album')
            ->where('estado_publicacion','!=','0')
            ->get();
        
        return view('VAdmin.Publicaciones.Administrar.admin_album',compact('albumes','album'));
    }
    public function showalbum($id)
    {
        $album = DB::select('select id, titulo from albums WHERE id='.$id.'');
        return $album;
    }
    public function admin_album(Request $request)
    {
        $date = Carbon::now();
        $spublicacion = DB::update('UPDATE albums SET titulo=?, updated_at = ? where id = ?',[$request->titulo, $date, $request->idalbum]);
        
        if($spublicacion)
        {
            return response()->json(["RES"=>true]);
        }
        else
        {
            return response()->json(["RES"=>false]);
        }
    }
    public function delete_album(Request $request)
    {
        $date = Carbon::now();
        $estado = 0;
        $spublicacion = DB::update('UPDATE fotografias SET estado_publicacion= ?, updated_at = ? where id_album = ?',[ $estado, $date, $request->idalbum]);
        if($spublicacion)
        {
            return response()->json(["RES"=>true]);
        }
        else
        {
            return response()->json(["RES"=>false]);
        }
    }
    
    /*EDITAR FOTOGRAFIAS DE ALBUM*/
    public function editalbum($id){
        $album = DB::select('select id, titulo from albums WHERE id='.$id.'');
        $fotos = DB::select('SELECT id, foto, titulo FROM fotografias WHERE id_album ='.$id.' and estado_publicacion !=0 ');
        return view('VAdmin.Publicaciones.Administrar.edit_album',compact('id','album','fotos'));   
    }
    public function showfotografia($id)
    {
        $album = DB::select('select id_album from fotografias WHERE id='.$id.'');
        return $album;
    }
    
    public function eliminar_picture(Request $request)
    {
        $date = Carbon::now();
        $estado = 0;
        $spublicacion = DB::update('UPDATE fotografias SET estado_publicacion= ?, updated_at = ? where id = ?',[ $estado, $date, $request->idfoto]);
        if($spublicacion)
        {
            return response()->json(["RES"=>true]);
        }
        else
        {
            return response()->json(["RES"=>false]);
        }
    }
    public function store_album_dos(Request $request)
    {
        $date = Carbon::now();
        $estado=1;
        $cont=0;
        //$archivo = ;
        $t_archivos= sizeof($request->file('archivo'));
        foreach($request->file('archivo') as $file) {
            $nombre_original=$file->getClientOriginalName();
            $rutadelaimagen="fotografias/".$nombre_original;
            $r1=Storage::disk('local')->put($nombre_original,  \File::get($file) );
            if($r1){
                $fotografias = DB::table('fotografias')->insertGetId(['id_album' => $request->idalbum,
                'foto' => $rutadelaimagen,
                'titulo' => $request->titulo,
                'estado_publicacion' => $estado,
                'fecha_publicacion' => $date,
                'created_at'=>$date
                ]);
            }
                    //printf($file->getClientOriginalName());
            $cont++;
        }

                if($t_archivos==$cont){
                    return response()->json(["RES"=>true]);
                }else {
                    return response()->json(["RES"=>false]);    
                }
    }

    /*ADMINISTRAR NOTICIAS*/
    public function adminnoticia(){
        //$adminalbum = DB::select('SELECT a.id, a.titulo, f.id as idfotografia, f.foto from albums a, fotografias f WHERE a.id=f.id_album and f.titulo=a.titulo and f.estado_publicacion!=0 ');
        //$albumes = DB::select('SELECT DISTINCT id, titulo FROM albums');
        $albumes = DB::select(' SELECT DISTINCT a.id, a.titulo, a.contenido FROM noticias a, fotografias f where f.estado_publicacion!=0 and a.id=f.id_noticia ');

        $adminvideo = DB::select('select v.id, v.link, v.id_noticia, n.titulo, n.contenido, n.id as idnoticia from videos v, noticias n WHERE v.id_noticia=n.id and v.estado_link!=0 ');

        $album =DB::table('fotografias')->select('foto','titulo','id_noticia')
            ->where('estado_publicacion','!=','0')
            ->get();
        //return $album;
        return view('VAdmin.Publicaciones.Administrar.admin_noticia',compact('albumes','album','adminvideo'));
    }
    public function shownoticia($id)
    {
        $album = DB::select('select id, titulo, contenido from noticias WHERE id='.$id.'');
        return $album;
    }
    public function admin_noticia(Request $request)
    {
        $date = Carbon::now();
        $spublicacion = DB::update('UPDATE noticias SET titulo=?, contenido= ?,updated_at = ? where id = ?',[$request->titulo, $request->contenido, $date, $request->idnoticia]);
        
        if($spublicacion)
        {
            return response()->json(["RES"=>true]);
        }
        else
        {
            return response()->json(["RES"=>false]);
        }
    }
    public function delete_noticia(Request $request)
    {
        $date = Carbon::now();
        $estado = 0;
        $spublicacion = DB::update('UPDATE fotografias SET estado_publicacion= ?, updated_at = ? where id_noticia = ?',[ $estado, $date, $request->idnoticia]);
        if($spublicacion)
        {
            return response()->json(["RES"=>true]);
        }
        else
        {
            return response()->json(["RES"=>false]);
        }
    }

    /*EDITAR FOTOGRAFIAS DE ALBUM*/
    public function editnoticia($id){
        $album = DB::select('select id, titulo, contenido from noticias WHERE id='.$id.'');
        $fotos = DB::select('SELECT id, foto, titulo FROM fotografias WHERE id_noticia ='.$id.' and estado_publicacion !=0 ');
        return view('VAdmin.Publicaciones.Administrar.edit_noticia',compact('id','album','fotos'));   
    }
    public function store_album_dos_noticia(Request $request)
    {
        $date = Carbon::now();
        $estado=1;
        $cont=0;
        //$archivo = ;
        $t_archivos= sizeof($request->file('archivo'));
        foreach($request->file('archivo') as $file) {
            $nombre_original=$file->getClientOriginalName();
            $rutadelaimagen="fotografias/".$nombre_original;
            $r1=Storage::disk('local')->put($nombre_original,  \File::get($file) );
            if($r1){
                $fotografias = DB::table('fotografias')->insertGetId(['id_noticia' => $request->idnoticia,
                'foto' => $rutadelaimagen,
                'titulo' => $request->titulo,
                'estado_publicacion' => $estado,
                'fecha_publicacion' => $date,
                'created_at'=>$date
                ]);
            }
                    //printf($file->getClientOriginalName());
            $cont++;
        }

                if($t_archivos==$cont){
                    return response()->json(["RES"=>true]);
                }else {
                    return response()->json(["RES"=>false]);    
                }
    }
    public function admin_video_dos_enlace_noticia(Request $request)
    {
        $date = Carbon::now();
        $videos = DB::update('UPDATE videos SET link=?, updated_at = ? where id = ?',[$request->contenid, $date, $request->idce_video]);
        if($videos){
            return response()->json(["RES"=>true]);
        }
        else
        {
            return response()->json(["RES"=>false]);    
        }
    }
    public function admin_video_dos_n_enlace(Request $request)
    {
        $date = Carbon::now();
        $archivo = $request->file('archivoev');
        $nombre_original=$archivo->getClientOriginalName();
        $extension=$archivo->getClientOriginalExtension();
        $nuevo_nombre="Portoaguas-".$nombre_original;
        $r1=Storage::disk('localvideo')->put($nuevo_nombre,  \File::get($archivo) );
        $rutadelaimagen="videos/".$nuevo_nombre;
        if ($r1){
            $videos = DB::update('UPDATE videos SET link=?, updated_at = ? where id = ?',[$rutadelaimagen, $date, $request->idc_video]);
            if($videos){
                return response()->json(["RES"=>true]);
            }
            else
            {
                return response()->json(["RES"=>false]);    
            }
        }else{

        }
    }

    /*ADMINISTRAR CONCURSO*/
    public function adminconcurso(){
        $adminpicture = DB::select('select f.id, f.foto, f.titulo, p.contenido, c.id as idcategoria ,c.categoria from fotografias f, publicaciones p, categorias c WHERE f.id_publicacion=p.id and p.id_categoria=c.id and f.estado_publicacion!=0  ');
        //return view('VAdmin.Publicaciones.admin_picture',compact('categorias'));
        $categorias= DB::select('select id, categoria from categorias WHERE categoria="Concursos"  ');
        return view('VAdmin.Publicaciones.Administrar.admin_concurso',compact('adminpicture','categorias'));
    }

    public function admin_concurso(Request $request)
    {
        $date = Carbon::now();
        $spublicacion = DB::update('UPDATE publicaciones SET id_categoria= ?, contenido=?, updated_at = ? where id = ?',[$request->categoria, $request->contenido, $date, $request->idpublicacion]);
        
        if($spublicacion)
        {
            $fotografias = DB::update('UPDATE fotografias SET id_publicacion= ?, titulo=?, foto=?, updated_at = ? where id = ?',[$request->idpublicacion, $request->titulo, $request->foto, $date, $request->idfoto]);
            if($fotografias){
                return response()->json(["RES"=>true]);
            }
            else
            {
                return response()->json(["RES"=>false]);    
            }
             //return response()->json(["RES"=>true]);
        }
        else
        {
            return response()->json(["RES"=>false]);
        }
    }

    public function admin_concurso_dos(Request $request)
    {
            $date = Carbon::now();
            $archivo = $request->file('archivo');
            //return $archivo;
            $input  = array('image' => $archivo) ;
            $reglas = array('image' => 'required|image|mimes:jpeg,jpg,bmp,png,gif');
            $validacion = Validator::make($input,  $reglas);
            if ($validacion->fails()){
               return response()->json(array('error_imagen'=>false));      
            }else {
                $nombre_original=$archivo->getClientOriginalName();
                $extension=$archivo->getClientOriginalExtension();
                $nuevo_nombre="Portoaguas-".$nombre_original;
                $r1=Storage::disk('local')->put($nuevo_nombre,  \File::get($archivo) );
                $rutadelaimagen="fotografias/".$nuevo_nombre;
                if ($r1){
                     $fotografias = DB::update('UPDATE fotografias SET foto=?, updated_at = ? where id = ?',[$rutadelaimagen, $date, $request->idc_fotografia]);
                    if($fotografias){
                    return response()->json(["RES"=>true]);
                    }
                    else
                    {
                        return response()->json(["RES"=>false]);    
                    }
                }else{

                }
            }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
    
     
}
