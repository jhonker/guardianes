<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use DB;

class HomeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $fotos = DB::select("SELECT * FROM guardianes.fotografias f where id_album is not null   order by id desc limit 5");
        return view('home', compact('fotos'));
    }
    public function index2()
    {
        //$fotos = DB::select("SELECT * FROM guardianes.fotografias f where id_album is not null   order by id desc limit 5");
        return view('welcome');
    }
    public function personajes(){
        return view('frontend.personajes');
    }

    public function proceso(){
        return view('frontend.proceso');
    }


    public function registro(){
        $unidades = DB::table('unidades')->get();
        return view('registro',compact('unidades'));
    }

    public function gotiahorro(){
       
        $gotiahorro = DB::select('select DISTINCT f.foto,f.titulo from fotografias f, publicaciones p, categorias c where f.id_publicacion = p.id and p.id_categoria in (SELECT id from categorias WHERE categoria="GotiAhorro") '); 
        $gotiahorrovideo = DB::select('select DISTINCT v.link, v.titulo from videos v, publicaciones p, categorias c where v.id_publicacion = p.id and p.id_categoria in (SELECT id from categorias WHERE categoria="GotiAhorro") ');
        return view('frontend.Gotiahorro',compact('gotiahorro','gotiahorrovideo'));
    }
    /*
    public function gotiahorro(){
        
        $gotiahorro = DB::select('select DISTINCT f.foto,f.titulo from fotografias f, publicaciones p, categorias c where f.id_publicacion = p.id and p.id_categoria in (SELECT id from categorias WHERE categoria="GotiAhorro") '); 
        $gotiahorrovideo = DB::select('select DISTINCT v.link, v.titulo from videos v, publicaciones p, categorias c where v.id_publicacion = p.id and p.id_categoria in (SELECT id from categorias WHERE categoria="GotiAhorro") ');
        return view('gotiahorro',compact('gotiahorro','gotiahorrovideo'));
    }*/

    public function gotinoticia(){
        $gotivideo = DB::table('videos')
                        ->where("estado_publico","=","1")
                        ->get();
        $gotinoticia = DB::table('noticias')
                        ->where("estado","=","0")
                        ->get();
        $fotos= DB::table('fotografias')
                        ->where("estado_publicacion","=","1")
                        ->get();

        return view('frontend.Gotinoticia',compact('gotivideo','gotinoticia','fotos'));
    }

    public function concurso(){
        $idc='';
        $idcat= DB::select('select id from categorias where categoria="Concursos" ');
        foreach ($idcat as $k) {
            $idc= $k->id;
        }
        $publi= DB::table('publicaciones')
                        ->where("id_categoria","=",$idc)
                        ->get();
        $fotos= DB::table('fotografias')
                        ->where("estado_publicacion","=","1")
                        ->get();
        return view('frontend.concursos',compact('publi','fotos'));
    }

    public function view_photos($id){
        $nid= base64_decode($id);
        $gotinoticia = DB::table('noticias')
                        ->where("id","=",$nid)
                        ->get();
        //return $gotinoticia;
        $fotos= DB::table('fotografias')
                        ->where("estado_publicacion","=","1")
                        ->get();
        $album =DB::table('fotografias')->select('foto','titulo')->where('id_noticia','=',$nid)->get();
        //return $album;
        return view('frontend.Gotinoticiaimagen',compact('gotinoticia','fotos','album'));
    }

    public function gotivideo(){
        $gotivideo = DB::table('videos')
                        ->join('publicaciones','publicaciones.id','=','videos.id_publicacion')
                        ->get();
        return view('frontend.Gotivideo',compact('gotivideo'));
    }

    public function descarga_diviertete(){
        $fotos= DB::select('select DISTINCT f.id,f.foto, f.titulo from fotografias f INNER JOIN publicaciones p, categorias c WHERE p.id= f.id_publicacion and p.id_categoria in (SELECT id from categorias WHERE categoria="Descarga-y-Diviertete") ');
        return view('descarga-y-diviertete',compact('fotos'));
    }
    
    public function descarga_diviertete_vista($id){
        $album =DB::table('fotografias')->select('id','foto','titulo')->where('id','=',$id)->get();
        //return $album;
        foreach($album as $a){
            header("Content-Type: application/force-download");
            header("Content-Disposition: attachment; filename=$a->titulo".".jpg");
            header("Content-Transfer-Encoding: binary");
            readfile($a->foto);
        }
       
        return view('vista',compact('album'));
    }
    public function descarga_diviertete_vista1($id){
        $album =DB::table('fotografias')->select('id','foto','titulo')->where('id','=',$id)->get();
        //return $album;
        /*foreach($album as $a){
            header("Content-Type: application/force-download");
            header("Content-Disposition: attachment; filename=$a->titulo".".jpg");
            header("Content-Transfer-Encoding: binary");
            readfile($a->foto);
        }*/
       
        return view('vista',compact('album'));
    }
    public function vista()
    {   
        return view('vista');
    }
    public function gotigaleria(){
        $galerias = DB::select("SELECT distinct a.id,a.titulo,a.fecha_publicacion FROM albums a inner join fotografias f where f.id_album = a.id ");
        $foto_portada = DB::table('fotografias')
        ->whereNotNull('id_album')
        ->groupBy('id_album','foto','id')
        ->select('id_album','foto','id')
        ->inRandomOrder()
        ->get();
        return view('frontend.Gotigaleria',compact('galerias','foto_portada'));
    }

    public function view_album($id){
        //$galerias = DB::select("SELECT distinct a.id,a.titulo,a.fecha_publicacion FROM albums a inner join fotografias f where f.id_album = a.id ");
        $galerias= DB::select("SELECT distinct a.id,a.titulo,a.fecha_publicacion FROM albums a inner join fotografias f where f.id_album = a.id AND a.id!=".$id);
        $foto_portada = DB::table('fotografias')
        ->whereNotNull('id_album')
        ->groupBy('id_album','foto','id')
        ->select('id_album','foto','id')
        ->inRandomOrder()
        ->get();
        $album =DB::table('fotografias')->select('foto','titulo')->where('id_album','=',$id)->get();
        $nalbum= DB::table('albums')->select('titulo')->where('id','=',$id)->get();
        //return $galerias;
        return view('frontend.open_album',compact('album','galerias','foto_portada','nalbum'));
    }

    public function descarga_y_diviertete(){
        //$galerias = DB::select("SELECT distinct a.id,a.titulo,a.fecha_publicacion FROM albums a inner join fotografias f where f.id_album = a.id ");
        $galerias= DB::select("SELECT distinct a.id,a.titulo,a.fecha_publicacion FROM albums a inner join fotografias f where f.id_album = a.id");
        $foto_portada = DB::table('fotografias')
        ->whereNotNull('id_album')
        ->groupBy('id_album','foto','id')
        ->select('id_album','foto','id')
        ->inRandomOrder()
        ->get();
        $album =DB::table('fotografias')->select('foto','titulo')->where('id_album','=','1')->get();
        $nalbum= DB::table('albums')->select('titulo')->where('id','=','1')->get();
        //return $galerias;
        return view('frontend.Descarga_y_Diviertete',compact('album','galerias','foto_portada','nalbum'));
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
