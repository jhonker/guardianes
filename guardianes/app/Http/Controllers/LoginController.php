<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use DB;
use Hash;

use Session;

use Redirect;

class LoginController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    public function logout(){
        Session::flush();
        return Redirect::to('administracion');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
           //return response()->json(["sms"=>"login" ]);
           $usuario = DB::table('users')->where('cedula','=',$request->cedula)->get();
            if($usuario=='[]'){
               return response()->json(["usuario"=>false]);
            }else{
                $hash="";
                foreach($usuario as $u){
                    $hash=$u->password;
                }
                if(Hash::check($request->clave,$hash)){
                    $user = DB::table("users")->where("cedula","=",$request->cedula)->get();
                    foreach ($user as $us) {
                        $request->session()->put('usuario', $us->cedula);
                        $request->session()->put('nombres_apellidos', $us->nombre);
                        $request->session()->put('rol', $us->privilegio);
                    }
                    return response()->json(["result"=>true]);
                    
                }else{
                    return response()->json(["clave"=>false]);
                }
            }
           
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
