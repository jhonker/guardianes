<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Carbon\Carbon;

class UsuarioController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //$usuarios = DB::table('users')->get();
        $usuarios = DB::table('users')->where("estado","=","1")->get();
        return view('VAdmin.Usuarios.index',compact('usuarios'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $date = Carbon::now();
        $usuario = DB::table('users')->insert(['nombre' => $request->nombres,
                             'cedula' => $request->cedula,
                             'password' => bcrypt($request->clave),
                             'privilegio' => $request->privilegio,
                              'estado' => $request->estado,
                             'created_at'=>$date
                            ]);
        if($usuario){
            return response()->json(["RES"=>true]);
        }else{
            return response()->json(["RES"=>false]);
        }
        //return response()->json(["RES"=>true]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $usuario = DB::table('users')->where('id','=',$id)->get();
        return $usuario;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $date = Carbon::now();
        $usuario = DB::update('UPDATE users SET nombre= ?, cedula=?, privilegio=?, updated_at = ? where id = ?',[$request->nombres, $request->cedula, $request->privilegio, $date, $request->id]);
        if($usuario==1){
            return response()->json(["RES"=>true]);
        }else{
            return response()->json(["RES"=>false]);
        }
    }
    
    public function update2(Request $request)
    {
        $date = Carbon::now();
        $usuario = DB::update('UPDATE users SET password= ?, updated_at = ? where id = ?',[bcrypt($request->clave), $date, $request->id]);
        if($usuario==1){
            return response()->json(["RES"=>true]);
        }else{
            return response()->json(["RES"=>false]);
        }
    }
    
    public function deleter(Request $request)
    {
        $date = Carbon::now();
        $usuario = DB::update('UPDATE users SET estado= ?, updated_at = ? where id = ?',[$request->estado, $date, $request->id]);
        if($usuario==1){
            return response()->json(["RES"=>true]);
        }else{
            return response()->json(["RES"=>false]);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
