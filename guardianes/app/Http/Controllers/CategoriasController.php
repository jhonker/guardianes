<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use DB;
use Carbon\Carbon;

class CategoriasController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //$categorias = DB::table('categorias')->get();
        $categorias = DB::table('categorias')->where("estado","=","1")->get();
        return view('VAdmin.Categorias.index',compact('categorias'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $date = Carbon::now();

        $dependientes = DB::table('categorias')->insert(['categoria' => $request->categoria,
                             'fecha' => $date,
                             'created_at'=>$date
                            ]);
        return response()->json(["RES"=>true]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $categoria = DB::table('categorias')->where('id','=',$id)->get()    ;
        return $categoria;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $date = Carbon::now();
        $categoria = DB::update('UPDATE categorias SET categoria= ?, updated_at = ? where id = ?',[$request->categoria, $date, $request->id]);
        if($categoria==1){
            return response()->json(["RES"=>true]);
        }else{
            return response()->json(["RES"=>false]);
        }
    }
    
    public function deleter(Request $request)
    {
        $date = Carbon::now();
        $categoria = DB::update('UPDATE categorias SET estado= ?, updated_at = ? where id = ?',[$request->estado, $date, $request->id]);
        if($categoria==1){
            return response()->json(["RES"=>true]);
        }else{
            return response()->json(["RES"=>false]);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
